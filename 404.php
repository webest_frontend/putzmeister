<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена | Putzmeister Россия - Официальный сайт");?>
<div class="wrap-container">
    <div class="error-b">
        <div class="text-error">
            <h2>Извините! Страница, которую Вы ищете, не может быть найдена</h2>
            <h4>возможные причины ошибки:</h4>
            <p>ошибка при наборе адреса страницы (URL);</p>
            <p>переход по неработающей или неправильной ссылке;</p>
            <p>отсутствие запрашиваемой страницы на сайте;</p>
        </div>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>