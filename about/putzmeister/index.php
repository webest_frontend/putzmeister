<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle('ООО "Путцмайстер-Рус" | Putzmeister Россия - Официальный сайт');
$APPLICATION->SetPageProperty("description", "Putzmeister в России начал свою историю в 1986 году с поставки 5 автобетононасосов.");?>
<section class="putzmeister_in_russia">
    <div class="wrap-container">
        <div class="page__content">
            <div class="page__side">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "side",
                    array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "side",
                        "COMPONENT_TEMPLATE" => "side",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "2",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "side",
                        "USE_EXT" => "Y"
                    ),
                    false
                ); ?>
            </div>
            <div class="page__main html_content">
                <h1>ООО "Путцмайстер-Рус"</h1>
                <p>
                    <a href="/images/pmr_code-of-conduct_ru.pdf">Кодекс поведения PM</a>
                    <br>
                    <br>
                    Российская часть истории Компании началась в 1986 году, с поставки пяти мощнейших автобетононасосов, в специальной противорадиационной комплектации,  и трех стационарных бетононасосов, для возведения саркофага над разрушенным реактором Чернобыльской АЭС.  <a href="http://www.putzmeister.ru/assets/uploaded/files/Putzmeister chernobyl.pdf" style="font-size: 10px;">подробнее Вы можете ознакомиться здесь</a>
                    <br>
                    <br>
                    Ещё раньше, в 1980 году, было начато и успешно завершено, в 1982, строительство трубы высотой 320 метров на Киришской ГРЭС в г. Кириши, Ленинградская область. На этой площадке применялся бетононасос Putzmeister BRA 2100.  <a href="http://www.putzmeister.ru/assets/uploaded/files/Putzmeister Kirishi.pdf" style="font-size: 10px;">подробнее здесь</a>
                    <br>
                    <br>
                    В 1993 году немецкая фирма «Putzmeister AG» открыла свое Представительство в Москве. Представительство отвечало за поставку <a href="http://www.putzmeister.ru/">бетонной техники</a> на территорию России и других стран СНГ.
                    <br>
                    <br>
                    Позже, 28 сентября 2004 года, была основана дочерняя компания немецкой фирмы "Putzmeister Concrete Pumps GmbH" - ООО "Путцмайстер-Рус". Компания стала полноценным центром снабжения и сервисного обслуживания техники Putzmeister в России.
                    <br>
                    <br>
                    За годы работы компания осуществила поставки таким крупным строительным фирмам как "Дон-Строй", "Моспромстрой", "Мосзарубежстрой", "Монолитстрой", "КРОСТ", "Жилстрой", "Тема", "Спецвысотстрой" и многим другим.
                    <br>
                    <br>
                    Техника Putzmeister отлично зарекомендовала себя на строительстве Храма Христа Спасителя, "Триумф-Паласа", комплексов "Алые Паруса", "Воробьевы Горы" и многих других объектов в России.
                    <br>
                    <br>
                    Сегодня компания ООО "Путцмайстер-Рус"  имеет возможность предложить решения для производства бетонной смеси, доставке, подачи и укладки смеси на объекте любой сложности. Компания Putzmesiter всегда готова предложить своим заказчикам растворную технику для реализации проектов любой сложности по внутренней и наружной отделке сооружений как готовых так и строящихся объектов.
                    <br>
                    <br>
                    Putzmeister в России, вот уже 20 лет, реализует высокотехнологичные решения для проектов по строительству тоннелей, для горно-обогатительных комбинатов и предприятий газонефтедобывающей отрасли, а также для экологических проектов различной сложности.
                    <br>
                    <br>
                    Специалисты компании оказывают консультации по проектам заказчика с целью правильного подбора оборудования. Сервисная служба компании обучает специалистов заказчика на местах эксплуатации техники.
                    <br>
                    <br>
                    <a href="/images/COUT.pdf">Результаты проведения специальной оценки условий труда сотрудников ООО "Путцмайстер-Рус"</a>
                </p>
                <h2>Наши преимущества:</h2>
                <ul>
                    <li>Современные технологии и ноу-хау, применяемые в производстве и сборке техники, позволяющие добиваться непревзойденного качества;</li>
                    <li>Низкие эксплуатационные расходы на технику;</li>
                    <li>Профессиональные консультации оказываемые специалистами компании в течении всего срока эксплуатации техники;</li>
                    <li>Сервисная поддержка. Гарантийное и Постгарантийное обслуживание техники; </li>
                    <li>Обучение операторов и других специалистов заказчика;</li>
                    <li>Оригинальные запчасти доступные, на складе в Москве и других городах России;</li>
                    <li>Оригинальное дополнительное оборудование увеличивающее производительность, удобство использования и срок службы техники.  </li>
                </ul>
                <h2>Фотогалерея предприятия:</h2>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.detail",
                    "company_slider",
                    array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BROWSER_TITLE" => "-",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "N",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "ELEMENT_CODE" => "",
                        "ELEMENT_ID" => "2018",
                        "FIELD_CODE" => array(
                            0 => "ID",
                            1 => "IBLOCK_ID",
                            2 => "",
                        ),
                        "IBLOCK_ID" => "14",
                        "IBLOCK_TYPE" => "content",
                        "IBLOCK_URL" => "",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "MESSAGE_404" => "",
                        "META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Страница",
                        "PROPERTY_CODE" => array(
                            0 => "",
                            1 => "GALLERY",
                            2 => "",
                        ),
                        "SET_BROWSER_TITLE" => "N",
                        "SET_CANONICAL_URL" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "STRICT_SECTION_CHECK" => "N",
                        "USE_PERMISSIONS" => "N",
                        "USE_SHARE" => "N",
                        "COMPONENT_TEMPLATE" => "company_slider"
                    ),
                    false
                ); ?>
            </div>
        </div>
    </div>
</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");