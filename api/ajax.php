<?
include_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
include_once './handlers.php';
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;

global $siteId;
global $request;
global $cpt;
global $error_msg;
global $captcha;

function handleRequest($request) {
    global $error_msg;
    global $cpt;
    if (Loader::includeModule('catalog') && Loader::includeModule('sale')) {
        switch ($request->getQuery('action')) {
            case 'add2basket':
                add2basketAjax($request);
                break;
            case 'login':
                login($request);
                break;
            case 'cancel_order':
                cancelOrder($request);
                break;
            default:
                $cpt->SetCode();
                echo json_encode([
                    "captcha" => $cpt->GetSID()
                ]);
                die();
        }
    } else {
        $error_msg = 'modules not loaded';
    }
}

$request = Application::getInstance()->getContext()->getRequest();
$siteId = Application::getInstance()->getContext()->getSite();
$cpt = new CCaptcha();
$captcha_sid = $request->getPost("captcha_sid");
$error_msg = 'сессия устарела';
if (check_bitrix_sessid()) {
    if($captcha_sid) {
        if($cpt->CheckCode($request->getPost("captcha_word"), $captcha_sid)) {
            handleRequest($request);
        } else {
            $error_msg = "неверный код";
        }
    } else {
        handleRequest($request);
    }
}
http_response_code(403);
$cpt->SetCode();
echo json_encode([
    "error" => $error_msg,
    "captcha" => $cpt->GetSID()
]);
