<?
include $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
\Bitrix\Main\Loader::includeModule("sale");
$cursor = CSaleLocation::GetList(
    array(),
    array("LID" => LANGUAGE_ID),
    false,
    false,
    ["CITY_NAME"]
);
$result = [];
while ($vars = $cursor->Fetch()) {
    $result[] = $vars;
}
echo json_encode($result);