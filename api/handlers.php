<?
use \Bitrix\Sale\Order;
use \Bitrix\Catalog\Product\Basket;

function add2basketAjax ($request) {
    global $APPLICATION;
    global $error_msg;
    $error_msg = 'invalid id';
    if($product_id = $request->getPost('pid')) {
        $fields = [
            'PRODUCT_ID' => $product_id,
            'QUANTITY' => 1
        ];
        $r = Basket::addProduct($fields);
        $error_msg = json_encode($r->getErrorMessages());
        if ($r->isSuccess()) {
            $APPLICATION->IncludeComponent(
                "bitrix:sale.basket.basket.line",
                "putzmeister",
                Array(
                    "HIDE_ON_BASKET_PAGES" => "Y",
                    "PATH_TO_BASKET" => SITE_DIR . "cart/",
                    "PATH_TO_ORDER" => "",
                    "PATH_TO_PERSONAL" => "",
                    "PATH_TO_PROFILE" => "",
                    "PATH_TO_REGISTER" => "",
                    "POSITION_FIXED" => "N",
                    "POSITION_HORIZONTAL" => "right",
                    "POSITION_VERTICAL" => "top",
                    "SHOW_AUTHOR" => "N",
                    "SHOW_DELAY" => "N",
                    "SHOW_EMPTY_VALUES" => "Y",
                    "SHOW_IMAGE" => "N",
                    "SHOW_NOTAVAIL" => "N",
                    "SHOW_NUM_PRODUCTS" => "Y",
                    "SHOW_PERSONAL_LINK" => "N",
                    "SHOW_PRICE" => "N",
                    "SHOW_PRODUCTS" => "N",
                    "SHOW_SUMMARY" => "N",
                    "SHOW_TOTAL_PRICE" => "N"
                )
            );
            die();
        }
    }
}

function login() {
    global $USER;
    global $request;
    global $cpt;
    $email = $request->getPost('email');
    $error = ['email' => 'неверный email'];
    if(check_email($email)) {
        $user = CUser::GetList(
            ($by = "name"),
            ($order = "desc"),
            ['EMAIL' => $email]
        )->Fetch();
        $error = ['email' => 'Пользователь с таким email не найден'];
        if($user) {
            $password = $request->getPost('password');
            $arAuthResult = $USER->Login($user['LOGIN'], $password);
            $error = ['password' => 'Неверный пароль'];
            if (!isset($arAuthResult['TYPE']) && !$arAuthResult['TYPE'] == 'ERROR') {
                echo json_encode([
                    "reload" => 1,
                    "captcha" => $cpt->GetSID()
                ]);
                die();
            }
        }
    }
    $cpt->SetCode();
    http_response_code(401);
    echo json_encode([
        "error" => $error,
        "captcha" => $cpt->GetSID()
    ]);
}

function cancelOrder($request) {
    global $error_msg;
    $order_id = $request->getPost("order_id");
    $reason = $request->getPost("reason");
    $error_msg = "Неверный id заказа";
    if($order_id) {
        $order = Order::load($order_id);
        $error_msg = "Заказ не найден";
        if($order) {
            $error_msg = "Заказ уже отменен";
            if($order->getField('CANCELED') != "Y") {
                $r = $order->setField('CANCELED', 'Y');
                $error_msg = $r->getErrorMessages();
                if ($reason) {
                    $order->setField('REASON_CANCELED', $reason);
                }
                if ($r->isSuccess()) {
                    $r = $order->save();
                    $error_msg = $r->getErrorMessages();
                    if ($r->isSuccess()) {
                        echo json_encode([
                            'message' => 'Заказ отменен',
                            'reload' => 1,
                            'url' => parse_url($_SERVER["HTTP_REFERER"])["path"],
                        ]);
                        die();
                    }
                }
            }
        }
    }
}
