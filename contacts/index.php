<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты | Putzmeister Россия - Официальный сайт");
$APPLICATION->SetPageProperty("description", "Все контакты официального представительства Putzmeister в России");
\Bitrix\Main\Page\Asset::getInstance()->addJs('//api-maps.yandex.ru/services/constructor/1.0/js/?sid=igTVJJT8kcWQIJCHy98c1sgEOQo-bcz1&amp;id=map--contacts&amp;height=344'); ?>
<section class="contacts">
    <div class="wrap-container">
        <div style="position: relative;">
            <span style="background-color: #ececec;
                height: 2px;
                width: 100%;
                display: inline-block;
                vertical-align: middle;"></span>
            <noindex style="position: absolute;
                right: 0;
                background-color: white;
                padding: 0 0 0 20px;
                display: inline-block;
                top: calc(50% - 15px);">
                <a href="#" style="background: url(<?=SITE_TEMPLATE_PATH."/images/print.png";?>) left/30px no-repeat;padding: 10px 10px 10px 38px;" onclick="return window.print();">Распечатать</a>
            </noindex>
        </div>
        <div class="contacts__page">
            <div class="contacts__main">
                <div class="contact-info">
                    <div itemscope="" itemtype="http://schema.org/LocalBusiness">
                        <h1 class="main-title" itemprop="name">ООО "Путцмайстер-Рус"</h1>
                        <div class="address" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">Адрес (фактический и юридический):<br> <span itemprop="postalCode">129343</span>, <span itemprop="addressCountry">Россия</span>, <span itemprop="addressLocality">г. Москва</span>, <span itemprop="streetAddress">ул. Уржумская, 4, стр. 31</span></div>
                        <div class="phone">
                            <dl>
                                <dt>Телефон:</dt>
                                <dd>+7 (800) 707-19-58 &nbsp;&nbsp;&nbsp;<span itemprop="telephone">+7 (495) 775-22-37</span>
                                </dd>
                            </dl>
                            <dl>
                                <dt>Факс:</dt>
                                <dd><span itemprop="faxNumber">+7 (495) 775-22-34</span>
                                </dd>
                            </dl>
                        </div>
                        <div class="openingHours">
                            <meta itemprop="openingHours" content="Mo-Fr 09:00-18:00">
                            ПН-ПТ: с 8:00 до 18:00
                        </div>
                        <div itemprop="department" itemscope="" itemtype="http://schema.org/LocalBusiness">
                            <h3 itemprop="name">Запасные части, сервисное&nbsp;обслуживание:</h3>
                            <div class="phone">
                                <dl>
                                    <dt>Телефон:</dt>
                                    <dd>+7 (800) 707-19-58 &nbsp;&nbsp;&nbsp;<span itemprop="telephone">+7 (495) 775-22-37</span>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>Факс:</dt>
                                    <dd>+7 (495) 775-22-49</dd>
                                </dl>
                            </div>
                            <div class="openingHours">
                                <meta itemprop="openingHours" content="Mo-Fr 09:00-18:00">
                                ПН-ПТ: с 8:00 до 18:00
                            </div>
                            <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                                <meta itemprop="addressLocality" content="г. Москва">
                                <meta itemprop="streetAddress" content="ул. Уржумская, 4, стр. 31">
                            </div>
                        </div>
                        <div class="mail">
                            <a itemprop="email" href="mailto:info@putzmeister.ru?subject=Письмо с сайта">info@putzmeister.ru</a>
                        </div>
                        <div class="rkb">
                            <p>ИНН: <span itemprop="taxID">7708538166</span> &nbsp;&nbsp; ОГРН: 1047796721873</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contacts__side">
                <div id="map--contacts"></div>
            </div>
        </div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "contacts",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "NAME",
                    1 => "",
                ),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "13",
                "IBLOCK_TYPE" => "content",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "20",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "EMAIL",
                    1 => "POSITION",
                    2 => "NAME",
                    3 => "H1",
                    4 => "PHONE",
                    5 => "",
                ),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "NAME",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "STRICT_SECTION_CHECK" => "N",
                "COMPONENT_TEMPLATE" => "contacts"
            ),
            false
        ); ?>
    <div class="contacts__footer">
        <div>
            <a href="/regions/">Региональные дилеры</a>
        </div>
        <div>
            <a class="modal_trigger--consult" href="javascript:void(0)" onclick="return false;" rel="nofollow" data-suffix="Обратный звонок">Форма обратной связи</a>
        </div>
    </div>
</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");