<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Бетонная техника и бетононасосы от производителя Putzmeister");
$APPLICATION->SetPageProperty("description", "Продажа бетонной техники и бетононасосов Putzmeister по выгодным ценам. Официальный сайт Путцмайстер - производителя бетононасосов в Москве и Московской области. Тел.: (495) 775-22-37.");
CJSCore::Init('video_player');?>
<section class="banner_top">
    <video
        id="v_player"
        src="<?="http://".$_SERVER["SERVER_NAME"]?>/upload/media/technic.mp4"
        muted="true"
        loop="true"
        preload="metadata"
        poster="<?=SITE_TEMPLATE_PATH?>/images/video_poster.png"
        autoplay="true"
        type="video/mp4">
        <source src="/upload/media/technic.mp4">
    </video>
    <div class="wrap-container">
        <div class="banner_top__front">
            <div class="banner_top__nav">
                <div class="banner_top__nav_item hovered" data-source="technic">
                    <div class="nav_item_body">Техника</div>
                </div>
                <div class="banner_top__nav_item service"  data-source="service">
                    <div class="nav_item_body">Сервис</div>
                </div>
                <div class="banner_top__nav_item spares" data-source="spares">
                    <div class="nav_item_body">Запчасти</div>
                </div>
                <div class="banner_top__position">
                    <div class="banner_top__position_pointer"></div>
                </div>
            </div>
            <p>
                Производство, поставка и обслуживание <a href="/catalog/betononasosy/">бетононасосов</a>, <a href="/catalog/avtobetononasosy/">автобетононасосов</a>, <a href="/catalog/rastvoronasosy/">растворонасосов</a>, <a href="/catalog/rastvoronasosy/shtukaturnie-stancii/">штукатурных станций</a>, <a href="/catalog/tonelnoe_oborudovanie/">торкрет оборудования</a>, <a href="/catalog/shlamoviye-nasosy/">шламовых насосов</a>, <a href="/catalog/avtobetonosmesiteli/">автобетоносмесителей</a>, БСУ\БРУ. <a href="/service/">Сервисные центры</a>. Склады оригинальных <a href="/service/spares/">запчастей и принадлежностей</a>.
            </p>
            <svg class="banner_top__scroll_sign">
                <use href="<?=SITE_TEMPLATE_PATH?>/images/icons/sprite.svg#arrow_down"></use>
            </svg>
        </div>
    </div>
</section>
<section class="catalog">
    <div class="wrap-container">
        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "catalog",
            Array(
                "VIEW_MODE" => "TEXT",
                "SHOW_PARENT_NAME" => "Y",
                "IBLOCK_TYPE" => "",
                "IBLOCK_ID" => "2",
                "SECTION_ID" => "",
                "SECTION_CODE" => "",
                "SECTION_URL" => "",
                "COUNT_ELEMENTS" => "N",
                "TOP_DEPTH" => "1",
                "SECTION_FIELDS" => "",
                "SECTION_USER_FIELDS" => "",
                "ADD_SECTIONS_CHAIN" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_NOTES" => "",
                "CACHE_GROUPS" => "Y"
            )
        );?>
    </div>
</section>
<section class="service_index">
    <div class="wrap-container">
        <h2 class="service__header">Сервис и услуги</h2>
        <div class="service__tiles">
            <div>
                <div class="service__tile cover-link filled">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/16.png" alt="">
                    <span>Заказ запчастей</span>
                    <div class="service__tile_label">
                        <svg>
                            <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#arrow-right"></use>
                        </svg>
                    </div>
                    <a href="/service/spares/"></a>
                </div>
            </div>
            <div>
                <div class="service__tile cover-link filled">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/18.png" alt="">
                    <span>Документация</span>
                    <div class="service__tile_label">
                        <svg>
                            <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#arrow-right"></use>
                        </svg>
                    </div>
                    <a href="/service/documentation/"></a>
                </div>
            </div>
            <div>
                <div class="service__tile cover-link filled">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/19.png" alt="">
                    <span>Запрос на технику</span>
                    <div class="service__tile_label">
                        <svg>
                            <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#arrow-right"></use>
                        </svg>
                    </div>
                    <a href="/service/order/"></a>
                </div>
            </div>
            <div>
                <div class="service__tile cover-link filled">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/21.png" alt="">
                    <span>PM Finance</span>
                    <div class="service__tile_label">
                        <svg>
                            <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#arrow-right"></use>
                        </svg>
                    </div>
                    <a href="/service/pm-finance/"></a>
                </div>
            </div>
            <div>
                <div class="service__tile cover-link filled">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/20.png" alt="">
                    <span>Б/у техника</span>
                    <div class="service__tile_label">
                        <svg>
                            <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#arrow-right"></use>
                        </svg>
                    </div>
                    <a href="/service/bu_tehnika/"></a>
                </div>
            </div>
            <div>
                <div class="service__tile cover-link filled">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/17.png" alt="">
                    <span>Ремонт</span>
                    <div class="service__tile_label">
                        <svg>
                            <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#arrow-right"></use>
                        </svg>
                    </div>
                    <a href="/service/remont/"></a>
                </div>
            </div>
            <div>
                <div class="service__tile cover-link filled">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/22.png" alt="">
                    <span>Запись на сервисное обслуживание и ремонт</span>>
                    <div class="service__tile_label">
                        <svg>
                            <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#arrow-right"></use>
                        </svg>
                    </div>
                    <a class="modal_trigger--service" href="javascript:void(0)" rel="nofollow" onclick="return false;" data-suffix="Cервисное обслуживание и ремонт"></a>
                </div>
            </div>
            <div>
                <div class="service__tile cover-link filled">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/15.png" alt="">
                    <span>Где купить</span>
                    <div class="service__tile_label">
                        <svg>
                            <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#arrow-right"></use>
                        </svg>
                    </div>
                    <a href="/regions/"></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="concrete">
    <div class="wrap-container">
        <h2 class="concrete__header">Бетонная техника и бетононасосы Putzmeister</h2>
        <p>
            Компания Putzmeister осуществляет передовые разработки, производство, реализацию и техническое обслуживание широкого спектра современной бетонной техники с 1958 года. Широкая география охватывает Германию, Россию, Великобританию, Бразилию, США, Японию, Италию, Испанию, Францию и десятки других стран. Корпорация включает более 20 дочерних предприятий по всему миру, в штате которых свыше 3000 инженеров, конструкторов, технологов и других квалифицированных специалистов. Сегодня немецкая компания Putzmeister является одним из лидирующих производителей бетононасосов, объединяет 11 крупных заводов и дилерскую сеть с представительствами в более чем 154 странах. Линейка оборудования включает широкий модельный ряд бетонной, растворной, шахтной и туннельной техники, торкрет оборудования, шламовых насосов, транспортеров и бункеров. Региональное подразделение Putzmeister в России существует с 1993 года.
        </p>
        <div class="concrete__tiles">
            <div>
                <div class="concrete__tile">
                    <div class="concrete__tile_preview">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/23.png">
                    </div>
                    <h2 class="concrete__tile_title">Строительная техника</h2>
                    <div>
                        Компания осуществляет продажу широкого спектра современной техники и специализированного оборудования. В каталоге представлен полный модельный ряд бетононасосов, бетоноводы и распределительные стрелы, штукатурные станции, торкрет установки, телескопические транспортеры, шламовые насосы, шахтное и тоннельное оборудование, а также автобетоносмесители и мобильные бетонные заводы.
                    </div>
                </div>
            </div>
            <div>
                <div class="concrete__tile">
                    <div class="concrete__tile_preview">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/24.png">
                    </div>
                    <h2 class="concrete__tile_title">Запчасти и аксессуары</h2>
                    <div>
                        В наличии оригинальные комплектующие, дополнительное оборудование и аксессуары для бетононасосов и другой растворной, тоннельной и бетонной техники. Вниманию заказчиков современные системы дистанционного управления, надежные бетоноводы, шланги, крепежи, переходники, приспособления для очистки, шиберы, пусковые смеси для бетононасосов, гидравлическое оборудование и другая продукция от завода производителя.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="advantages">
    <div class="wrap-container">
        <h2 class="advantages__header">Преимущества сотрудничества</h2>
        <div class="advantages__tiles">
            <div>
                <div class="advantages__tile">
                    <svg>
                        <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#thumb_up"></use>
                    </svg>
                    <h3 class="advantages__title">Широкий выбор</h3>
                    <div class="advantages__anons">
                        В ассортименте представлены новые бетононасосы и другая строительная техника, запасные части и аксессуары для реализации спектра строительных работ, вне зависимости от объема и степени сложности проекта.
                    </div>
                </div>
            </div>
            <div>
                <div class="advantages__tile">
                    <svg>
                        <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#reward"></use>
                    </svg>
                    <h3 class="advantages__title">Гарантия качества</h3>
                    <div class="advantages__anons">
                        Все мировые заводы–производители бетононасосов, специализированной техники и дополнительного оборудования Putzmeister работают по принятым в компании нормам и правилам. Продукция производится в соответствии со стандартом качества ISO 9001/2008 с применением самых передовых технологий, должное значение уделяется защите окружающей среды. На все бетононасосы, растворонасосы, шахтовое, тоннельное оборудование и штукатурные станции, а также запчасти к ним предоставляется фирменная гарантия Putzmeister.
                    </div>
                </div>
            </div>
            <div>
                <div class="advantages__tile">
                    <svg>
                        <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#man_say"></use>
                    </svg>
                    <div class="advantages__title">Сервисная поддержка</div>
                    <div class="advantages__anons">
                        Специалисты сервисной службы Putzmeister обеспечат ввод в эксплуатацию бетононасосов и другого оборудования, а также обучение представителей заказчика правилам эксплуатации. На протяжении всего срока службы насосов предоставят высококвалифицированную техническую и информационную поддержку.
                    </div>
                </div>
            </div>
        </div>
        <div class="advantages__banner">Для получения более подробной информации о бетононасосах, торкрет установках и другом бетонном, растворном и тоннельном оборудовании, запчастях и аксессуарах, а также оформления заказа следует обратиться к специалистам компании Putzmeister по телефону 8 800 707 1958 или 8 (495) 775-22-37 в будние дни с 08:00 до 18:00 (по московскому времени).</div>
        <h2 class="advantages__header">Putzmeister в России</h2>
        <div class="advantages__tiles--putz">
            <div>
                <div class="advantages__tile--putz">
                    ООО "Путцмайстер-Рус" - дочерняя компания немецкой фирмы Putzmeister Concrete Pumps GmbH.
                    <div class="checkmark"></div>
                </div>
            </div>
            <div>
                <div class="advantages__tile--putz">
                    Сервисная служба и система снабжения запасными частями со склада в Москве.
                    <div class="checkmark"></div>
                </div>
            </div>
            <div>
                <div class="advantages__tile--putz">
                    Дилерская сеть на территории России, дилеры в Казахстане, Белоруссии, Грузии и Украине.
                    <div class="checkmark"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="useful">
    <div class="wrap-container">
        <h2 class="useful__header">Полезная информация</h2>
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "info",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "NAME",
                    1 => "PREVIEW_PICTURE",
                    2 => "",
                ),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "15",
                "IBLOCK_TYPE" => "content",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "4",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "EMAIL",
                    2 => "POSITION",
                    3 => "NAME",
                    4 => "PHONE",
                    5 => "",
                ),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "NAME",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "STRICT_SECTION_CHECK" => "N",
                "COMPONENT_TEMPLATE" => "info"
            ),
            false
        ); ?>
        <a class="btn btn--yellow" href="/info">Все статьи</a>
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "slider_main",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "N",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "NAME",
                    1 => "PREVIEW_TEXT",
                    2 => "PREVIEW_PICTURE",
                    3 => "",
                ),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "19",
                "IBLOCK_TYPE" => "content",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "999",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "LINK_URL",
                    1 => "LINK_TEXT",
                    2 => "DARK_THEME",
                    3 => "",
                ),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "ID",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "STRICT_SECTION_CHECK" => "N",
                "COMPONENT_TEMPLATE" => "slider_main"
            ),
            false
        ); ?>
    </div>
</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>