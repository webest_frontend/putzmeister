<?
$aMenuLinks = Array(
    Array(
        "Полезная информация",
        "/info/",
        Array(),
        Array('ROOT-ITEM' => true),
        ""
    ),
    Array(
        "Регионы",
        "/regions/",
        Array(),
        Array('ROOT-ITEM' => true),
        ""
    ),
    Array(
        "Новости",
        "/info/news/",
        Array(),
        Array('ROOT-ITEM' => true),
        ""
    ),
    Array(
        "PM Finance",
        "/service/pm-finance/",
        Array(),
        Array('ROOT-ITEM' => true),
        ""
    ),
    Array(
        "Журнал Putzmeister Post",
        "/putzmeister-post/",
        Array(),
        Array("PM_POST" => '/images/PM_POST.jpg'),
        ""
    ),
    Array(
        "Google Play",
        "https://play.google.com/store/apps/details?id=com.putzmeister.experts",
        Array(),
        Array("GOOGLE_PLAY" => '/images/icons/android.png'),
        ""
    ),
    Array(
        "App Store",
        "https://itunes.apple.com/us/app/putzmeister-experts/id1370410217?fbclid=IwAR3SG-b_9tt1BWLlcBXHm0sTDbLmn4rxEXwM1oeh6Kb-4JpeUdTeFdzcGtomt=8",
        Array(),
        Array("APP_STORE" => '/images/icons/ios.png'),
        ""
    )
);