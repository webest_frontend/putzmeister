window.cookieconsent.initialise({
    "palette": {
        "popup": {
            "background": "#FFFFFF"
        },
        "button": {
            "background": "#f1b900"
        }
    },
    "content": {
        "href": "/politika_obrabotki_personalnyh_dannyh/",
        header: 'Cookies used on the website!',
        message: 'Этот сайт использует cookies',
        dismiss: 'Ok',
        allow: 'Разрешить',
        deny: 'Отклонить',
        link: 'Подробнее',
        close: '&#x274c;',
        policy: 'Cookie Policy',
        target: '_blank'
    }
});