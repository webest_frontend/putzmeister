BX(function() {
    BX.namespace('BX.Webest');
    BX.Webest.SelectInput = function (container) {
        this.container = container;
        this.inputs = this.container.querySelectorAll("input");
        this.selected = this.container.querySelector(".select__selected");
        this.list = this.container.querySelector(".select__list");
    };
    BX.Webest.SelectInput.prototype = {
        get name() {
            return this.inputs.item(0).name;
        },
        get value() {
            var checked, value;
            value = "";
            if (checked = Array.from(this.inputs).find(i => i.checked)) {
                value = checked.value;
            }
            return value;
        },
        set value(val) {
            this.inputs.forEach(i => {
                i.checked = i.value === val;
            });
        },
        reset: function () {
            this.inputs.forEach(i => {
                i.checked = i.hasAttribute("checked");
            });
            this.selected.innerText = this.list.querySelector(".list__item").innerText.trim();
            BX.removeClass(this.component, "invalid");
        }
    };
});