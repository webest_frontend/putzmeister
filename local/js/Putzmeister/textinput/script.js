BX(function() {
    BX.namespace('BX.Webest');
    BX.Webest.TextInput = function (container) {
        this.container = container;
        this.input = this.container.querySelector('input');
        this.errorString = this.container.querySelector('.text__error');
        this.throttle = "";
        this.init.call(this);
    };
    BX.Webest.TextInput.prototype = {
        init: function() {
            this.input.addEventListener('blur', this.validateInput.bind(this));
            this.input.addEventListener('keyup', this._onKeyUp.bind(this));
            return this;
        },
        _onKeyUp: function(e) {
            clearTimeout(this.throttle);
            this.throttle = setTimeout(this.validateInput.bind(this), 200);
        },
        validateInput: function() {
            var error, isInvalid;
            error = '';
            isInvalid = false;
            if (!this.input.checkValidity()) {
                error = this.input.validationMessage;
                isInvalid = true;
            }
            this.container.classList.toggle('invalid', isInvalid);
            this.container.classList.toggle('valid', !isInvalid);
            this.errorString.innerText = error;
        },
        reset: function() {
            this.container.classList.remove('valid', ['invalid']);
        }
    }
});