BX(function() {
    BX.namespace('BX');
    BX.Video_player = function(container) {
        this.banner = container;
        this.mobile = true;
        this.delayTimer = '';
        this.prevSource = "technic";
        this.prevTime = {
            technic: 0,
            spares: 0,
            service: 0
        };
        this.video_container = BX('v_player');
        this.banner_items = this.banner.querySelectorAll('.banner_top__nav_item');
        this._init.call(this);
    };
    BX.Video_player.prototype = {
        _init: function () {
            var self = this;
            this.mql = window.matchMedia('(max-width: 768px)');
            BX.bind(this.mql, 'change', this._screenTest.bind(this));
            this.mql.dispatchEvent(new Event('change'));
            this.banner_items.forEach(i => {
                BX.bind(i, 'mouseenter', BX.proxy(self._onHover, self));
            });
        },
        _onHover: function (e) {
            var target, banner_background, source;
            if(target = e.target.closest("[data-source]")) {
                this.prevTime[this.prevSource] = this.video_container.currentTime;
                source = target.dataset.source;
                this.prevSource = source;
                banner_background = "banner--" + source;
                this.banner_items.forEach(f => {
                    f.classList.toggle('hovered', Object.is(f, target));
                });
                this.banner.classList.remove('banner--service', ['banner--spares']);
                banner_background && BX.addClass(this.banner, banner_background);
                if (!this.mobile && !this.video_container.currentSrc.includes(source)) {
                    clearTimeout(this.delayTimer);
                    this.delayTimer = setTimeout(BX.delegate(function () {
                        this.video_container.src = this.video_container.src.replace(/\w+(\.\w{3})$/, source + "$1");
                        this.video_container.currentTime = this.prevTime[source];
                    }, this), 200);
                }
            }
        },
        _screenTest(e) {
            if (e.target.matches) {
                this.mobile = true;
                this.video_container.pause();
            } else {
                this.mobile = false;
                this.video_container.play();
            }
        }
    };
    new BX.Video_player(document.querySelector(".banner_top"));
});