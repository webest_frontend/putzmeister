BX.namespace('BX.Webest');
BX.Webest.YandexMaps = function (container, {zoom = 12, center=[36.0, 50.0], placemarks = [], template = '<div></div>'}) {
    this.container = container;
    this.iconPath = '/images/lp-p.png';
    this.map = {};
    this.zoom = zoom;
    this.center = center;
    this.placemarks = placemarks;
    this.template = template;
};

BX.Webest.YandexMaps.prototype = {
    init: function () {
        const self = this;
        window.ymaps && this.container.id && window.ymaps.ready(function () {
            self.template = ymaps.templateLayoutFactory.createClass(self.template);
            self.map = new ymaps.Map(
                self.container.id,
                {
                    center: self.center,
                    zoom: self.zoom,
                    controls: ['zoomControl']
                }
            );
            self.placemarks.forEach(e => {
                if (e.STATUS === 'dealer') {
                    self.iconPath = '/images/lp-d.png';
                } else if (e.STATUS === 'main') {
                    self.iconPath = '/images/lp-main.png';
                }
                self.map.geoObjects.add(new ymaps.Placemark(
                    e.COORDINATES.map(e => parseFloat(e)),
                    {
                        hintContent: e.NAME || '',
                        name: e.NAME,
                        address: e.ADDRESS,
                        phone: e.PHONE,
                        email: e.EMAIL,
                        link: e.LINK
                    },
                    {
                        hideIconOnBalloonOpen: false,
                        balloonContent: self.name,
                        balloonContentLayout: self.template,
                        iconLayout: 'default#image',
                        iconImageHref: self.iconPath,
                        iconImageSize: [32, 48]
                    }
                ));
            });
        });
        return this;
    },
    setCenter: function (coordinates = this.center) {
        var castedCoordinates = coordinates.map(e => parseFloat(e));
        try {
            this.map.setCenter(castedCoordinates);
        } catch (e) {}
    }
};