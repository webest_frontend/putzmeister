<?
$arJsConfig = [
    "tabs" => [
        "js" => [
            "/local/js/Putzmeister/tabs/bootstrap.bundle.min.js",
            "/local/js/Putzmeister/tabs/bootstrap.min.js"
        ],
        "css" => [
            "/bitrix/css/main/bootstrap_v4/bootstrap.min.css",
            "/local/js/Putzmeister/tabs/bootstrap-theme.css"
        ],
        "skip_core" => true
    ],
    "slick_carousel" => [
        "js" => "/local/js/Putzmeister/slick/slick.min.js",
        "css" => ["/local/js/Putzmeister/slick/slick.css", "/local/js/Putzmeister/slick/slick-theme.css"],
        "rel" => ["jquery2"],
        "skip_core" => true
    ],
    "owl_carousel" => [
        "js" => "/local/js/Putzmeister/owlcarousel/owl.carousel.min.js",
        "css" => [
            "/local/js/Putzmeister/owlcarousel/owl.carousel.min.css",
            "/local/js/Putzmeister/owlcarousel/owl.theme.default.min.css",
            "/local/js/Putzmeister/owlcarousel/owl.theme.custom.css"
        ],
        "rel" => ["jquery2"],
        "skip_core" => true
    ],
    "magnific_popup" => [
        "js" => "/local/js/Putzmeister/magnificpopup/jquery.magnific-popup.min.js",
        "css" => [
            "/local/js/Putzmeister/magnificpopup/magnific-popup.css",
            "/local/js/Putzmeister/magnificpopup/theme.css"
        ],
        "rel" => ["jquery2"],
        "skip_core" => true
    ],
    "video_player" => [
        "js" => [
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/bitrix/player/js/fileman_player.js",
            "/local/js/Putzmeister/video_player/script.js"
        ],
        "css" => "/local/js/Putzmeister/video_player/style.css",
        "rel" => ["player"],
        "skip_core" => true
    ],
    "selectinput" => [
        "js" => "/local/js/Putzmeister/selectinput/script.js",
        "css" => "/local/js/Putzmeister/selectinput/style.css",
        "skip_core" => true
    ],
    "textinput" => [
        "js" => "/local/js/Putzmeister/textinput/script.js",
        "css" => "/local/js/Putzmeister/textinput/style.css",
        "skip_core" => true
    ],
    "ymaps" => [
        "js" => [
            "https://api-maps.yandex.ru/2.1/?apikey=21fcbf9e-f597-45bb-ba61-6bcb21081e5e&lang=ru_RU",
            "/local/js/Putzmeister/ymaps/script.js"
        ],
        "skip_core" => true
    ],
    "cookiesconsent" => [
        "js" => [
            "https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js",
            "/local/js/Putzmeister/cookiesconsent/script.js"
        ],
        "css" => [
            "/local/js/Putzmeister/cookiesconsent/cookieconsent.min.css",
            "/local/js/Putzmeister/cookiesconsent/theme.css",
        ],
        "skip_core" => true
    ]
];
