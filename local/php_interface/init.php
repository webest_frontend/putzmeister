<?
include_once \Bitrix\Main\Application::getDocumentRoot().'/local/php_interface/extensions.php';
foreach ($arJsConfig as $ext => $arExt) {
    CJSCore::RegisterExt($ext, $arExt);
}

CJSCore::Init([
    'jquery2',
    'magnific_popup',
    'masked_input',
    'selectinput',
    'textinput',
    'cookiesconsent'
]);