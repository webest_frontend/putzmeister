<?
include $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("sale");

function extractFilesFromJson() {
	$cursor = CIBlockElement::GetList(
	    [],
        ["IBLOCK_ID" => 4],
		false,
		false,
        [
            "IBLOCK_ID",
			"ID"
        ]
    );
    $el = new CIBlockElement;
	while($buf = $cursor->Fetch()) {
		$json_images=json_decode($buf['PROPERTY_IMAGES_JSON_VALUE'], true);
		if(!empty($json_images)) {
			$arFiles=array_map(function($i) {
				$file = CFile::MakeFileArray('/upload'.$i['src']);
				if($file) {
						return ["VALUE" => $file, "DESCRIPTION" => $i['altName']];
				} else {
					return [];
				}
			}, $json_images);
			CIBlockElement::SetPropertyValuesEx(
				$buf['ID'],
				4,
				['GALLERY' => $arFiles]
			);
			$file = CFile::MakeFileArray('/upload'.$json_images[0]['src']);
			if(!$el->Update(
				$buf['ID'],
				[
					'DETAIL_PICTURE' => $file,
					'PREVIEW_PICTURE' => $file
				]
			)) {
				echo $buf['ID'].': '.$el->LAST_ERROR.PHP_EOL;
			};
		}
	}
	CUtil::PhpToJSObject()
}

function extractSectionFilesFromJSON () {
	$cursor = CIBlockElement::GetList(
		[],
		[
			"IBLOCK_ID" => 2,
			"?NAME" => "dummy%"
		],
		false,
		false,
		[
			"IBLOCK_ID",
			"ID",
			"PROPERTY_gallery_json",
			"PROPERTY_docs_json",
			"PROPERTY_shared_docs_json"
		]
	);
    $sec = new CIBlockSection();
	while($buf = $cursor->Fetch()) {
		$sectionId = CIBlockElement::GetElementGroups($buf['ID'], true)->Fetch()['ID'];
		$json_images=json_decode($buf['PROPERTY_GALLERY_JSON_VALUE'], true);
		$json_docs=json_decode($buf['PROPERTY_DOCS_JSON_VALUE'], true);
		$json_shared_docs=json_decode($buf['PROPERTY_SHARED_DOCS_JSON_VALUE'], true);
		if(!empty($json_images)) {
			$file = CFile::MakeFileArray('/upload'.$json_images[0]['src']);
			if (!$sec->Update(
				$sectionId,
				array(
					"PICTURE" => $file,
					"DETAIL_PICTURE" => $file
				),
				true,
				true,
				true
			)) {
				echo $buf['ID'].': '.$sec->LAST_ERROR.PHP_EOL;
			};
		}
		if (!empty($json_docs)) {
			$arFiles=array_map(function($i) {
				$file = CFile::MakeFileArray('/upload'.$i['src']);
				if($file) {
                    CFile::UpdateDesc($file, $i['altName']);
					return $file;
				} else {
					return false;
				}
			}, $json_docs);
			if (!$sec->Update(
				$sectionId,
				array(
					"UF_DOCS" => $arFiles,
				),
				true,
				true,
				true
			)) {
				echo $buf['ID'].': '.$sec->LAST_ERROR.PHP_EOL;
			};
		}
		if (!empty($json_shared_docs)) {
			$arFiles=array_map(function($i) {
				$file = CFile::MakeFileArray('/upload'.$i['src']);
				if($file) {
                    CFile::UpdateDesc($file, $i['altName']);
					return $file;
				} else {
					return false;
				}
			}, $json_shared_docs);
			if (!$sec->Update(
				$sectionId,
				array(
					"UF_SHARED_DOCS" => $arFiles,
				),
				true,
				true,
				true
			)) {
				echo $buf['ID'].': '.$sec->LAST_ERROR.PHP_EOL;
			};
		}
	}
}

function sitemapUrlsGenerate() {
	include $_SERVER["DOCUMENT_ROOT"].'/upload/sitemap/subdomains.php';
	$index = simplexml_load_file($_SERVER["DOCUMENT_ROOT"].'/upload/sitemap/sitemap.xml');
	foreach ($arCities as $subdomain) {
		$data = simplexml_load_file($_SERVER["DOCUMENT_ROOT"].'/upload/sitemap/sitemap_iblock_6.xml');
		foreach ($data as $url) {
			$url->loc = preg_replace('/catalog/', $subdomain.'/catalog', $url->loc);
		}
		$xmlUrlDocument = new DOMDocument('1.0');
		$xmlUrlDocument->preserveWhiteSpace = false;
		$xmlUrlDocument->formatOutput = true;
		$xmlUrlDocument->loadXML($data->asXML());
		$formattedXML = $xmlUrlDocument->saveXML();
		$urlSitemapPath = 'sitemap'.$subdomain.'_iblock_6.xml';
		$fp = fopen($_SERVER["DOCUMENT_ROOT"].'upload/sitemap/'.$urlSitemapPath,'w+');
		fwrite($fp, $formattedXML);
		fclose($fp);
		$sitemap = $index->addChild('sitemap');
		$sitemap->addChild('loc', 'https://inbenzo.ru/sitemap/'.$urlSitemapPath);
		$sitemap->addChild('lastmod', '2019-04-25T12:29:43+03:00');
	}
	$xmlDocument = new DOMDocument('1.0');
	$xmlDocument->preserveWhiteSpace = false;
	$xmlDocument->formatOutput = true;
	$xmlDocument->loadXML($index->asXML());
	$formattedXML = $xmlDocument->saveXML();
	$fp = fopen($_SERVER["DOCUMENT_ROOT"].'/upload/sitemap/sitemap.xml','w+');
	fwrite($fp, $formattedXML);
	fclose($fp);
}

function parseRelations () {
	$index = simplexml_load_file($_SERVER["DOCUMENT_ROOT"].'/upload/import/29152.xml');
	$objects = $index->pages->xpath("page[@type-id='138'] | page[@type-id='86']");
	echo count($objects);
	$fp = fopen($_SERVER["DOCUMENT_ROOT"].'/upload/test.csv','w+');
	$data = [];
	foreach($objects as $object) {
		$values = [];
		foreach($object->properties->xpath("group/property[@name='primen']")[0]->value->page as $model) {
			$values[] = (string) $model->name;
		}
		$data = [
			(string) $object->attributes()->id,
			join('~', $values)
		];
		fseek($fp, 0, SEEK_END);
		fputcsv($fp, $data, ';');
	}
	fclose($fp);
}

function parseRelatedArticles () {
    $index = simplexml_load_file($_SERVER["DOCUMENT_ROOT"].'/upload/29152.xml');
    $objects = $index->pages->xpath("page[@type-id='138'] | page[@type-id='86']");
    $values = [];
    foreach($objects as $object) {
        $articles = [];
        $name = $object->properties->xpath("group/property[@id='2']")[0]->value;
        foreach($object->properties->xpath("group/property[@name='related_articles']")[0]->value->page as $article) {
            $articles[] = (string) $article->attributes()->link;
        }
        if(!empty($articles)) {
            $values[] = [
                'name' => (string) $name,
                'articles' => $articles
            ];
        }
    }
    print_r($values);
}

function loadRelations () {
	$cursorModels = CIBlockElement::GetList(
		[],
		[
			"IBLOCK_ID" => 2
		],
		false,
		false,
		[
			"IBLOCK_ID",
			"ID",
			"NAME"
		]
	);
	$arEquipment = [];
	while($buf = $cursorModels->Fetch()) {
		$arEquipment[] = $buf;
	}

	$cursor = CIBlockElement::GetList(
		[],
		[
			"IBLOCK_ID" => 4
		],
		false,
		false,
		[
			"IBLOCK_ID",
			"ID",
			"PROPERTY_RELATIONS_JSON"
		]
	);
	while($buf = $cursor->Fetch()) {
		$json_models = explode('~', $buf['PROPERTY_RELATIONS_JSON_VALUE']);
		if(!empty($json_models)) {
			$arFiles = array_map(function ($i) use ($arEquipment) {
				$result = array_filter(
					$arEquipment,
					function ($e) use ($i) {
						return $e['NAME'] == $i;
					}
				);
				if (!count($result)) echo 'Model '.$i.' not found for '.$buf['ID'].PHP_EOL;
				return $result[array_keys($result)[0]]['ID'];
			}, $json_models);
			print_r($arFiles);
			CIBlockElement::SetPropertyValuesEx(
				$buf['ID'],
				4,
				['RELATIONS' => $arFiles]
			);
		} else echo 'empty relations';
	}
}

function parseDate () {
	$cursor = CIBlockElement::GetList(
		[],
		[
			"IBLOCK_ID" => 5
		],
		false,
		false,
		[
			"IBLOCK_ID",
			"ID",
			"PROPERTY_CREATED_JSON"
		]
	);
    $el = new CIBlockElement;
	while($buf = $cursor->Fetch()) {
		$date = $buf['PROPERTY_CREATED_JSON_VALUE'];
		if (strlen($date)) {
			$timestamp = strtotime($date);
			$formattedDate = date("d.m.Y", $timestamp);
			if(!$el->Update($buf['ID'], ['ACTIVE_FROM' => $formattedDate])) {
				echo $buf['ID'].': '.$el->LAST_ERROR;
			};
		}
	}
}

function deleteElement () {
	if (!CIBlockElement::Delete($ELEMENT_ID)) {
        $strWarning .= 'Error!';
        $DB->Rollback();
    }
    else {
        $DB->Commit();
	}
}

function setFieldTypeToHTML() {
    $cursor = \Bitrix\Iblock\ElementTable::getList([
        'filter' => ['IBLOCK_ID' => 12],
        'select' => ['IBLOCK_ID', 'ID']
    ]);
    $el = new CIBlockElement;
    while($buf = $cursor->Fetch()) {
        if(!$el->Update($buf['ID'], [
            'DETAIL_TEXT_TYPE' => 'html'
        ])) echo $buf['ID'].': '.$el->LAST_ERROR.PHP_EOL;
    }
}

function getStringProperies() {
    include $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
    \Bitrix\Main\Loader::includeModule("iblock");
    $cursor = \Bitrix\Iblock\PropertyTable::getList([
        'filter' => [
            'IBLOCK_ID' => 3,
            'PROPERTY_TYPE' => 'S'
        ],
        'select' => ['CODE']
    ]);
    $result = [];
    while($buf = $cursor->Fetch()) {
        $result[] = $buf['CODE'];
    }
}

function getUniquePropertyValues() {
    $cursor = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => 3,
            [
                'LOGIC' => 'OR',
                '!PROPERTY_MAX_FRAKTSIYA_BETONA_MM' => false,
                '!PROPERTY_MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI' => false,
                '!PROPERTY_MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI' => false,
                '!PROPERTY_SKOROST_PODACHI_M3_CH' => false,
                '!PROPERTY_SKOROST_PODACHI_L_MIN' => false
            ]
        ],
        false,
        false,
        ['ID', 'NAME', 'PROPERTY_MAX_FRAKTSIYA_BETONA_MM', 'PROPERTY_MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI', 'PROPERTY_MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI', 'PROPERTY_SKOROST_PODACHI_M3_CH', 'PROPERTY_SKOROST_PODACHI_L_MIN']
    );
    $result = [
        'MAX_FRAKTSIYA_BETONA_MM' => [],
        'MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI' => [],
        'MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI' => [],
        'SKOROST_PODACHI_M3_CH' => [],
        'SKOROST_PODACHI_L_MIN' => []
    ];
    while ($buf = $cursor->Fetch()) {
        if ($buf['PROPERTY_MAX_FRAKTSIYA_BETONA_MM_VALUE']) {
            $result['MAX_FRAKTSIYA_BETONA_MM'] = array_merge(array_diff([$buf['PROPERTY_MAX_FRAKTSIYA_BETONA_MM_VALUE']], $result['MAX_FRAKTSIYA_BETONA_MM']), $result['MAX_FRAKTSIYA_BETONA_MM']);
        }
        if ($buf['PROPERTY_MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI_VALUE']) {
            $result['MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI'] = array_merge(array_diff([$buf['PROPERTY_MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI_VALUE']], $result['MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI']), $result['MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI']);
        }
        if ($buf['PROPERTY_MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI_VALUE']) {
            $result['MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI'] = array_merge(array_diff([$buf['PROPERTY_MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI_VALUE']], $result['MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI']), $result['MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI']);
        }
        if ($buf['PROPERTY_SKOROST_PODACHI_M3_CH_VALUE']) {
            $result['SKOROST_PODACHI_M3_CH'] = array_merge(array_diff([$buf['PROPERTY_SKOROST_PODACHI_M3_CH_VALUE']], $result['SKOROST_PODACHI_M3_CH']), $result['SKOROST_PODACHI_M3_CH']);
        }
        if ($buf['PROPERTY_SKOROST_PODACHI_L_MIN_VALUE']) {
            $result['SKOROST_PODACHI_L_MIN'] = array_merge(array_diff([$buf['PROPERTY_SKOROST_PODACHI_L_MIN_VALUE']], $result['SKOROST_PODACHI_L_MIN']), $result['SKOROST_PODACHI_L_MIN']);
        }
        $result['ITEMS'][$buf['ID']] = [
            'MAX_FRAKTSIYA_BETONA_MM' => $buf['PROPERTY_MAX_FRAKTSIYA_BETONA_MM_VALUE'],
            'MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI' => $buf['PROPERTY_MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI_VALUE'],
            'MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI' => $buf['PROPERTY_MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI_VALUE'],
            'SKOROST_PODACHI_M3_CH' => $buf['PROPERTY_SKOROST_PODACHI_M3_CH_VALUE'],
            'SKOROST_PODACHI_L_MIN' => $buf['PROPERTY_SKOROST_PODACHI_L_MIN_VALUE']
        ];
    }
    print_r($result['ITEMS']);
    $arFields = [
        [
            "ID" => 78,
            'DATA' => Array(
                "PROPERTY_TYPE" => "L",
                'VALUES' => array_map(function($e) {
                    return [
                        "VALUE" => $e,
                        "DEF" => "N",
                        "SORT" => "100"
                    ];
                }, $result['MAX_FRAKTSIYA_BETONA_MM'])
            )
        ],
        [
            "ID" => 80,
            'DATA' => Array(
                "PROPERTY_TYPE" => "L",
                'VALUES' => array_map(function($e) {
                    return [
                        "VALUE" => $e,
                        "DEF" => "N",
                        "SORT" => "100"
                    ];
                }, $result['MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI'])
            )
        ],
        [
            "ID" => 81,
            'DATA' => Array(
                "PROPERTY_TYPE" => "L",
                'VALUES' => array_map(function($e) {
                    return [
                        "VALUE" => $e,
                        "DEF" => "N",
                        "SORT" => "100"
                    ];
                }, $result['MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI'])
            )
        ],
        [
            "ID" => 155,
            'DATA' => Array(
                "PROPERTY_TYPE" => "L",
                'VALUES' => array_map(function($e) {
                    return [
                        "VALUE" => $e,
                        "DEF" => "N",
                        "SORT" => "100"
                    ];
                }, $result['SKOROST_PODACHI_M3_CH'])
            )
        ],
        [
            "ID" => 156,
            'DATA' => Array(
                "PROPERTY_TYPE" => "L",
                'VALUES' => array_map(function($e) {
                    return [
                        "VALUE" => $e,
                        "DEF" => "N",
                        "SORT" => "100"
                    ];
                }, $result['SKOROST_PODACHI_L_MIN'])
            )
        ]
    ];
    $ibp = new CIBlockProperty;
    foreach ($arFields as $field) {
        if (!$ibp->Update($field['ID'], $field['DATA'])) {
            die($ibp->LAST_ERROR);
        }
    }
    foreach ($result['ITEMS'] as $id => $item) {
        $max_fract = CIBlockPropertyEnum::GetList([],[
            'PROPERTY_ID ' => 78,
            'VALUE' => $item['MAX_FRAKTSIYA_BETONA_MM']
        ])->Fetch();
        $max_rasst_vert = CIBlockPropertyEnum::GetList([],[
            'PROPERTY_ID ' => 80,
            'VALUE' => $item['MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI']
        ])->Fetch();
        $max_rasst_hor = CIBlockPropertyEnum::GetList([],[
            'PROPERTY_ID ' => 81,
            'VALUE' => $item['MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI']
        ])->Fetch();
        $skor_pod_m = CIBlockPropertyEnum::GetList([],[
            'PROPERTY_ID ' => 155,
            'VALUE' => $item['SKOROST_PODACHI_M3_CH']
        ])->Fetch();
        $skor_pod_l = CIBlockPropertyEnum::GetList([],[
            'PROPERTY_ID ' => 156,
            'VALUE' => $item['SKOROST_PODACHI_L_MIN']
        ])->Fetch();
        $preparedProps = [];
        if($max_fract['ID']) $preparedProps['MAX_FRAKTSIYA_BETONA_MM'] = $max_fract['ID'];
        if($max_rasst_vert['ID']) $preparedProps['MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI'] = $max_rasst_vert['ID'];
        if($max_rasst_hor['ID']) $preparedProps['MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI'] = $max_rasst_hor['ID'];
        if($skor_pod_m['ID']) $preparedProps['SKOROST_PODACHI_M3_CH'] = $skor_pod_m['ID'];
        if($skor_pod_l['ID']) $preparedProps['SKOROST_PODACHI_L_MIN'] = $skor_pod_l['ID'];
        if(!empty($preparedProps)) {
            CIBlockElement::SetPropertyValuesEx($id, 3, $preparedProps);
        } else echo 'empty props: '.$id.PHP_EOL;
    }
}

function getUserProfileValues() {
    $profiles = [];
    $cursor = \Bitrix\Sale\OrderUserProperties::getList(["filter" => ["USER_ID" => 38]]);
    while($buf = $cursor->Fetch()) {
        $profiles[] = $buf;
    }
    $cursor = CSaleOrderUserPropsValue::GetList(
        array("DATE_UPDATE" => "DESC"),
        array("USER_PROPS_ID" => array_map(function($p){return $p["ID"];}, $profiles)),
        false,
        false,
        ["*"]
    );
    $result = [];
    while($buf = $cursor->Fetch()) {
        $result[] = $buf;
    }
    print_r($result);
}
\Bitrix\Iblock\ElementTable::getList()