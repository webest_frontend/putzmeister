<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>
<div class="item__container">
    <div class="item__preview cover-link">
        <img src="<?= $item['PREVIEW_PICTURE']['SRC'] ? $item['PREVIEW_PICTURE']['SRC'] : SITE_TEMPLATE_PATH.'/images/no_photo.png'?>" alt="<?= $item['DETAIL_PICTURE']['ALT'] ?>">
        <? if ($item['LABEL']) { ?>
            <div class="item__labels">
                <? if (!empty($item['LABEL_ARRAY_VALUE'])) {
                    foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value) { ?>
                        <span class="item__label <?=$value?>">
                            <?= $value ?>
                        </span>
                    <? }
                } ?>
            </div>
        <? } ?>
        <a href="<?= $item['DETAIL_PAGE_URL'] ?>"></a>
    </div>
    <div class="item__anons">
        <div class="item__title"><?=$productTitle?></div>
        <? if ($item['PROPERTIES']['ARTICLE']['~VALUE']) { ?>
            <div class="item__article">
                <span>Артикул:</span>
                <?=$item['PROPERTIES']['ARTICLE']['~VALUE']?>
            </div>
        <? } ?>
    </div>
    <div class="item__price">
        <? if (!empty($price)) { ?>
            <div class="item__price--roubles">
                <span><?= $price['IN_RUBLES'] ?></span>
            </div>
            <div class="item__price--euro">
                <?= $price['PRINT_RATIO_BASE_PRICE'] ?>
            </div>
            <button class="btn btn--yellow" data-product_id="<?=$item['ID']?>">
                В корзину
            </button>
        <? } else { ?>
            <button
                class="btn item__price_request_btn modal_trigger--price_request"
                data-model_name="<?=$item['NAME']?>"
                data-model_link="<?=$item['FULL_PATH']?>">Узнать цену</button>
        <? } ?>
    </div>
</div>