<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Loader::includeModule("currency");
$arResult['ITEM']['FULL_PATH'] = $_SERVER["HTTPS"] ? "https" : "http"."://".$_SERVER["HTTP_HOST"].$arResult['ITEM']['DETAIL_PAGE_URL'];
if (!empty($arResult['ITEM']['ITEM_PRICES'])) {
    $selected_price = $arResult['ITEM']['ITEM_PRICE_SELECTED'];
    $currency = $arResult['ITEM']['ITEM_PRICES'][$selected_price]['CURRENCY'];
    $price = $arResult['ITEM']['ITEM_PRICES'][$selected_price]['RATIO_BASE_PRICE'];
    if ($currency == 'EUR') {
        $price_in_rubles = CCurrencyRates::ConvertCurrency($price, "EUR", "RUB");
        $price_in_rubles = CCurrencyLang::CurrencyFormat($price_in_rubles, 'EUR', false);
        $price_in_euro = $arResult['ITEM']['ITEM_PRICES'][$selected_price]['PRINT_RATIO_BASE_PRICE'];
    } elseif ($currency == 'RUB') {
        $price_in_rubles = $price;
        $price_in_euro = CCurrencyRates::ConvertCurrency($price, "RUB", "EUR");
        $price_in_euro = CCurrencyLang::CurrencyFormat($price_in_euro, 'EUR');
    }
    $arResult['ITEM']['ITEM_PRICES'][$selected_price]['PRINT_RATIO_BASE_PRICE'] = $price_in_euro;
    $arResult['ITEM']['ITEM_PRICES'][$selected_price]['IN_RUBLES'] = $price_in_rubles;
}