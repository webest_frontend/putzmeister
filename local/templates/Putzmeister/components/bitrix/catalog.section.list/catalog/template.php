<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));?>
<? if (0 < $arResult["SECTIONS_COUNT"]) { ?>
    <div class="catalog__tiles">
        <? foreach ($arResult['SECTIONS'] as $arSection) {
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams); ?>
            <div id="<?= $this->GetEditAreaId($arSection['ID']); ?>">
                <div class="catalog__tile cover-link filled">
                    <img src="<?=$arSection['PICTURE']['SRC']?>" alt="">
                    <div class="catalog__tile_title">
                        <?= $arSection['NAME'] ?>
                        <svg>
                            <use href="<?=SITE_TEMPLATE_PATH?>/images/icons/sprite.svg#arrow-right"></use>
                        </svg>
                    </div>
                    <a href="<?=$arSection['SECTION_PAGE_URL'] ?>"></a>
                </div>
            </div>
        <? } ?>
    </div>
<? } ?>