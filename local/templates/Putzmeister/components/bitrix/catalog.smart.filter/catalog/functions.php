<?
function renderFilterBlock($arItem) {
    if ($arItem['IS_EMPTY']) return;
    ob_start();?>
    <div class="smartfilter__inputs_block block_<?=$arItem['ID'];?>">
        <? if ($arItem['NAME']) { ?>
            <div class="smartfilter__inputs_header">
                <?= $arItem['NAME'] ?>
            </div>
        <? } ?>
        <? $arCur = current($arItem["VALUES"]);
        $checkedItemExist = false;
        switch ($arItem["DISPLAY_TYPE"]) {
            case "P": ?>
                <div class="smartfilter__inputs_footer">
                    <? if ($arItem['TITLE']) { ?>
                        <div>
                            <?= $arItem['TITLE'] ?>
                        </div>
                    <? } ?>
                    <div class="input input-multi">
                        <div class="wrap__select<?if($arItem["INACTIVE"] || $arItem["ONLY_VALUE"]) echo ' disabled';?>">
                            <div class="select__selected">
                                <? if($arItem["ONLY_VALUE"]) {
                                    echo $arItem["ONLY_VALUE"];
                                } else {
                                    foreach ($arItem["VALUES"] as $val => $ar) {
                                        if ($ar["CHECKED"]) {
                                            echo $ar["VALUE"];
                                            $checkedItemExist = true;
                                        }
                                    }
                                    if (!$checkedItemExist) {
                                        echo GetMessage("CT_BCSF_FILTER_ALL");
                                    }
                                } ?>
                            </div>
                            <? if(!$arItem["INACTIVE"] && !$arItem["ONLY_VALUE"]) { ?>
                                <div class="select__list">
                                    <label class="list__item bx-filter-param-label" data-role="label_<?= "all_" . $arCur["CONTROL_ID"] ?>" onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape("all_" . $arCur["CONTROL_ID"]) ?>')">
                                        <span><? echo GetMessage("CT_BCSF_FILTER_ALL"); ?></span>
                                        <input
                                            type="radio"
                                            name="<?= $arCur["CONTROL_NAME_ALT"] ?>"
                                            id="<? echo "all_" . $arCur["CONTROL_ID"] ?>"
                                            value="">
                                    </label>
                                    <? foreach ($arItem["VALUES"] as $val => $ar) {
                                        $class = "";
                                        if ($ar["CHECKED"])
                                            $class .= " selected";
                                        if ($ar["DISABLED"])
                                            $class .= " disabled";
                                        if (!$ar["DISABLED"]) { ?>
                                            <label class="input input-dropdown_item" data-role="label_<?= $ar["CONTROL_ID"] ?>" onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>');">
                                                <span><?= $ar["VALUE"] . '&nbsp;' . $arItem["FILTER_HINT"] ?></span>
                                                <input
                                                    type="radio"
                                                    name="<?= $ar["CONTROL_NAME_ALT"] ?>"
                                                    value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                                    id="<?= $ar["CONTROL_ID"] ?>"
                                                    <? if ($ar["CHECKED"]) echo ' checked'; ?>>
                                            </label>
                                        <? }
                                    } ?>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
                <? break;
            case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS?>
                <div class="smartfilter__list--icons">
                    <? foreach ($arItem["VALUES"] as $val => $ar) {
                        $class = "";
                        if ($ar["CHECKED"])
                            $class .= " bx-active";
                        if ($ar["DISABLED"])
                            $class .= " disabled"; ?>
                        <label
                            class="bx-filter-param-label<?= $class ?> wrap__checkbox"
                            data-role="label_<?= $ar["CONTROL_ID"] ?>"
                            <?=$ar["DISABLED"] ? '  disabled onclick="return false"' : ' onclick="smartFilter.keyup(BX(\''.CUtil::JSEscape($ar["CONTROL_ID"]).'\')); BX.toggleClass(this, \'bx-active\');"'; ?>>
                            <span><?= $ar["VALUE"]; ?></span>
                            <input
                                id="<?= $ar["CONTROL_ID"] ?>"
                                type="checkbox"
                                name="<?= $ar["CONTROL_NAME"] ?>"
                                value="<?= $ar["HTML_VALUE"] ?>"
                                <? if ($ar["CHECKED"]) echo ' checked';?>>
                            <span
                                    class="checkmark"
                                    <? if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])) { ?>
                                        style="background-image:url('<?= $ar["FILE"]["SRC"] ?>')">
                                    <? } ?>
                            </span>
                        </label>
                    <? } ?>
                </div>
                <? break;
            case "K"://RADIOBUTTONS ?>
                <? if (in_array($arItem["ID"], ["164", "165"])) { ?>
                    <div class="smartfilter__list<?if($arItem["ID"] == "164") echo "--icons";?>">
                <? }
                $filtered_cheked = array_filter($arItem["VALUES"], function ($ar) {
                    return isset($ar["CHECKED"]);
                });
                foreach ($arItem["VALUES"] as $val => $ar) { ?>
                    <label
                            data-role="label_<?= $ar["CONTROL_ID"] ?>"
                            class="wrap__radio<? if (isset($ar["FILE"])) echo " with-icon"; ?>"
                            <? if ($ar["DISABLED"]) echo ' disabled onclick="return false"'; ?>
                            <?if($arItem["ID"] == "164") echo 'title="'.$ar["FILE"]["DESCRIPTION"].'"'; ?>">
                        <span>
                            <?= $ar["VALUE"]; ?>
                        </span>
                        <input
                                type="radio"
                                value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
                                <? if ($ar["CHECKED"]) echo ' checked'; ?>
                                <? if (!$ar["DISABLED"]) echo ' onclick="smartFilter.click(this)"'; ?>>
                        <span class="checkmark" <? if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])) { ?>
                            style="background-image:url('<?= $ar["FILE"]["SRC"] ?>')">
                        <? } ?></span>
                    </label>
                <? } ?>
                <label class="wrap__radio<? if (isset($ar["FILE"])) echo " with-icon"; ?>">
                    <span>Все</span>
                    <input
                            type="radio"
                            value=""
                            name="<?= $arCur["CONTROL_NAME_ALT"] ?>"
                            <?if(!count($filtered_cheked)) echo " checked";?>
                            onclick="smartFilter.click(this)">
                    <span class="checkmark"<?if($arItem["ID"] == "164") echo ' style="background-image:url('.SITE_TEMPLATE_PATH.'/images/icons/dummy_pump.svg);"'?>></span>
                </label>
                <? if (in_array($arItem["ID"], ["164", "165"])) { ?>
                    </div>
                <? }
                break;
            default://CHECKBOXES ?>
                <div class="smartfilter__list">
                    <? foreach ($arItem["VALUES"] as $val => $ar) {
                        if(!$ar["DISABLED"]) { ?>
                            <label
                                data-role="label_<?= $ar["CONTROL_ID"] ?>"
                                class="wrap__checkbox<?if ($ar["DISABLED"]) echo ' disabled'; ?>">
                                <?= $ar["VALUE"] ?>
                                <input
                                    type="checkbox"
                                    value="<? echo $ar["HTML_VALUE"] ?>"
                                    name="<? echo $ar["CONTROL_NAME"] ?>"
                                    <? if ($ar["CHECKED"]) echo " checked"; ?>
                                    onclick="smartFilter.click(this)">
                                <span class="checkmark"></span>
                            </label>
                        <? }
                    } ?>
                </div>
        <? } ?>
    </div>
    <? ob_end_flush();
}