<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"]))
{
	$arAvailableThemes = array();
	$dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__)."/themes/"));
	if (is_dir($dir) && $directory = opendir($dir))
	{
		while (($file = readdir($directory)) !== false)
		{
			if ($file != "." && $file != ".." && is_dir($dir.$file))
				$arAvailableThemes[] = $file;
		}
		closedir($directory);
	}

	if ($arParams["TEMPLATE_THEME"] == "site")
	{
		$solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
		if ($solution == "eshop")
		{
			$templateId = COption::GetOptionString("main", "wizard_template_id", "eshop_bootstrap", SITE_ID);
			$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? "eshop_adapt" : $templateId;
			$theme = COption::GetOptionString("main", "wizard_".$templateId."_theme_id", "blue", SITE_ID);
			$arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
		}
	}
	else
	{
		$arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
	}
}
else
{
	$arParams["TEMPLATE_THEME"] = "blue";
}

$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";
$arResult['SHOW_FILTER'] = false;
if(!empty(array_diff_key($arResult['ITEMS'], ['base' => 1, 'BASE' => 1]))) {

    function filterEmptyItems(&$item) {
        if(in_array($item['ID'], [155, 156])) {
            $item['IS_EMPTY'] = empty(array_filter($item['VALUES'], function ($f) {
                return isset($f['DISABLED']) ? !$f['DISABLED'] : true;
            }));
        } else {
            $item['IS_EMPTY'] = empty($item['VALUES']);
            $item['INACTIVE'] = empty(array_filter($item['VALUES'], function ($f) {
                return isset($f['DISABLED']) ? !$f['DISABLED'] : true;
            }));
        }
        $total_values = array_filter($item["VALUES"], function ($e) {
            return !$e['DISABLED'];
        });
        if(count($total_values) == 1) {
            $item['ONLY_VALUE'] = $total_values[array_keys($total_values)[0]]['VALUE'];
        }
    }

    array_walk($arResult['ITEMS'], 'filterEmptyItems');
    $arResult["ITEMS"]['base']['IS_EMPTY'] = $arResult["ITEMS"]['BASE']['IS_EMPTY'] = true;
	if (key_exists(155, $arResult['ITEMS'])) {
		$arResult['ITEMS'][155]['NAME'] = '';
	}
	if (key_exists(156, $arResult['ITEMS'])) {
		$arResult['ITEMS'][156]['NAME'] = '';
	}
	if (key_exists(80, $arResult['ITEMS'])) {
		$arResult['ITEMS'][80]['NAME'] = 'по вертикали';
	}
	if (key_exists(81, $arResult['ITEMS'])) {
		$arResult['ITEMS'][81]['NAME'] = 'по горизонтали';
	}

	$arResult['SHOW_FILTER'] = !empty(array_filter(
		$arResult['ITEMS'],
		function($e) {
			return !$e['IS_EMPTY'];
		}
	));
}