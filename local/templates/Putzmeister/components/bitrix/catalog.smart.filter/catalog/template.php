<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include $_SERVER['DOCUMENT_ROOT'].'/'.$templateFolder.'/functions.php';
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult['SHOW_FILTER']) { ?>
    <form
        id="smartfilter"
        name="<? echo $arResult["FILTER_NAME"] . "_form" ?>"
        action="<? echo $arResult["FORM_ACTION"] ?>"
        method="get">
        <input type="hidden" name="foo" value="bar">
        <? foreach ($arResult["HIDDEN"] as $arItem) { ?>
            <input type="hidden" name="<?= $arItem["CONTROL_NAME"] ?>" id="<?= $arItem["CONTROL_ID"] ?>" value="<?= $arItem["HTML_VALUE"] ?>"/>
        <? } ?>
        <div class="bx-filter-parameters-box-container">
            <input
                    class="btn btn-themes"
                    type="submit"
                    id="set_filter"
                    name="set_filter"
                    value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
                    style="display: none">
            <div
                    class="bx-filter-popup-result <? if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"] ?>"
                    id="modef" <? if (!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"'; ?>
                    style="display: inline-block;">
                <a href="<? echo $arResult["FILTER_URL"] ?>" target="">Фильтр</a>
                (<span id="modef_num"><?= intval($arResult["ELEMENT_COUNT"]) ?></span>)
            </div>
            <input
                type="submit"
                id="del_filter"
                name="del_filter"
                value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>">
        </div>
        <div class="smartfilter__info" onclick="this.classList.toggle('expanded');">
            <p class="smartfilter__info_content">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "AREA_FILE_RECURSIVE" => "Y",
                        "PATH" => SITE_TEMPLATE_PATH."/include_areas/filter_text.php",
                        "EDIT_TEMPLATE" => "standard.php"
                    )
                );?>
            </p>
        </div>
        <div class="smartfilter__body">
            <div class="smartfilter__body_top">
                <div class="smartfilter__main_block">
                    <div class="smartfilter__block_body">
                        <? foreach ([$arResult["ITEMS"][165], $arResult["ITEMS"][164]] as $arItem) {
                            renderFilterBlock($arItem);
                        } ?>
                    </div>
                </div>
            </div>
            <div class="smartfilter__body_bottom">
                <?/*<div class="smartfilter__performance_block">
                    <div class="smartfilter__block_title">
                        Скорость подачи
                    </div>
                    <div class="smartfilter__block_body">
                        <? foreach ([$arResult["ITEMS"][155], $arResult["ITEMS"][156]] as $arItem) {
                            renderFilterBlock($arItem);
                        } ?>
                    </div>
                </div>*/?>
                <div class="smartfilter__fraction_block">
                    <div class="smartfilter__block_body">
                        <? renderFilterBlock($arResult["ITEMS"][78]); ?>
                    </div>
                </div>
                <div class="smartfilter__range_block">
                    <div class="smartfilter__block_title">
                        Максимальное расстояние подачи
                    </div>
                    <div class="smartfilter__block_body">
                        <? foreach ([$arResult["ITEMS"][81], $arResult["ITEMS"][80]] as $arItem) {
                            renderFilterBlock($arItem);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="smartfilter__footer">
            <p>
                При эксплуатации оборудования необходимо использовать раствор, подходящий для данной машины.
                <br>
                В случае возникновения вопросов обратитесь к производителю материала или свяжитесь непосредственно с нами.
            </p>
            <div class="advanced_filter" onclick="this.classList.toggle('expanded');document.querySelector('.page__main').classList.toggle('filter_expanded');">
                <span>Подробнее</span>
            </div>
        </div>
    </form>
    <script>
        var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
    </script>
<? }