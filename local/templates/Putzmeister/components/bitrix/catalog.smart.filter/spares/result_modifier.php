<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Iblock\ElementTable;
use \Bitrix\Iblock\SectionTable;

function walk_elements(&$el, $key, $arElements) {
    $el['SECTION_ID'] = $arElements[$key]['IBLOCK_SECTION_ID'];
}

if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"])) {
    $arAvailableThemes = array();
    $dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__) . "/themes/"));
    if (is_dir($dir) && $directory = opendir($dir)) {
        while (($file = readdir($directory)) !== false) {
            if ($file != "." && $file != ".." && is_dir($dir . $file))
                $arAvailableThemes[] = $file;
        }
        closedir($directory);
    }

    if ($arParams["TEMPLATE_THEME"] == "site") {
        $solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
        if ($solution == "eshop") {
            $templateId = COption::GetOptionString("main", "wizard_template_id", "eshop_bootstrap", SITE_ID);
            $templateId = (preg_match("/^eshop_adapt/", $templateId)) ? "eshop_adapt" : $templateId;
            $theme = COption::GetOptionString("main", "wizard_" . $templateId . "_theme_id", "blue", SITE_ID);
            $arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
        }
    } else {
        $arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
    }
} else {
    $arParams["TEMPLATE_THEME"] = "blue";
}

$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";

if (array_key_exists($arParams['RELATIONS_PROPERTY_ID'], $arResult['ITEMS'])) {
    $cursor = SectionTable::getList([
        'filter' => ['IBLOCK_ID' => $arParams['RELATIONS_IBLOCK_ID']],
        'select' => ['ID', 'IBLOCK_SECTION_ID', 'NAME']
    ]);
    $arSectionsAll = [];
    while ($buf = $cursor->Fetch()) {
        $arSectionsAll[$buf['ID']] = $buf;
    }
    $arElements = [];
    $arSections = [];
    $cursor = ElementTable::getList([
        'filter' => ['IBLOCK_ID' => $arParams['RELATIONS_IBLOCK_ID'], 'ID' => array_keys($arResult['ITEMS'][$arParams['RELATIONS_PROPERTY_ID']]['VALUES'])],
        'select' => ['ID', 'IBLOCK_SECTION_ID']
    ]);
    while ($buf = $cursor->Fetch()) {
        $arElements[$buf['ID']] = $buf;
        $section_id = $buf['IBLOCK_SECTION_ID'];
        $arSections[$section_id] = $arSectionsAll[$section_id];
        $parent_section_id = $arSectionsAll[$section_id]['IBLOCK_SECTION_ID'];
        if($parent_section_id) {
            $arSections[$parent_section_id] = $arSectionsAll[$parent_section_id];
            $arSections[$parent_section_id]['SUBSECTIONS'][] = $arSectionsAll[$section_id];
        }
    }
    array_walk($arResult['ITEMS'][$arParams['RELATIONS_PROPERTY_ID']]['VALUES'], 'walk_elements', $arElements);
    $arResult['SECTIONS'] = array_filter($arSections, function ($s) {
        return $s['IBLOCK_SECTION_ID'];
    });
    $arResult['ROOT_SECTIONS'] = array_filter($arSections, function ($s) {
        return !$s['IBLOCK_SECTION_ID'];
    });

    $arResult['SELECTED'] = [
        'CATEGORY' => GetMessage("CT_BCSF_FILTER_ALL_CATEGORY"),
        'TYPE' => GetMessage("CT_BCSF_FILTER_ALL_TYPE"),
        'MODEL' => GetMessage("CT_BCSF_FILTER_ALL_MODEL")
    ];
    foreach ($arResult['ITEMS'][$arParams['RELATIONS_PROPERTY_ID']]["VALUES"] as $id => $ar) {
        if ($ar["CHECKED"]) {
            $section_id = $ar['SECTION_ID'];
            $parent_section_id = $arResult["SECTIONS"][$ar['SECTION_ID']]['IBLOCK_SECTION_ID'];
            if(!$parent_section_id) {
                $parent_section_id = $section_id;
            }
            if(in_array($section_id, array_keys($arResult["SECTIONS"]))) {
                $arResult['SELECTED']['TYPE'] = $arResult["SECTIONS"][$section_id]['NAME'];
            }
            $arResult['SELECTED']['CATEGORY'] = $arResult["ROOT_SECTIONS"][$parent_section_id]['NAME'];
            $arResult['SELECTED']['CATEGORY_ID'] = $arResult["ROOT_SECTIONS"][$parent_section_id]['ID'];
            $selected_models = array_filter($arResult['ITEMS'][$arParams['RELATIONS_PROPERTY_ID']]['VALUES'], function ($e) {
                return $e['CHECKED'];
            });
            $arResult['SELECTED']['MODEL'] = $selected_models[array_keys($selected_models)[0]]['VALUE'];
            $arResult['SELECTED']['SECTION_ID'] = $selected_models[array_keys($selected_models)[0]]['SECTION_ID'];
            $arResult['SELECTED']['TYPE'] = $arSections[$arResult['SELECTED']['SECTION_ID']]['NAME'];
        }
    }
}