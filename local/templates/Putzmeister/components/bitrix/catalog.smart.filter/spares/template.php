<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<form
        id="smartfilter"
        name="<? echo $arResult["FILTER_NAME"] . "_form" ?>"
        action="<? echo $arResult["FORM_ACTION"] ?>"
        method="get">
    <? foreach ($arResult["HIDDEN"] as $arItem) { ?>
        <input type="hidden" name="<?= $arItem["CONTROL_NAME"] ?>" id="<?= $arItem["CONTROL_ID"] ?>" value="<?= $arItem["HTML_VALUE"] ?>"/>
    <? } ?>
    <div class="bx-filter-parameters-box-container">
        <input
                class="btn btn-themes"
                type="submit"
                id="set_filter"
                name="set_filter"
                value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
                style="display: none">
        <div
                class="bx-filter-popup-result <? if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"] ?>"
                id="modef" <? if (!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"'; ?>
                style="display: inline-block;">
            <a href="<? echo $arResult["FILTER_URL"] ?>" target="">Фильтр</a>
            (<span id="modef_num"><?= intval($arResult["ELEMENT_COUNT"]) ?></span>)
        </div>
        <input
                class="btn btn-link"
                type="submit"
                id="del_filter"
                name="del_filter"
                value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>">
    </div>
    <div class="smartfilter__body">
        <div class="wrap__select">
            <div class="select__selected">
                <?=$arResult['SELECTED']['CATEGORY']?>
            </div>
            <div class="select__list filter_categories">
                <? foreach ($arResult["ROOT_SECTIONS"] as $section) { ?>
                    <div class="list__item" data-id="<?=$section['ID']?>">
                        <?= $section['NAME'] ?>
                    </div>
                <? } ?>
            </div>
        </div>
        <div class="wrap__select<?if(count(array_filter($arResult["SECTIONS"], function($e) use($arResult) {
                return $arResult['SELECTED']['CATEGORY_ID'] == $e['IBLOCK_SECTION_ID'];
            })) < 2) echo ' disabled';?>">
            <div class="select__selected">
                <?=$arResult['SELECTED']['TYPE']?>
            </div>
            <div class="select__list filter_types">
                <? foreach ($arResult["SECTIONS"] as $section) { ?>
                    <div
                            class="list__item<?if($arResult['SELECTED']['CATEGORY_ID'] != $section['IBLOCK_SECTION_ID']) echo ' hidden';?>"
                            data-id="<?= $section['ID'] ?>"
                            data-parent_id="<?= $section['IBLOCK_SECTION_ID'] ? $section['IBLOCK_SECTION_ID'] : $section['ID'] ?>">
                        <?= $section['NAME'] ?>
                    </div>
                <? } ?>
            </div>
        </div>
        <div class="wrap__select">
            <div class="select__selected">
                <?=$arResult['SELECTED']['MODEL']?>
            </div>
            <div class="select__list filter_models">
                <? foreach ($arResult['ITEMS'][$arParams['RELATIONS_PROPERTY_ID']]['VALUES'] as $val => $ar) {
                    $class = "";
                    if ($ar["CHECKED"])
                        $class.= " selected";
                    if ($ar["DISABLED"])
                        $class.= " disabled"; ?>
                    <label
                            data-id="<?=$ar['SECTION_ID']?>"
                            class="bx-filter-param-label<?=$class?> list__item<? if($ar['SECTION_ID'] != $arResult['SELECTED']['SECTION_ID']) echo ' hidden'; ?>"
                            data-role="label_<?=$ar["CONTROL_ID"]?>"
                            onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
                        <span><?= $ar["VALUE"] ?></span>
                        <input
                            type="radio"
                            id="<?=$ar["CONTROL_ID"]?>"
                            name="<?=$ar["CONTROL_NAME_ALT"]?>"
                            value="<? echo $ar["HTML_VALUE_ALT"] ?>">
                        <span class="checkmark"></span>
                    </label>
                <? } ?>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
    var smartFilterExt;
    BX(function () {
        smartFilterExt = new SmartFilterExt(document.getElementById('smartfilter'));
    });
</script>