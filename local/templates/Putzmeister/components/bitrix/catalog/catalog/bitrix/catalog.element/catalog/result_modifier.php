<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arResult['FULL_PATH'] = $_SERVER["HTTPS"] ? "https" : "http"."://".$_SERVER["HTTP_HOST"].$arResult['DETAIL_PAGE_URL'];

$arResult['PROPERTIES']['PHOTO_IMAGE_GALLERY']['VALUE'] = array_map(function ($s) {
    return CFile::GetPath($s);
}, $arResult['PROPERTIES']['PHOTO_IMAGE_GALLERY']['VALUE']);

use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
\Bitrix\Main\Loader::includeModule('highloadblock');
foreach ([
    'SCOPES' => 1,
     'PUMP' => 4
     ] as $key=>$id) {
    $hl_block = HLBT::getById($id)->fetch();
    $entity = HLBT::compileEntity($hl_block);
    $entity_data_class = $entity->getDataClass();
    $rs_data = $entity_data_class::getList(array(
        'select' => array('UF_XML_ID', 'UF_FILE', 'UF_NAME', 'UF_FULL_DESCRIPTION')
    ));
    $arResult['HB'][$key] = [];
    while ($el = $rs_data->fetch()) {
        $file = CFile::GetPath($el['UF_FILE']);
        $arResult['HB'][$key][$el['UF_XML_ID']] = array_merge($el, ['ICON' => $file]);
    }
}
$arResult['OFFERS_PROPS'] = [
    'PERFORMANCE' => [
        'MIN' => 0,
        'MAX'  => 0
    ],
    'RANGE' => [
        'HORIZONTAL' => 0,
        'VERTICAL' => 0
    ],
    'SUIT' => [],
    'GENERAL' => []
];
$arResult["tech_keys"] = [];
$arResult["extra_keys"] = [];
$arResult["scopes_keys"] = [];
$arResult["pump_keys"] = [];
foreach ($arResult['OFFERS'] as $offer) {
    $performance = [
        'meters' => 0,
        'liters' => 0
    ];
    if($offer['PROPERTIES']['PODKHODIT_DLYA_KRUPNYKH_STROITELNYKH_PLOSHCHADOK_']['VALUE_XML_ID'] == "Y") {
        $arResult['OFFERS_PROPS']['SUIT']['LARGE'] = [
            'TITLE' => 'Идеально подходит для средних и крупных строительных площадок',
            'PICTURE' => '/images/icons/15.svg'
        ];
    }
    if($offer['PROPERTIES']['PODKHODIT_DLYA_NEBOLSHIKH_STROITELNYKH_PLOSHCHADOK']['VALUE_XML_ID'] == "Y") {
        $arResult['OFFERS_PROPS']['SUIT']['SMALL'] = [
            'TITLE' => 'Идеально подходит для небольших и средних строительных площадок',
            'PICTURE' => '/images/icons/14.svg'
        ];
    }
    if($offer['PROPERTIES']['SKOROST_PODACHI_M3_CH']['VALUE']) {
        $performance['meters'] = $offer['PROPERTIES']['SKOROST_PODACHI_M3_CH']['VALUE'];
    } elseif($offer['PROPERTIES']['SKOROST_PODACHI_L_MIN']['VALUE']) {
        $performance['liters'] = $offer['PROPERTIES']['SKOROST_PODACHI_L_MIN']['VALUE'];
    }
    $range = [
        'vertical' => $offer['PROPERTIES']['MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI']['VALUE'] ? $offer['PROPERTIES']['MAX_RASSTOYANIE_PODACHI_PO_VERTIKALI']['VALUE']: 0,
        'horizontal' => $offer['PROPERTIES']['MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI']['VALUE'] ? $offer['PROPERTIES']['MAX_RASSTOYANIE_PODACHI_PO_GORIZONTALI']['VALUE']: 0
    ];
    $filtered_techs = array_filter(
        $offer['PROPERTIES'],
        function ($i) {
            return $i['USER_TYPE'] != 'directory' && $i['VALUE'] && !in_array($i['ID'], [202, 51, 192, 121, 122]);
        }
    );
    foreach ($filtered_techs as $code => $value) {
        $arResult["tech_keys"][$code] = $value["NAME"];
    }
    $filtered_extras = array_filter(
        $offer['PROPERTIES'],
        function ($i) {
            return $i['USER_TYPE'] == 'directory' && $i['VALUE'] && !in_array($i['ID'], [165, 164]);
        }
    );
    foreach ($filtered_extras as $code => $value) {
        $arResult["extra_keys"][$code] = $value["NAME"];
    }
    if (!empty($offer['PROPERTIES']['SFERY_PRIMENENIYA']['VALUE'])) {
        foreach ($offer['PROPERTIES']['SFERY_PRIMENENIYA']['VALUE'] as $key) {
            $arResult["scopes_keys"][$key] = "";
        }
    }
    if (!empty($offer['PROPERTIES']['STEPEN_GOTOVNOSTI_K_PROKACHKE']['VALUE'])) {
        foreach ($offer['PROPERTIES']['STEPEN_GOTOVNOSTI_K_PROKACHKE']['VALUE'] as $key) {
            $arResult["pump_keys"][$key] = "";
        }
    }
    if ($performance) {
        if (!$arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MIN']) $arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MIN'] = 65535;
        if (!$arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MIN']) $arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MIN'] = 65535;
        $arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MIN'] = min(
            $arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MIN'],
            $performance['meters']
        );
        $arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MAX'] = max(
            $arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MAX'],
            $performance['meters']
        );
        $arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MIN'] = min(
            $arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MIN'],
            $performance['liters']
        );
        $arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MAX'] = max(
            $arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MAX'],
            $performance['liters']
        );
    }
    $arResult['OFFERS_PROPS']['RANGE']['HORIZONTAL'] = max(
        $arResult['OFFERS_PROPS']['RANGE']['HORIZONTAL'],
        $range['horizontal']
    );
    $arResult['OFFERS_PROPS']['RANGE']['VERTICAL'] = max(
        $arResult['OFFERS_PROPS']['RANGE']['VERTICAL'],
        $range['vertical']
    );
    $general_props = array_slice(array_filter($offer['PROPERTIES'], function ($p) {
     return $p['PROPERTY_TYPE'] == "N" && $p['VALUE'];
    }), 0, 4);
    $arResult['OFFERS_PROPS']['GENERAL'] = $general_props;
}


if ($arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MAX']) {
    if ($arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MIN'] != $arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MAX']) {
        $arResult['OFFERS_PROPS']['PERFORMANCE']['HTML'] = $arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MIN'] . '-' . $arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MAX'] . ' м<sup>3</sup>/ч';
    } else $arResult['OFFERS_PROPS']['PERFORMANCE']['HTML'] = $arResult['OFFERS_PROPS']['PERFORMANCE']['METERS']['MIN'] . ' м<sup>3</sup>/ч';
} elseif($arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MAX']) {
    if ($arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MIN'] != $arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MAX']) {
        $arResult['OFFERS_PROPS']['PERFORMANCE']['HTML'] = $arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MIN'] . '-' . $arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MAX'] . ' л/мин';
    } else $arResult['OFFERS_PROPS']['PERFORMANCE']['HTML'] = $arResult['OFFERS_PROPS']['PERFORMANCE']['LITERS']['MIN'] . ' л/мин';
}

$arResult['LABELS'] = array_filter($arResult['PROPERTIES'], function ($f) {
    return in_array($f['CODE'], ['NEW', 'HIT']) && $f['VALUE'] == "Y";
});

$arResult['OFFERS_PROPS']['SEO'] = [
    'DETAIL_TEXT' => $arResult['OFFERS'][0]['DETAIL_TEXT'],
    'DOP_FOTO' => array_map(function ($s) {
        return CFile::GetPath($s);
    }, $arResult['OFFERS'][0]['PROPERTIES']['DOP_FOTO']['VALUE'])
];
