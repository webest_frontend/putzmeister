BX(function () {
    BX.namespace('BX.Webest');
    BX.Webest.Slider = function (slider) {
        this.slider = slider;
        this.slider_config = {
            mobileFirst: true,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            waitForAnimate: false,
            infinite: true,
            focusOnSelect: true,
            prevArrow: '<button type="button" class="arrow arrow--simple"></button>',
            nextArrow: '<button type="button" class="arrow arrow--simple next"></button>',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        };
    };

    BX.Webest.Slider.prototype.init = function () {
        $(this.slider).slick(this.slider_config);
        return this;
    };

    BX.Webest.ModelSlider = function (slider) {
        BX.Webest.Slider.call(this, slider);
        Object.assign(this.slider_config, {
            arrows: false,
            asNavFor: '.multicard__slider_dots',
            responsive: []
        });
    };

    BX.Webest.ModelDotsSlider = function (slider) {
        BX.Webest.Slider.call(this, slider);
        Object.assign(this.slider_config, {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false,
            asNavFor: '.multicard__slider',
            centerMode: true,
            autoplay: true,
            autoplaySpeed: 2000
        });
    };

    BX.Webest.SEOSlider = function (slider) {
        BX.Webest.Slider.call(this, slider);
        Object.assign(this.slider_config, {
            arrows: false,
            responsive: []
        });
        this.mql = null;
    };

    BX.extend(BX.Webest.ModelSlider, BX.Webest.Slider);
    BX.extend(BX.Webest.ModelDotsSlider, BX.Webest.Slider);
    BX.extend(BX.Webest.SEOSlider, BX.Webest.Slider);

    BX.Webest.SEOSlider.prototype.init = function () {
        this.superclass.init.call(this);
        this.mql = window.matchMedia('(max-width: 768px)');
        this.mql.addEventListener('change', this.adapt.bind(this));
        this.mql.dispatchEvent(new Event("change"));
        return this;
    };
    BX.Webest.SEOSlider.prototype.adapt = function (e) {
        if(e.target.matches) {
            $(this.slider).slick(this.slider_config);
        } else {
            try {
                $(this.slider).slick("unslick");
            } catch (e) {
                console.dir(e);
            }
        }
    };

    $(".gallery-item").magnificPopup({
        gallery: {
            enabled: true
        },
        type: "image",
        closeMarkup: '<button class="mfp-close btn btn--close" type="button"></button>'
    });
});