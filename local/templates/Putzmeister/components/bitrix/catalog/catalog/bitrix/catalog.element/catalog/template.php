<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);
$haveOffers = !empty($arResult['OFFERS']);?>
<div class="multicard">
    <div class="multicard__top">
        <div class="multicard__slider_container">
            <? if (!empty($arResult['PROPERTIES']['PHOTO_IMAGE_GALLERY']['VALUE'])) { ?>
                <div class="multicard__slider">
                    <? foreach ($arResult['PROPERTIES']['PHOTO_IMAGE_GALLERY']['VALUE'] as $slide) { ?>
                        <div>
                            <a class="gallery-item" href="<?= $slide ?>">
                                <img src="<?= $slide ?>">
                            </a>
                        </div>
                    <? } ?>
                </div>
                <? if (count($arResult['PROPERTIES']['PHOTO_IMAGE_GALLERY']['VALUE']) > 1) { ?>
                    <div class="multicard__slider_dots">
                        <? foreach ($arResult['PROPERTIES']['PHOTO_IMAGE_GALLERY']['VALUE'] as $slide) { ?>
                            <div>
                                <img src="<?= $slide ?>">
                            </div>
                        <? } ?>
                    </div>
                <? } ?>
            <? } else { ?>
                <a class="gallery-item" href="<?= $arResult['DETAIL_PICTURE']["SRC"] ?>">
                    <img src="<?=$arResult['DETAIL_PICTURE']["SRC"]?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>">
                </a>
            <? }
            if(!empty($arResult['LABELS'])) { ?>
                <div class="multicard__labels">
                    <? foreach ($arResult['LABELS'] as $label) { ?>
                        <span class="label <?=$label['CODE']?>"><?=$label['CODE']?></span>
                    <? } ?>
                </div>
            <? } ?>
        </div>
        <div class="multicard__top_side">
            <? if (!empty($arResult['OFFERS'])) { ?>
                <div class="multicard__features">
                    <div class="accordion">
                        <? if (!empty($arResult['scopes_keys'])) { ?>
                            <div class="accordion__couple">
                                <div class="accordion__header">Сфера применения</div>
                                <div class="accordion__content">
                                    <? foreach ($arResult['scopes_keys'] as $key => $value) { ?>
                                        <div class="scope">
                                            <?=$arResult['HB']['SCOPES'][$key]['UF_NAME'];?>
                                        </div>
                                    <? } ?>
                                </div>
                            </div>
                        <? }
                        if ($arResult['OFFERS_PROPS']['PERFORMANCE']['HTML']) { ?>
                            <div class="accordion__couple">
                                <div class="accordion__header">Скорость подачи</div>
                                <div class="accordion__content">
                                    <?= $arResult['OFFERS_PROPS']['PERFORMANCE']['HTML'] ?>
                                </div>
                            </div>
                        <? }
                        if ($arResult['OFFERS_PROPS']['RANGE']['HORIZONTAL'] || $arResult['OFFERS_PROPS']['RANGE']['VERTICAL']) { ?>
                            <div class="accordion__couple">
                                <div class="accordion__header">Макс. расстояние подачи</div>
                                <div class="accordion__content">
                                    <? if ($arResult['OFFERS_PROPS']['RANGE']['HORIZONTAL']) {
                                        echo $arResult['OFFERS_PROPS']['RANGE']['HORIZONTAL']; ?> м (по горизонтали) /
                                    <? }
                                    if ($arResult['OFFERS_PROPS']['RANGE']['VERTICAL']) {
                                        echo $arResult['OFFERS_PROPS']['RANGE']['VERTICAL']; ?> м (по вертикали)
                                    <? } ?>
                                </div>
                            </div>
                        <? }
                        if (empty($arResult['OFFERS_PROPS']['SCOPES']) &&
                            !$arResult['OFFERS_PROPS']['PERFORMANCE']['HTML'] &&
                            (!$arResult['OFFERS_PROPS']['RANGE']['HORIZONTAL'] && !$arResult['OFFERS_PROPS']['RANGE']['VERTICAL']) &&
                            !empty($arResult['OFFERS_PROPS']['GENERAL'])
                        ) {
                            foreach ($arResult['OFFERS_PROPS']['GENERAL'] as $prop) { ?>
                                <div class="accordion__couple">
                                    <div class="accordion__header"><?= $prop['NAME'] ?></div>
                                    <div class="accordion__content">
                                        <?= $prop['VALUE'] . ' ' . $prop['HINT'] ?>
                                    </div>
                                </div>
                            <? }
                        } ?>
                    </div>
                    <div class="multicard__pump">
                        <? foreach ($arResult['pump_keys'] as $key => $value) { ?>
                            <div class="pump_ready"
                                 style="background-image: url(<?= $arResult['HB']['PUMP'][$key]['ICON'] ?>)"
                                 title="<?= $arResult['HB']['PUMP'][$key]['UF_FULL_DESCRIPTION'] ?>">
                            </div>
                        <? }
                        if (!empty($arResult['pump_keys']) && !empty($arResult['OFFERS_PROPS']['SUIT'])) { ?>
                            <div class="multicard__divisor"></div>
                        <? }
                        foreach ($arResult['OFFERS_PROPS']['SUIT'] as $suit) { ?>
                            <div class="pump_ready" style="background-image: url(<?= $suit['PICTURE'] ?>)" title="<?= $suit['TITLE'] ?>"></div>
                        <? } ?>
                    </div>
                </div>
            <? } ?>
            <div style="text-align: center">
                <button
                        class="btn btn--yellow modal_trigger--price_request"
                        data-model_name="<?= $arResult['NAME'] ?>"
                        data-model_link="<?= $arResult['FULL_PATH'] ?>"
                        data-model_id="<?= $arResult['ID'] ?>"
                        rel="nofollow">Узнать цену
                </button>
            </div>
            <noindex>
                <div class="multicard__links">
                    <a class="multicard__link docs" href="/service/documentation?id=<?=$arResult['ID']?>" rel="nofollow">
                        Документация
                    </a>
                    <a class="multicard__link scopes" href="/info/scopes?id=<?=$arResult['ID']?>" rel="nofollow">
                        Сферы применения
                    </a>
                </div>
            </noindex>
        </div>
    </div>
    <div class="multicard__bottom">
        <? if ($haveOffers) { ?>
            <table class="multicard__table">
                <thead>
                    <tr>
                        <? if ($haveOffers) { ?>
                            <th>Технические данные</th>
                            <? foreach ($arResult['OFFERS'] as $offer) { ?>
                                <th><?= $offer['NAME'] ?></th>
                            <? }
                        } else { ?>
                            <th colspan="2">Технические данные</th>
                        <? } ?>
                    </tr>
                </thead>
                <tbody>
                <? foreach ($arResult['tech_keys'] as $key => $value) { ?>
                    <tr>
                        <td><?=$value?></td>
                        <? foreach ($arResult['OFFERS'] as $offer) { ?>
                            <td>
                                <? if ($offer['PROPERTIES'][$key]['VALUE']) {
                                    if(isset($offer['PROPERTIES'][$key]['VALUE']['TEXT'])) {
                                        echo $offer['PROPERTIES'][$key]['VALUE']['TEXT'];
                                    } elseif(is_array($offer['PROPERTIES'][$key]['VALUE'])) {
                                        echo join(', ', $offer['PROPERTIES'][$key]['VALUE']);
                                    } else {
                                        echo $offer['PROPERTIES'][$key]['VALUE'].' '.$offer['PROPERTIES'][$key]['HINT'];
                                    }
                                } ?>
                            </td>
                        <? } ?>
                    </tr>
                <? } ?>
                </tbody>
            </table>
            <? if (!empty($arResult['extra_keys'])) { ?>
                <table class="multicard__table">
                    <thead>
                    <tr>
                        <? if ($haveOffers) { ?>
                            <th>Стандартное и дополнительное оборудование</th>
                            <? foreach ($arResult['OFFERS'] as $offer) { ?>
                                <th><?= $offer['NAME'] ?></th>
                            <? }
                        } else { ?>
                            <th colspan="2">Стандартное и дополнительное оборудование</th>
                        <? } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($arResult['extra_keys'] as $key => $value) { ?>
                        <tr>
                            <td><?= $value ?></td>
                            <? foreach ($arResult['OFFERS'] as $offer) { ?>
                                <td>
                                <span class="legend <? switch ($offer['PROPERTIES'][$key]['VALUE']) {
                                    case 'standart':
                                        echo 'standart';
                                        break;
                                    case 'custom':
                                        echo 'custom';
                                        break;
                                    default:
                                        echo 'not_available';
                                } ?>"></span>
                                </td>
                            <? } ?>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
                <div class="multicard__legend">
                    <span class="legend standart">Стандарт</span>
                    <span class="legend custom">По отельному заказу</span>
                    <span class="legend not_available">Нет в наличии</span>
                </div>
            <? } ?>
        <? } else { ?>
            <div class="html_content">
                <? if($arResult['DETAIL_TEXT']) { ?>
                    <?=$arResult['DETAIL_TEXT']?>
                <? } ?>
                <? if($arResult['PROPERTIES']['TECH_ITEM']['~VALUE']) { ?>
                    <div class="multicard__datasheet">Технические характеристики</div>
                    <?=$arResult['PROPERTIES']['TECH_ITEM']['~VALUE']['TEXT']?>
                <? } ?>
                <? if($arResult['PROPERTIES']['MODE_FEATURES']['~VALUE']) { ?>
                    <?=$arResult['PROPERTIES']['MODE_FEATURES']['~VALUE']['TEXT']?>
                <? } ?>
            </div>
        <? } ?>
    </div>
</div>
<div class="model__SEO">
    <? if(!empty($arResult['OFFERS_PROPS']['SEO']['DOP_FOTO'])) { ?>
        <div class="model__slider">
            <?foreach ($arResult['OFFERS_PROPS']['SEO']['DOP_FOTO'] as $foto) { ?>
                <div>
                    <img src="<?= $foto ?>">
                </div>
            <? } ?>
        </div>
    <? } ?>
    <div class="html_content">
        <?=$arResult['OFFERS_PROPS']['SEO']['DETAIL_TEXT'];?>
    </div>
</div>
<script>
    BX(function () {
        document.querySelectorAll('.multicard__slider').forEach(s => {
            new BX.Webest.ModelSlider(s).init();
        });
        document.querySelectorAll('.multicard__slider_dots').forEach(s => {
            new BX.Webest.ModelDotsSlider(s).init();
        });
        document.querySelectorAll('.model__slider').forEach(s => {
            new BX.Webest.SEOSlider(s).init();
        });
        document.querySelectorAll('.raw-slider').forEach(s => {
            new BX.Webest.Slider(s).init();
        });
    });
</script>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.link.list",
    "catalog",
    array(
        "AJAX_MODE" => "Y",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => "12",
        "LINK_PROPERTY_SID" => "RELATIONS",
        "ELEMENT_ID" => $arResult['ID'],
        "ELEMENT_SORT_FIELD" => "CATALOG_AVAILABLE",
        "ELEMENT_SORT_ORDER" => "desc",
        "ELEMENT_SORT_FIELD2" => "HAS_PREVIEW_PICTURE",
        "ELEMENT_SORT_ORDER2" => "desc",
        "FILTER_NAME" => "",
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "BASKET_URL" => "/personal/basket.php",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "SET_TITLE" => "N",
        "PAGE_ELEMENT_COUNT" => "4",
        "PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PRICE_CODE" => array(
            0 => "base",
        ),
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "300",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "LAZY_LOAD" => "Y",
        "MESS_BTN_LAZY_LOAD" => "Показать еще",
        "LOAD_ON_SCROLL" => "N",
        "PAGER_TITLE" => "Запчасти",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "arrows",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "HIDE_NOT_AVAILABLE" => "L",
        "CONVERT_CURRENCY" => "N",
        "AJAX_OPTION_JUMP" => "Y",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "PRODUCT_DISPLAY_MODE" => "N",
        "TEMPLATE_THEME" => "blue",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_CLOSE_POPUP" => "N",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "DISPLAY_COMPARE" => "N",
        "ADD_PICT_PROP" => "GALLERY",
        "LABEL_PROP" => "-",
        "SET_LAST_MODIFIED" => "N",
        "ADD_TO_BASKET_ACTION" => "ADD",
        "COMPONENT_TEMPLATE" => "catalog",
        "HIDE_NOT_AVAILABLE_OFFERS" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPATIBLE_MODE" => "N"
    ),
    false
);
$cp = $this->__component;
if (is_object($cp)) {
    $cp->arResult['SPECIAL_DETAIL_PICTURE'] = $arResult['DETAIL_PICTURE']['SRC'];
    $cp->SetResultCacheKeys(array('SPECIAL_DETAIL_PICTURE'));
    $arResult['SPECIAL_DETAIL_PICTURE'] = $cp->arResult['SPECIAL_DETAIL_PICTURE'];
}