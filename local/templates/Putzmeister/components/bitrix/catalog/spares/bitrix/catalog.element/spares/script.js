BX(function () {
    $('.popup_image').magnificPopup({
        type: 'image',
        mainClass: "mfp-fade",
        closeMarkup: '<button type="button" class="mfp-close btn btn--close"></button>'
    });
});