<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true); ?>
<div class="element__row">
    <div class="element__main">
        <div class="element__picture">
            <a class="popup_image" href="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>">
                <img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>">
            </a>
        </div>
        <? if ($arResult['PROPERTIES']['ARTICLE']['~VALUE']) { ?>
            <div class="element__article">
                Артикул: <?= $arResult['PROPERTIES']['ARTICLE']['~VALUE'] ?>
            </div>
            <div class="html_content">
                <?= $arResult['~DETAIL_TEXT'] ?>
                <?= $arResult['PROPERTIES']['TECH_PARAMETERS']['~VALUE']['TEXT'] ?>
                <?= $arResult['PROPERTIES']['NOTE']['~VALUE']['TEXT'] ?>
            </div>
        <? } ?>
    </div>
    <div class="element__side">
        <div class="element__availability<? if (!$arResult['PRODUCT']['QUANTITY']) echo " outofstock"; ?>">
            <? echo $arResult['PRODUCT']['QUANTITY'] ? "Есть " : "Нет " ?>в наличии
        </div>
        <? if (!empty($arResult['ITEM_PRICES'])) {
            $price = $arResult['ITEM_PRICES'][$arResult["ITEM_PRICE_SELECTED"]]; ?>
            <div class="element__prices">
                <meta itemprop="price" content="<?= $price['PRINT_BASE_PRICE'] ?>">
                <meta itemprop="priceCurrency" content="<?= $price['CURRENCY'] ?>">
                <div>
                    <span class="price price--rub"><?= $price['IN_RUBLES']; ?></span>
                </div>
                <div>
                    <span class="price price--eur"><?= $price['PRINT_RATIO_BASE_PRICE']; ?></span>
                </div>
            </div>
            <? if ($arResult['CAN_BUY']) { ?>
                <button
                        class="btn btn--yellow add2basketbtn"
                        data-product_id="<?= $arResult['ID'] ?>"
                        <?if(!$arResult['PRODUCT']['QUANTITY']) echo "disabled";?>>В корзину</button>
            <? } ?>
        <? } else { ?>
            <div>
                <button
                        class="btn btn--yellow modal_trigger--price_request"
                        data-model_name="<?= $arResult['NAME'] ?>"
                        data-model_link="<?= $arResult['DETAIL_PAGE_URL'] ?>">Узнать цену</button>
            </div>
        <? } ?>
    </div>
</div>