BX(function () {
    BX.namespace("BX.Webest");
    BX.Webest.CallbackForm = function (form, fields=null) {
        BX.Webest.CustomForm.call(this, form, fields);
        this.subscribeList = BX.findChild(this.form, {class: 'subscribe__list'}, true);
    };

    BX.extend(BX.Webest.CallbackForm, BX.Webest.CustomForm);
    BX.Webest.CallbackForm.prototype.init = function () {
        this.superclass.init.call(this);
        var modal_config = Object.assign({}, this.modal_config, {
            items: {src: document.getElementById('callback_request__modal')},
            modalTitle: "Заявка на консультацию",
            postActions: {
                beforeOpen: function () {
                    var form, modal_title;
                    if (modal_title = this.items[0].src.querySelector('.modal__title')) {
                        var modal_title_text = this.st.modalTitle;
                        var modal_title_text_data = this.st.el[0].dataset;
                        if (modal_title_text_data.suffix) {
                            modal_title_text += " " + modal_title_text_data.suffix;
                        }
                        modal_title.innerText = modal_title_text;
                        if (form = this.items[0].src.querySelector('form')) {
                            form.elements["fields[TITLE]"].value = modal_title_text;
                        }
                    }
                },
                close: function () {
                    var form;
                    if (form = this.items[0].src.querySelector("form")) {
                        form.reset();
                    }
                }
            }
        });
        $(".modal_trigger--consult").magnificPopup(modal_config);
        $(".modal_trigger--service").magnificPopup(Object.assign({}, modal_config, {
            modalTitle: "Заявка на услугу"
        }));
    };
    BX.Webest.CallbackForm.prototype._onReset = function (e) {
        this.superclass._onReset.call(this);
        this.subscribeList.classList.add("hidden");
    };
    BX.Webest.CallbackForm.prototype._onChange = function (e) {
        this.superclass._onChange.call(this, e);
        if(e.target.name === '[news][subscribe]') {
            this.subscribeList.classList.toggle('hidden', !e.target.checked);
        }
    };
});