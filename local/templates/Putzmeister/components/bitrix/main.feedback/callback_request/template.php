<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */?>
<div id="callback_request__modal" class="mfp-modal mfp-hide">
    <div class="modal__title">Заявка на консультацию</div>
    <form class="custom-form" id="callback_request__form" method="post" action="<?=$arParams["ACTION_URL"]?>">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="fields[TITLE]" value="Заявка на консультацию">
        <input type="hidden" name="fields[STATUS_ID]" value="NEW">
        <input type="hidden" name="fields[OPENED]" value="Y">
        <input type="hidden" name="fields[PHONE][0][VALUE_TYPE]" value="WORK">
        <input type="hidden" name="fields[MAIL][0][VALUE_TYPE]" value="WORK">
        <input type="hidden" name="fields[<?=$arParams["LEAD_FIELDS"]["REAL_LOCATION"]?>]" value="">
        <input type="hidden" name="fields[SOURCE_ID]" value="WEB">
        <input type="hidden" name="fields[TRACE]" value="">
        <label class="wrap__text">
            <input type="text" name="fields[NAME]" placeholder="ФИО*" pattern="^[A-Za-zА-Яа-яёЁ\s-]*$" required autocomplete="name">
            <span class="text__error"></span>
        </label>
        <label class="wrap__text">
            <input type="text" name="fields[<?=$arParams["LEAD_FIELDS"]["COMPANY"]?>]" placeholder="Наименование фирмы" maxlength="100" autocomplete="organization">
            <span class="text__error"></span>
        </label>
        <label class="wrap__text">
            <input type="text" name="fields[ADDRESS_CITY]" placeholder="Город*" pattern="^[А-Яа-яёЁ\s-]*$" required list="suggest_region_list" autocomplete="address-level2">
            <datalist id="suggest_region_list"></datalist>
            <span class="text__error"></span>
        </label>
        <label class="wrap__text">
            <input type="tel" name="fields[PHONE][0][VALUE]" placeholder="Телефон*" pattern="^\+\d{1}\s*\d{3}\s*\d{3}\s*\d{2}\s*\d{2}$" required autocomplete="tel">
            <span class="text__error"></span>
        </label>
        <label class="wrap__text">
            <input type="email" name="fields[PHONE][0][EMAIL]" placeholder="Email*" pattern="^.+@.+\.\w{2,3}$" required autocomplete="email">
            <span class="text__error"></span>
        </label>
        <textarea class="wrap__textarea" name="fields[COMMENTS]" placeholder="Сообщение" maxlength="2000"></textarea>
        <label class="wrap__checkbox news_subscribe">
            <input type="checkbox" name="[news][subscribe]">
            <span class="checkmark"></span>
            <span>Я хочу получать новости от Putzmeister</span>
        </label>
        <div class="wrap__select subscribe__list hidden">
            <div class="select__selected">
                Выберите категорию...
            </div>
            <div class="select__list">
                <label class="list__item">
                    <span>Выберите категорию...</span>
                    <input type="radio" name="[news][category]" value="">
                </label>
                <? foreach ($arResult['NEWS'] as $post) { ?>
                    <label class="list__item">
                        <span><?= $post['NAME'] ?></span>
                        <input type="radio" name="[news][category]" value="<?=$post['ID']?>">
                    </label>
                <? } ?>
            </div>
        </div>
        <?if($arParams["USE_CAPTCHA"] == "Y") { ?>
            <div class="mf-captcha">
                <div class="mf-text"><?= GetMessage("MFT_CAPTCHA") ?></div>
                <input type="hidden" name="captcha_sid" value="<?= $arResult["capCode"] ?>">
                <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["capCode"] ?>" alt="CAPTCHA">
                <input type="text" name="captcha_word"value="">
            </div>
        <? } ?>
        <input class="btn btn--yellow" type="submit" value="Отправить запрос">
        <?$APPLICATION->IncludeComponent(
            'bitrix:main.userconsent.request',
            'putzmeister',
            array(
                'ID' => 1,
                'IS_CHECKED' => "Y",
                'IS_LOADED' => "N",
                'AUTO_SAVE' => 'N',
                'INPUT_NAME' => 'consent',
                'SUBMIT_EVENT_NAME' => 'bx-soa-order-save',
                'REPLACE' => array(
                    'button_caption' => isset($arParams['~MESS_ORDER']) ? $arParams['~MESS_ORDER'] : $arParams['MESS_ORDER'],
                    'fields' => $arResult['USER_CONSENT_PROPERTY_DATA']
                )
            )
        )?>
        <div class="form__errors"></div>
    </form>
</div>
<script>
    BX(function () {
        new BX.Webest.CallbackForm(
            BX('callback_request__form'),
            {
                model: "<?=CUtil::JSEscape($arParams["LEAD_FIELDS"]["MODEL"])?>",
                realLocation: "<?=CUtil::JSEscape($arParams["LEAD_FIELDS"]["REAL_LOCATION"])?>"
            }
        ).init();
    });
</script>