BX(function () {
    BX.namespace("BX.Webest");
    BX.Webest.CancelOrderForm = function (form) {
        BX.Webest.CustomForm.call(this, form);
        this.submit_message_modal_config.modalTitle = "Ваш заказ отменен";
    };
    BX.extend(BX.Webest.CancelOrderForm, BX.Webest.CustomForm);
    BX.Webest.CancelOrderForm.prototype.init = function() {
        this.superclass.init.call(this);
        var modal_config = Object.assign({}, this.modal_config, {
            items: {src: document.getElementById('cancel_order_modal')}
        });
        modal_config.postActions.beforeOpen = function () {
            var id, form;
            if (id = this.st.el[0].dataset.id) {
                if(form = this.items[0].src.querySelector("form")) {
                    try {
                        form.elements["order_id"].value = id;
                    } catch (e) {}
                }
            }
        };
        $(".modal-trigger--cancel_order").magnificPopup(modal_config);
    };

    BX.Webest.CancelOrderForm.prototype._sendRequest = function() {
        var form_data = new FormData(this.form);
        var XHR = new XMLHttpRequest();
        XHR.onload = this._onSuccess.bind(this);
        XHR.onerror = this._onError.bind(this);
        XHR.onloadend = this._onComplete.bind(this);
        XHR.responseType = 'json';
        XHR.open(this.form.method, this.form.action, true);
        XHR.send(form_data);
    }

    BX.Webest.CancelOrderForm.prototype._onSuccess = function(e) {
        if (!!e.target.response.reload) {
            var event = e;
            this.submit_message_modal_config.callbacks.close = () => location.href = event.target.response.url;
        }
        this.superclass._onSuccess.call(this, e);
    };
});