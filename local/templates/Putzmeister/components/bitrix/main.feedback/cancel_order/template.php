<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div id="cancel_order_modal" class="mfp-modal mfp-hide">
    <div class="modal__title">Отмена заказа</div>
    <form class="custom-form" id="cancel_order_form" method="post" action="/api/ajax.php?action=cancel_order">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="order_id" value="">
        <textarea name="reason" maxlength="2000" cols="50" rows="10" placeholder="Вы можете указать причину отмены"></textarea>
        <input class="btn btn--yellow" type="submit" value="Отменить">
        <div class="form__errors"></div>
    </form>
</div>
<script>
    BX(function () {
        new BX.Webest.CancelOrderForm(BX('cancel_order_form')).init();
    });
</script>