BX(function () {
    BX.Webest.ForgotPasswordForm = function (form) {
        BX.Webest.CustomForm.call(this, form);
    };
    BX.extend(BX.Webest.ForgotPasswordForm, BX.Webest.CustomForm);
    BX.Webest.ForgotPasswordForm.prototype.init = function (e) {
        this.superclass.init.call(this);
        var modal_config = Object.assign({}, this.modal_config);
        delete modal_config['items'];
        $(".modal_trigger--login").magnificPopup(modal_config);
    };
    BX.Webest.ForgotPasswordForm.prototype._sendRequest = function() {
        var form_data = new FormData(this.form);
        form_data.delete("sessid");
        var XHR = new XMLHttpRequest();
        XHR.onload = this._onSuccess.bind(this);
        XHR.onerror = this._onError.bind(this);
        XHR.onloadend = this._onComplete.bind(this);
        XHR.open(this.form.method, this.form.action, true);
        XHR.send(form_data);
    };
    BX.Webest.ForgotPasswordForm.prototype._onSuccess = function (e) {
        var error_node, note_node, dummy;
        dummy = document.createElement("div");
        dummy.innerHTML = e.target.response;
        if(error_node = dummy.querySelector('font.errortext')) {
            this.form_errors.innerText = error_node.innerText.trim();
            e.target.data = error_node.innerText.trim();
            e.target.dispatchEvent(new Event("error"));
        } else if(note_node = dummy.querySelector('font.notetext')) {
            this.form.reset();
            $.magnificPopup.open(this.submit_message_modal_config);
            $.magnificPopup.instance.content[0].querySelector(".modal__title").innerText = note_node.innerText.trim();
        }
    };

    BX.Webest.ForgotPasswordForm.prototype._onError = function (e) {
        this.form_errors.innerText = e.target.data || 'ошибка';
        this.form.classList.add("error");
    };
});