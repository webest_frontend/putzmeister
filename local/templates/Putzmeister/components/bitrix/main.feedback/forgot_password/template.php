<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */?>
<div id="forgot_password__modal" class="mfp-modal mfp-hide">
    <div class="modal__title">Восстановление пароля</div>
    <form
            class="custom-form"
            id="forgot_password__form"
            name="bform"
            method="post"
            action="/auth/?forgot_password=yes">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="backurl" value="/">
        <input type="hidden" name="AUTH_FORM" value="Y">
        <input type="hidden" name="TYPE" value="SEND_PWD">
        <input type="hidden" name="USER_EMAIL">
        <div>Выберите, какую информацию использовать для изменения пароля:</div>
        <div>
            <label>
                <span>Логин или email:</span>
                <input type="text" name="USER_LOGIN" value="admin" oninput="this.form.elements['USER_EMAIL'].value = this.value">
            </label>
        </div>
        <div>Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы вам по email.</div>
        <div>
            <input type="submit" name="send_account_info" value="Выслать">
        </div>
        <div class="form__errors"></div>
    </form>
</div>
<script>
    BX(function () {
        new BX.Webest.ForgotPasswordForm(BX('forgot_password__form')).init();
    });
</script>