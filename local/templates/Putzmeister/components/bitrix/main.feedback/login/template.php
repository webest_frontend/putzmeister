<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */?>
<div id="login__modal" class="mfp-modal mfp-hide">
    <div class="modal__title">Вход</div>
    <form class="custom-form" id="login__form" method="post" action="/api/ajax.php?action=login">
        <?=bitrix_sessid_post()?>
        <div class="form__error"></div>
        <div class="wrap__text">
            <input type="email" name="email" placeholder="Email" required autocomplete="username">
            <span class="text__error"></span>
        </div>
        <div class="wrap__text">
            <input type="password" name="password" placeholder="Пароль" required autocomplete="current-password">
            <span class="text__error"></span>
        </div>
        <input class="btn btn--yellow" type="submit" name="Login" value="Войти">
        <a class="modal_trigger--login" href="#forgot_password__modal">Забыли пароль?</a>
        <div class="form__errors"></div>
    </form>
</div>
<script>
    BX(function () {
        new BX.Webest.LoginForm(BX('login__form')).init();
    });
</script>