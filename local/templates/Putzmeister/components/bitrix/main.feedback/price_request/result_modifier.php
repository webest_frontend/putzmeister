<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arResult['NEWS'] = [
    [
        'NAME' => 'Бетононасосы',
        'CODE' => 'betononasosy'
    ],
    [
        'NAME' => 'Горно-Шахтное оборудование',
        'CODE' => 'gorno-shakhtnoe_oborudovanie'
    ],
    [
        'NAME' => 'Растворонасосы',
        'CODE' => 'rastvoronasosy'
    ],
    [
        'NAME' => 'Торкрет оборудование',
        'CODE' => 'torkret_oborudovanie'
    ],
    [
        'NAME' => 'Шламовые насосы',
        'CODE' => 'shlamovye_nasosy'
    ]
];