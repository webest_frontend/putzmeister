BX(function () {
    BX.Webest.PriceForm = function (form, fields=null) {
        BX.Webest.CustomForm.call(this, form, fields);
        this.modal = this.form.closest(".mfp-modal");
        this.subscribeList = BX.findChild(this.form, {class: 'subscribe__list'}, true);
    };
    BX.extend(BX.Webest.PriceForm, BX.Webest.CustomForm);
    BX.Webest.PriceForm.prototype.init = function () {
        this.superclass.init.call(this);
        BX.bindDelegate(
            document,
            'click',
            {class: 'modal_trigger--price_request'},
            this._openModal.bind(this)
        );
    };
    BX.Webest.PriceForm.prototype._openModal = function (e) {
        var data, self, modal_config;
        data = e.target.dataset;
        self = this;
        modal_config = Object.assign({}, this.modal_config, {
            items: {
                src: this.modal
            }
        });
        modal_config.postActions.beforeOpen = function () {
            var modal_form, modal_title;
            if(modal_form = this.items[0].src.querySelector("form")) {
                if (data.model_name) {
                    modal_form.elements[`fields[${self.fieldsCodes.title}]`].value = data.model_name;
                    if(modal_title = self.modal.querySelector(".modal__title")) {
                        modal_title.innerText = "Узнать цену " + data.model_name;
                    }
                }
                if (data.model_link) {
                    modal_form.elements[`fields[${self.fieldsCodes.model}]`].value = data.model_link;
                }
            }
        };
        $.magnificPopup.open(modal_config);
    };
    BX.Webest.CallbackForm.prototype._onReset = function (e) {
        this.superclass._onReset.call(this);
        this.subscribeList.classList.add("hidden");
    };
    BX.Webest.PriceForm.prototype._onChange = function (e) {
        this.superclass._onChange.call(this, e);
        if(e.target.name === '[news][subscribe]') {
            this.subscribeList.classList.toggle('hidden', !e.target.checked);
        }
        if (e.target.name === "fields[" + this.fieldsCodes.model + "]") {
            this.form.elements["fields[" + this.fieldsCodes.title + "]"].value = e.target.previousElementSibling.innerText.trim();
        }
    };
});