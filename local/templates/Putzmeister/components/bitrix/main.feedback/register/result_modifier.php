<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Loader::includeModule("sale");
$cursor = \Bitrix\Sale\Property::getList();
$arResult["REG_FIELDS"] = [
    1 => "",
    2 => ""
];
$arrFields = [];
while($buf = $cursor->Fetch()) {
    $arrFields[$buf["ID"]] = $buf;
}
$arResult["REG_FIELDS"] = [
    1 => array_filter($arrFields, function ($f) {
        return $f["PERSON_TYPE_ID"] == 1;
    }),
    2 => array_filter($arrFields, function ($f) {
        return $f["PERSON_TYPE_ID"] == 2;
    })
];