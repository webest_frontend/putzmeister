BX(function () {
    BX.Webest.LoginForm = function (form) {
        BX.Webest.CustomForm.call(this, form);
    };
    BX.extend(BX.Webest.LoginForm, BX.Webest.CustomForm);
    new BX.Webest.LoginForm(BX('login__form')).init();
});