<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */?>
<div id="register__modal" class="mfp-modal mfp-hide">
    <h2 class="modal__header">Регистрация</h2>
    <form class="custom-form" id="register__form" method="post" action="/api/ajax.php?action=register">
        <?=bitrix_sessid_post()?>
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#phys_props">ФИЗИЧЕСКОЕ ЛИЦО</a>
            </li>
            <li>
                <a data-toggle="tab" href="#jur_props">ЮРИДИЧЕСКОЕ ЛИЦО</a>
            </li>
        </ul>
        <div class="tab-content">
            <? $input_type = "text";
                foreach ($arResult["REG_FIELDS"] as $field) {
                    if($field["IS_EMAIL"] == "Y") {
                        $input_type = "email";
                    } elseif ($field["IS_PHONE"] == "Y") {
                        $input_type = "tel";
                    } ?>
                    <label class="wrap__text">
                        <span class="text__error"></span>
                        <input
                                type="text"
                                name="<?=$data["ID"]?>"
                                placeholder="<?=$data["REQUIRED"] == "Y" ? $data["NAME"].'*' : $data["NAME"];?>"
                            <?if($data["REQUIRED"] == "Y") echo " required";?>
                            <?if($data["PATTERN"]) echo "pattern='".$data["PATTERN"]."'";?>>
                    </label>
                <? } ?>
            <div id="phys_props" class="tab-pane fade in active"></div>
            <div id="menu1" class="tab-pane fade"></div>
        </div>
            <div class="wrap__text">
                <input type="email" name="email" placeholder="Email" required>
                <span class="text__error"></span>
            </div>
            <div class="wrap__text">
                <input type="password" name="password" placeholder="Пароль" required>
                <span class="text__error"></span>
            </div>
        </div>
        <a href="javascript:void(0)">Забыли пароль?</a>
        <input type="submit" name="Login" value="Войти">
        <div class="form__errors"></div>
    </form>
</div>