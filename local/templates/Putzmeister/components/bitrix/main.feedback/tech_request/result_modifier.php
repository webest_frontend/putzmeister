<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$cursor = \Bitrix\Iblock\ElementTable::getList([
    'select' => [
        'IBLOCK_ID',
        'ID',
        'CODE',
        'NAME',
        'IBLOCK_SECTION_ID',
        'DETAIL_PAGE_URL' => 'IBLOCK.DETAIL_PAGE_URL'
    ],
    'filter' => [
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => $arParams["MODELS_IBLOCK_ID"]
    ]
]);
$filtered_sections = [];
$path_prefix = $_SERVER["HTTPS"] ? "https" : "http"."://".$_SERVER["HTTP_HOST"];
while($buf = $cursor->Fetch()) {
    $buf['DETAIL_PAGE_URL'] = $path_prefix.CIBlock::ReplaceDetailUrl($buf['DETAIL_PAGE_URL'], $buf, false, 'E');
    $arResult['MODELS'][$buf['ID']] = $buf;
    $filtered_sections[$buf['IBLOCK_SECTION_ID']] = "";
}
$cursor = \Bitrix\Iblock\SectionTable::getList([
    'select' => [
        'IBLOCK_ID',
        'ID',
        'NAME'
    ],
    'filter' => [
        'IBLOCK_ID' => $arParams["MODELS_IBLOCK_ID"],
        'ID' => array_keys($filtered_sections)
    ]
]);

while($buf = $cursor->Fetch()) {
    $arResult['SECTIONS'][$buf['ID']] = $buf;
}

$arResult['NEWS'] = [
    [
        'NAME' => 'Бетононасосы',
        'CODE' => 'betononasosy'
    ],
    [
        'NAME' => 'Горно-Шахтное оборудование',
        'CODE' => 'gorno-shakhtnoe_oborudovanie'
    ],
    [
        'NAME' => 'Растворонасосы',
        'CODE' => 'rastvoronasosy'
    ],
    [
        'NAME' => 'Торкрет оборудование',
        'CODE' => 'torkret_oborudovanie'
    ],
    [
        'NAME' => 'Шламовые насосы',
        'CODE' => 'shlamovye_nasosy'
    ]
];