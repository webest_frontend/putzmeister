BX(function () {
    BX.namespace("BX.Webest");
    BX.Webest.TechForm = function (form, fields=null) {
        BX.Webest.CustomForm.call(this, form, fields);
        this.modelsList = BX.findChild(this.form, {class: 'filter_models'}, true);
        this.subscribeList = BX.findChild(this.form, {class: 'subscribe__list'}, true);
    };
    BX.extend(BX.Webest.TechForm, BX.Webest.CustomForm);
    BX.Webest.TechForm.prototype.init = function () {
        this.superclass.init.call(this);
        var modal_config = Object.assign({}, this.modal_config, {
            items: {src: document.getElementById('tech_request_modal')},
            modalTitle: "Запрос на технику",
            postActions: {
                beforeOpen: function () {
                    var form, modal_title;
                    if (modal_title = this.items[0].src.querySelector('.modal__title')) {
                        var modal_title_text = this.st.modalTitle;
                        var modal_title_text_data = this.st.el[0].dataset;
                        if (modal_title_text_data.suffix) {
                            modal_title_text += " " + modal_title_text_data.suffix;
                        }
                        modal_title.innerText = modal_title_text;
                        if (form = this.items[0].src.querySelector('form')) {
                            form.elements["fields[TITLE]"].value = modal_title_text;
                        }
                    }
                },
                close: function () {
                    var form;
                    if (form = this.items[0].src.querySelector("form")) {
                        form.reset();
                    }
                }
            }
        });
        $('.modal_trigger--tech').magnificPopup(modal_config);
    };
    BX.Webest.TechForm.prototype._onReset = function (e) {
        this.superclass._onReset.call(this);
        this.subscribeList.classList.add("hidden");
        this.modelsList.classList.add("disabled");
    };

    BX.Webest.TechForm.prototype._onChange = function (e) {
        var target, category_id, matchedModels;
        this.superclass._onChange.call(this, e);
        if(e.target.name === 'category') {
            target = e.target.closest('[data-id]');
            category_id = target.dataset.id;
            matchedModels = Array
                .from(this.modelsList.querySelectorAll('label'))
                .filter(d => {
                    var match = d.dataset.id === category_id;
                    d.classList.toggle('hidden', !match);
                    return match;
                });
            this.modelsList.classList.toggle('disabled', !matchedModels.length);
            this.selectInputs.filter(i => i.name === 'fields[' + this.fieldsCodes.model + ']').forEach(i => i.reset());
        } else if (e.target.name === '[news][subscribe]') {
            this.subscribeList.classList.toggle('hidden', !e.target.checked);
        }
        if (e.target.name === "fields[" + this.fieldsCodes.model + "]") {
            this.form.elements["fields[" + this.fieldsCodes.title + "]"].value = e.target.previousElementSibling.innerText.trim();
        }
    };
});