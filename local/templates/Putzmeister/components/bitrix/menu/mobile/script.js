BX(function() {
	var node;
	BX.namespace("BX.Webest");
	BX.Webest.Menu = function (menu) {
		this.menu = menu;
		this._init.call(this);
	};
	BX.Webest.Menu.prototype = {
		_init: function () {
			BX.bind(BX("appBody"), 'click', BX.delegate(this._onClick, this));
			return this;
		},
		_onClick: function (e) {
			var isActive, parent;
			isActive = this.menu.classList.contains("activated");
			if (e.target.closest(".menu__trigger")) {
				e.preventDefault();
				document.body.classList.toggle('scroll-block', !isActive);
				this.menu.classList.toggle('activated', !isActive);
				this.menu.querySelectorAll(".parent").forEach(l => BX.removeClass(l, "expanded"));
			} else if (parent = e.target.closest('.parent')) {
				this.menu.querySelectorAll('.parent').forEach(e => {
					if(Object.is(e, parent)) {
						e.classList.toggle('expanded');
					} else {
						e.classList.remove('expanded');
					}
				});
			} else {
				BX.removeClass(this.menu, "activated");
				BX.removeClass(BX("appBody"), 'scroll-block');
				this.menu.querySelectorAll(".parent").forEach(l => BX.removeClass(l, "expanded"));
			}
		}
	};

	if(node = document.querySelector("#mobile-multilevel-menu")) {
		new BX.Webest.Menu(node);
	}
});