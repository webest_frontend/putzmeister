<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<img class="menu__trigger" src="<?= SITE_TEMPLATE_PATH ?>/images/mobile/menu.png">
<div class="menu__container" id="mobile-multilevel-menu">
    <?if (!empty($arResult)) { ?>
        <ul class="menu__list">
            <? $previousLevel = 0;
            foreach ($arResult as $arItem) {
            $query = $arItem['PARAMS']['query'];
            if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
                echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
            <? }
            if ($arItem["IS_PARENT"]) {
                if ($arItem["DEPTH_LEVEL"] == 1) { ?>
                    <li class="menu__item parent">
                        <a href="<?= $arItem["LINK"] ?>">
                            <?= $arItem["TEXT"] ?>
                        </a>
                        <ul class="menu__item_child">
                <? } else { ?>
                    <div class="menu__item cover-link">
                        <a href="<?= $arItem["LINK"] ?>">
                            <?= $arItem["TEXT"] ?>
                        </a>
                        <ul class="menu__item_child">
                <? }
            } else {
                if ($arItem["PERMISSION"] > "D") {
                    if ($arItem["DEPTH_LEVEL"] == 1) { ?>
                        <li class="menu__item">
                            <a href="<?= $arItem["LINK"] ?>">
                                <?= $arItem["TEXT"] ?>
                            </a>
                        </li>
                    <? } else { ?>
                        <li class="menu__item">
                            <a href="<?= $arItem["LINK"] ?>">
                                <?= $arItem["TEXT"] ?>
                            </a>
                        </li>
                    <? }
                } else { ?>
                    <li class="menu__item" title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>">
                        <?= $arItem["TEXT"] ?>
                    </li>
                <? }
            }
            $previousLevel = $arItem["DEPTH_LEVEL"];
            }
            if ($previousLevel > 1) {
                echo str_repeat("</ul></li>", ($previousLevel - 1));
            } ?>
            </ul>
    <? } ?>
    <div class="mobile-visible">
        <? $APPLICATION->IncludeComponent(
            "bitrix:search.form",
            "mobile",
            Array(
                "USE_SUGGEST" => "Y",
                "PAGE" => "#SITE_DIR#search/index.php"
            )
        ); ?>
    </div>
</div>