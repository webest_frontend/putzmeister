<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (!empty($arResult)) { ?>
    <ul class="footer__menu">
    <? $previousLevel = 0;
    foreach($arResult as $arItem) {
        if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) { ?>
            <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
        }
        if ($arItem["IS_PARENT"]) {
            if ($arItem["DEPTH_LEVEL"] == 1) { ?>
                <li class="root-item">
                    <a href="<?=$arItem["LINK"]?>">
                        <?=$arItem["TEXT"]?>
                    </a>
                    <ul>
            <? } else { ?>
                <li class="<?if($arItem['PARAMS']['ROOT-ITEM']) echo ' root-item';?>">
                    <a href="<?=$arItem["LINK"]?>" >
                        <?=$arItem["TEXT"]?>
                    </a>
                    <ul>
            <? }
        } else {
            if ($arItem["PERMISSION"] > "D") {
                if ($arItem["DEPTH_LEVEL"] == 1) { ?>
                    <li class="root-item">
                        <a href="<?=$arItem["LINK"]?>">
                            <?=$arItem["TEXT"]?>
                        </a>
                    </li>
                <? } else {
                    if($arItem['PARAMS']['PM_POST']) { ?>
                        <li class="footer__menu_item--with_picture root-item">
                            <div class="footer__menu_item_title root-item"><?=$arItem["TEXT"]?></div>
                            <img src="<?=SITE_TEMPLATE_PATH.$arItem['PARAMS']['PM_POST']?>">
                            <a href="<?=$arItem["LINK"]?>"></a>
                        </li>
                    <? } else if($arItem['PARAMS']['GOOGLE_PLAY']) { ?>
                        <li class="footer__mobile_apps mobile_apps__link--google_play">
                            <a href="<?=$arItem["LINK"]?>">
                                <img src="<?=$arItem['PARAMS']['GOOGLE_PLAY']?>">
                            </a>
                        </li>
                    <? } else if($arItem['PARAMS']['APP_STORE']) { ?>
                        <li class="footer__mobile_apps mobile_apps__link--app_store">
                            <a href="<?=$arItem["LINK"]?>">
                                <img src="<?=$arItem['PARAMS']['APP_STORE']?>">
                            </a>
                        </li>
                    <? } else { ?>
                        <li class="<?if($arItem['PARAMS']['ROOT-ITEM']) echo ' root-item';?>">
                            <a href="<?=$arItem["LINK"]?>">
                                <?=$arItem["TEXT"]?>
                            </a>
                        </li>
                    <? }
                }
            } else { ?>
                <li title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></li>
            <? }
        }
        $previousLevel = $arItem["DEPTH_LEVEL"];
    }
    if ($previousLevel > 1) {
        echo str_repeat("</ul></li>", ($previousLevel-1) );
    } ?>
    </ul>
<? }