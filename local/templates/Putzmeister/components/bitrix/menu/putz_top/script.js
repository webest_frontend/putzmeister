BX(function() {
	BX.bind(BX('horizontal-multilevel-menu'), 'mouseover', function (e) {
		var target, container;
		if (target = e.target.closest('.parent')) {
			BX.addClass(target, 'expanded');
			BX.addClass(BX('appBody'), 'shadowed');
		}
		if (target = e.target.closest('.header__menu_item:not(.parent)')) {
			if(container = e.target.closest('.header__menu_item_child')) {
				container.classList.add("faded");
			}
		}
	});
	BX.bindDelegate(BX('horizontal-multilevel-menu'), 'mouseout', {tag: 'li'}, function (e) {
		var target, container;
		if (target = e.target.closest('.parent')) {
			BX.removeClass(target, 'expanded');
			BX.removeClass(BX('appBody'), 'shadowed');
		}
		if (target = e.target.closest('.header__menu_item:not(.parent)')) {
			if(container = e.target.closest('.header__menu_item_child')) {
				container.classList.remove("faded");
			}
		}
	});
});