<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)) { ?>
    <ul class="header__menu" id="horizontal-multilevel-menu">
        <? $previousLevel = 0;
        foreach ($arResult as $arItem) {
            $query = $arItem['PARAMS']['query'];
            if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
                echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
            }
            if ($arItem["IS_PARENT"]) {
                if ($arItem["DEPTH_LEVEL"] == 1) { ?>
                    <li class="header__menu_item parent">
                        <a href="<?= $arItem["LINK"] ?>">
                            <?= $arItem["TEXT"] ?>
                        </a>
                        <ul class="header__menu_item_child">
                <? } else { ?>
                    <div class="header__menu_item">
                        <a href="<?= $arItem["LINK"] ?>">
                            <?= $arItem["TEXT"] ?>
                        </a>
                        <ul class="header__menu_item_child">
                    <? }
            } else {
                if ($arItem["PERMISSION"] > "D") {
                    if ($arItem["DEPTH_LEVEL"] == 1) { ?>
                        <li class="header__menu_item">
                            <a href="<?= $arItem["LINK"];
                            if ($query) echo '?' . $query; ?>">
                                <?= $arItem["TEXT"] ?>
                            </a>
                        </li>
                    <? } else { ?>
                        <li class="header__menu_item">
                            <a href="<?= $arItem["LINK"];
                            if ($query) echo '?' . $query; ?>">
                                <?= $arItem["TEXT"] ?>
                            </a>
                        </li>
                    <? }
                } else { ?>
                    <li class="header__menu_item" title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>">
                        <?= $arItem["TEXT"] ?>
                    </li>
                <? }
            }
            $previousLevel = $arItem["DEPTH_LEVEL"];
        }
        if ($previousLevel > 1) {
            echo str_repeat("</ul></li>", ($previousLevel - 1));
        } ?>
        </ul>
<? } ?>
