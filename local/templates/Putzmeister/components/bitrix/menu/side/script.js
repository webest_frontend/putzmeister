BX.ready(function () {
    $('#open_menu').click(function (e) {
        $(e.target)
            .closest('li')
            .toggleClass('expanded');
    });
});