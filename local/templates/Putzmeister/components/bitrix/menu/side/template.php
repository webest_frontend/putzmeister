<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)) { ?>
    <ul id="open_menu" class="menu--side">
    <? $previousLevel = 0;
    foreach($arResult as $arItem) {
        if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
            echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
        }
        if ($arItem["IS_PARENT"]) { ?>
            <li<? if($arItem["SELECTED"]) echo ' class="active expanded"';?>>
                <div class="menu__text parent"><?=$arItem["TEXT"]?></div>
                <a href="<?=$arItem["LINK"]?>"></a>
                <ul class="first-child">
        <? } else {
            if ($arItem["PERMISSION"] > "D") {
                if ($arItem["DEPTH_LEVEL"] == 1) { ?>
                    <li<? if($arItem["SELECTED"]) echo ' class="active"';?>>
                        <div class="menu__text"><?=$arItem["TEXT"]?></div>
                        <a href="<?=$arItem["LINK"]?>"></a>
                    </li>
                <? } else { ?>
                    <li<? if($arItem["SELECTED"]) echo ' class="active"';?>>
                        <div class="menu__text"><?=$arItem["TEXT"]?></div>
                        <a href="<?=$arItem["LINK"]?>"></a>
                    </li>
                <? } ?>
            <? } ?>
        <? }
        $previousLevel = $arItem["DEPTH_LEVEL"];
    }
    if ($previousLevel > 1) {
        echo str_repeat("</ul></li>", ($previousLevel - 1)); ?>
    <? } ?>
    </ul>
    <div class="side__links">
        <a class="side__links_link docs" href="/service/documentation/">
            Документация
        </a>
        <a class="side__links_link scopes" href="/info/scopes/">
            Сферы применения
        </a>
    </div>
<? } ?>