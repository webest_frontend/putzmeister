<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arResult['PROPERTIES']['GALLERY']['VALUE'] = array_map(
    function($i) {
        $src = CFile::GetPath($i);
        return $src;
    },
    $arResult['PROPERTIES']['GALLERY']['VALUE']
);