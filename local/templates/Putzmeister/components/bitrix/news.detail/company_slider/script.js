BX.ready(function () {
    var mfpOpen = true;
    $("#top_gallery")
        .on('beforeChange', () => mfpOpen = false)
        .on('afterChange', () => mfpOpen = true)
        .slick({
            mobileFirst: true,
            arrows: true,
            prevArrow: '<button type="button" class="arrow arrow--simple"></button>',
            nextArrow: '<button type="button" class="arrow arrow--simple next"></button>',
            slidesToShow: 1,
            slidesToScroll: 1,
            waitForAnimate: false,
            infinite: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    $('.popup-image').magnificPopup({
        type: 'image',
        gallery:{
            enabled: true
        },
        disableOn: () => mfpOpen,
        mainClass: "mfp-fade",
        closeMarkup: '<button class="mfp-close btn btn--close" type="button"></button>'
    });
});