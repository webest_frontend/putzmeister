<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
CJSCore::Init("slick_carousel"); ?>
<? if (!empty($arResult["PROPERTIES"]["GALLERY"]["VALUE"])) { ?>
    <div id="top_gallery">
        <? foreach ($arResult["PROPERTIES"]["GALLERY"]["VALUE"] as $slide) { ?>
            <div>
                <a href="<?= $slide ?>" class="popup-image">
                    <img src="<?= $slide ?>">
                </a>
            </div>
        <? } ?>
    </div>
<? } ?>