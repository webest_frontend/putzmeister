<?
$arSections = CIBlockSection::GetList(
    [],
    ['IBLOCK_ID' => $arParams['IBLOCK_ID']],
    false,
    ['IBLOCK_ID', 'ID', 'NAME']
);
$arResult['SECTIONS'] = [];
while($buf = $arSections->Fetch()) {
    $elements = array_filter(
        $arResult['ITEMS'],
        function ($i) use($buf) {
            return $i['IBLOCK_SECTION_ID'] == $buf['ID'];
        }
    );
    $arResult['SECTIONS'][] = array_merge($buf, ['ELEMENTS' => $elements]);
} ?>
<?/*<pre>
    <?=print_r($arResult['SECTIONS']);?>
</pre>*/?>

