<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
if(!empty($arResult["SECTIONS"])) { ?>
    <div class="contacts__rows">
    <? foreach ($arResult["SECTIONS"] as $arSection) { ?>
        <div class="contacts__row">
            <div class="contacts__row_title">
                <?= $arSection['NAME']; ?>
            </div>
            <div class="contacts__persons">
                <? foreach ($arSection['ELEMENTS'] as $arItem) {
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>
                    <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <div class="contacts__person">
                            <div class="person__name">
                                <?= $arItem['PROPERTIES']['NAME']['VALUE']; ?>
                            </div>
                            <div class="person__position">
                                <?= $arItem['PROPERTIES']['POSITION']['VALUE']; ?>
                            </div>
                            <div class="person__contacts">
                                <? if ($arItem['PROPERTIES']['PHONE']['VALUE']) { ?>
                                    <div>доб. <?=$arItem['PROPERTIES']['PHONE']['VALUE']; ?></div>
                                <? } ?>
                                <? if($arItem['PROPERTIES']['EMAIL']['VALUE']) { ?>
                                    <div>
                                        <a href="mailto:<?= $arItem['PROPERTIES']['EMAIL']['VALUE']; ?>?subject=%D0%9F%D0%B8%D1%81%D1%8C%D0%BC%D0%BE%20%D1%81%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0">
                                            <?= $arItem['PROPERTIES']['EMAIL']['VALUE']; ?>
                                        </a>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>
    <? }
} ?>