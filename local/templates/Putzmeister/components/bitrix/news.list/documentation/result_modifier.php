<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
function walk_elements (&$i) {
    $i['PROPERTIES']['DOC_FILE_GALLERY']['VALUE'] = array_filter(
        array_map(
            function ($e) {
                $file = CFile::GetFileArray($e);
                if ($file) {
                    $file['FILE_SIZE'] = CFile::FormatSize($file['FILE_SIZE']);
                    return $file;
                } else {
                    return false;
                }
            },
            $i['PROPERTIES']['DOC_FILE_GALLERY']['VALUE']
        ),
        function ($e) {
            return $e;
        }
    );
}
function walk_sections (&$s, $key, $result) {
    $nested_sections = array_filter(
        $result,
        function ($ss) use($s) {
            return $ss['IBLOCK_SECTION_ID'] == $s['ID'];
        }
    );
    if (count($nested_sections)) {
        array_walk($nested_sections,'walk_sections', $result);
        $s['SUBSECTIONS'] = $nested_sections;
    }
}
array_walk($arResult['ITEMS'], 'walk_elements');
$cursor = CIBlockElement::GetList(
    [],
    [
        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
        "?NAME" => "%dummy%"
    ],
    false,
    false,
    [
        "IBLOCK_ID",
        "ID",
        "IBLOCK_SECTION_ID"
    ]
);
$dummies = [];
while($buf = $cursor->GetNextElement()) {
    /*$buf['PROPERTIES_DOC_FILE_GALLERY_VALUE'] = array_map(function ($e) {
        $file = CFile::GetFileArray($e);
        if ($file) {
            $file['FILE_SIZE'] = CFile::FormatSize($file['FILE_SIZE']);
            return $file;
        } else {
            return false;
        }
    }, $buf['PROPERTIES']['DOC_FILE_GALLERY']['VALUE'];*/
    $fields = $buf->GetFields();
    $docs = $buf->GetProperty(7)['VALUE'];
    $docs = array_map(
        function ($e) {
            $file = CFile::GetFileArray($e);
            if ($file) {
                $file['FILE_SIZE'] = CFile::FormatSize($file['FILE_SIZE']);
                return $file;
            } else {
                return false;
            }
        },
        $docs
    );
    $dummies[] = array_merge($fields, ["DOCS" => $docs]);
}
$cursor = CIBlockSection::GetList(
    [],
    [
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        "!UF_DOCS" => false,
        "!UF_SHARED_DOCS" => false
    ],
    false,
    [
        'IBLOCK_ID',
        'ID',
        'IBLOCK_SECTION_ID',
        'NAME'
    ]
);
$arSections = [];
while($section = $cursor->Fetch()) {
    $elements = array_filter(
        $arResult['ITEMS'],
        function ($i) use($section) {
            return $i['IBLOCK_SECTION_ID'] == $section['ID'];
        }
    );
    $dummy = array_filter(
        $dummies,
        function ($d) use($section) {
            return $d['IBLOCK_SECTION_ID'] == $section['ID'];
        }
    );
    if (!empty($dummy)) {
        $section['DOCS'] = array_filter(
            $dummy[array_keys($dummy)[0]]['DOCS'],
            function ($e) {
                return $e;
            }
        );
    }
    $arSections[] = array_merge($section, ['ELEMENTS' => $elements]);
}
$root_sections = array_filter(
    $arSections,
    function ($e) {
        return !$e['IBLOCK_SECTION_ID'];
    }
);
array_walk($root_sections,'walk_sections', $arSections);
$arResult['SECTIONS'] = $root_sections;