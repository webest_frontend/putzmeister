BX(function () {
   BX.bindDelegate(BX('documentation_tree'), 'click',{class: 'documentation__switch'}, BX.proxy(function (e) {
       var children;
       e.target.classList.toggle('expanded');
       if(e.target.classList.contains("expanded")) {
           if (children = BX.findChildren(e.target, {class: 'expanded'})) {
               children.forEach(e => e.classList.remove('expanded'));
           }
       }
   }));
   var opened_node = BX.findChild(BX('documentation_tree'), {class: 'expanded'},true);
   if(opened_node) expand_parents(opened_node);

   function expand_parents(node) {
       var parent_node;
       if (parent_node = BX.findParent(node, {class: 'documentation__switch'})) {
           parent_node.classList.add('expanded');
           expand_parents(parent_node);
       }
   }
});