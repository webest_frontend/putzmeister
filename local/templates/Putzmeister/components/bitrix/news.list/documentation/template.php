<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
global $model_id;
$model_id = 0;
if($request->getQuery('id')) $model_id = $request->getQuery('id');
function recurse ($section, $iter=1, $isElement = false) {
    global $model_id;
    if($iter > 5) die();?>
    <div class="documentation__switch <?if($isElement && $model_id == $section['ID']) echo 'expanded';?>" data-id="<?=$section['ID']?>">
        <?=$section['NAME'];?>
        <div class="documentation__content">
            <? $arArticles = [];
                if (isset($section['DOCS'])) {
                    $arArticles = array_merge($arArticles, $section['DOCS']);
                }
                if (isset($section['PROPERTIES'])) {
                    $arArticles = array_merge($arArticles, $section['PROPERTIES']['DOC_FILE_GALLERY']['VALUE']);
                }
                if (!empty($arArticles)) {
                    foreach ($arArticles as $article) { ?>
                        <div class="documentation__pdf_link cover-link">
                            <div class="pdf_link__title">
                                <?=$article['DESCRIPTION']?>
                            </div>
                            <div class="pdf_link__size">
                                <?=$article['FILE_SIZE']?>
                            </div>
                            <div class="pdf_link__download">
                                скачать
                            </div>
                            <a href="<?=$article['SRC']?>" target="_blank"></a>
                        </div>
                    <? }
                }
                if (isset($section['SUBSECTIONS']) && !empty($section['SUBSECTIONS'])) {
                    foreach ($section['SUBSECTIONS'] as $subsection) {
                        recurse($subsection, $iter+1);
                    }
                } elseif (!empty($section['ELEMENTS'])) {
                    foreach ($section['ELEMENTS'] as $element) {
                        recurse($element, $iter+1, true);
                    }
                } ?>
        </div>
    </div>
<? } ?>
<div id="documentation_tree" class="documentation__container">
    <? foreach ($arResult["SECTIONS"] as $arSection) {
        recurse($arSection, 1);
    } ?>
</div>
