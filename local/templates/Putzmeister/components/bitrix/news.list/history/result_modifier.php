<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
function walk (&$v) {
    $v['PROPERTIES']['GALLERY']['VALUE'] = array_map(
        function ($i) {
            $file = CFile::GetPath($i);
            return $file;
        },
        $v['PROPERTIES']['GALLERY']['VALUE']
    );
}
array_walk($arResult['ITEMS'], 'walk');