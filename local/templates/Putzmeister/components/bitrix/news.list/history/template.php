<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);?>
<div class="w-history">
    <ul class="nav nav-tabs">
        <? $iter = 1;
        foreach ($arResult['ITEMS'] as $arItem) { ?>
            <li class="nav-item">
                <a class="nav-link<? if ($iter == 1) echo ' active'; ?>" href="#data-<?= $iter ?>" data-toggle="tab">
                    <div class="point border-r"><span class="border-r"></span></div>
                    <?= $arItem['NAME'] ?>
                </a>
            </li>
            <? $iter++;
        } ?>
    </ul>
    <div class="tab-content">
        <? $iter = 1;
        foreach ($arResult['ITEMS'] as $arItem) { ?>
            <div id="data-<?= $iter ?>" class="tabs tab-pane<? if ($iter == 1) echo ' active'; ?>">
                <div class="history-events">
                    <h6 class="caption"><?= $arItem['NAME'] ?></h6>
                    <?= $arItem['PREVIEW_TEXT'] ?>
                </div>
                <div class="photo-b">
                    <? if (!empty($arItem['PROPERTIES']['GALLERY']['VALUE'])) {
                        foreach ($arItem['PROPERTIES']['GALLERY']['VALUE'] as $image) { ?>
                            <img src="<?= $image ?>">
                        <? }
                    } ?>
                </div>
            </div>
            <? $iter++;
        } ?>
    </div>
</div>
