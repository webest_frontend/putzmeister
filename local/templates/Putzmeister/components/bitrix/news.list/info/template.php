<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);?>
<div class="useful__tiles">
    <?foreach ($arResult["ITEMS"] as $arItem) { ?>
        <div>
            <div class="useful__tile">
                <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>">
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                    <?= $arItem['NAME'] ?>
                </a>
            </div>
        </div>
    <? } ?>
</div>
