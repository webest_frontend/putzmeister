<?
function walk_elements(&$i)
{
    $file = CFile::GetFileArray($i['PROPERTIES']['FILE']['VALUE']);
    $file['FILE_SIZE'] = CFile::FormatSize($file['FILE_SIZE']);
    $i['PROPERTIES']['FILE']['VALUE'] = $file;
}

array_walk($arResult['ITEMS'], 'walk_elements');
$cursor = \Bitrix\Iblock\SectionTable::GetList([
    'filter' => ['IBLOCK_ID' => $arParams['IBLOCK_ID']],
    'select' => ['IBLOCK_ID', 'ID', 'NAME']
]);
while ($buf = $cursor->Fetch()) {
    $elements = array_filter(
        $arResult['ITEMS'],
        function ($i) use ($buf) {
            return $i['IBLOCK_SECTION_ID'] == $buf['ID'];
        }
    );
    $buf['ELEMENTS'] = $elements;
    $arResult['SECTIONS'][] = $buf;
}
?>
<?/*<script>
    window.dumpJournal = <?=CUtil::PhpToJSObject($arResult['SECTIONS']);?>;
</script>*/?>