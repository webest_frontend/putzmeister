<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
CJSCore::Init('tabs'); ?>
<ul class="nav nav-tabs pm-post__tabs">
    <? $first = true;
    foreach ($arResult['SECTIONS'] as $section) { ?>
        <li class="nav-item pm-post__tab">
            <a
                    class="nav-link<? if ($first) echo ' active'; ?>"
                    href="#post-<?= $section['NAME'] ?>"
                    data-toggle="tab"><?= $section['NAME'] ?></a>
        </li>
    <?
        $first = false;
    } ?>
</ul>
<div class="tab-content pm-post__panes">
    <? $first = true;
    foreach ($arResult['SECTIONS'] as $section) { ?>
        <div
                class="tab-pane<?=$first ? ' active' : ' fade';?> pm-post__pane"
                id="post-<?= $section['NAME'] ?>">
            <? foreach ($section['ELEMENTS'] as $post) { ?>
                <div class="pm-post__issue">
                    <img src="<?= $post['PREVIEW_PICTURE']['SRC'] ?>" alt="">
                    <div><?= $post['NAME'] ?></div>
                    <div class="pm-post__link">
                        Скачать <?= $post['PROPERTIES']['FILE']['VALUE']['FILE_SIZE'] ?>
                    </div>
                    <a class="pm-post__cover" href="<?= $post['PROPERTIES']['FILE']['VALUE']['SRC'] ?>"></a>
                </div>
            <? } ?>
        </div>
    <? $first = false;
    } ?>
</div>
