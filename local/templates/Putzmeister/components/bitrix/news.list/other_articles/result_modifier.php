<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
function walk (&$v) {
    if(isset($v['PROPERTIES']['GALLERY'])) {
        $v['PROPERTIES']['GALLERY']['VALUE'] = array_map(
            function ($i) {
                $file = CFile::GetPath($i);
                return $file;
            },
            $v['PROPERTIES']['GALLERY']['VALUE']
        );
    }
    if(!$v['PREVIEW_TEXT']) {
        $v['PREVIEW_TEXT'] = substr(strip_tags($v['DETAIL_TEXT']), 0, 160).'...';
    }
}
array_walk($arResult['ITEMS'], 'walk');