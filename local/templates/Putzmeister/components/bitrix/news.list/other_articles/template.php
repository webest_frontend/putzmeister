<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true); ?>
<h2>Другие <?=$arResult['ELEMENTS_NAME']?>:</h2>
<ul class="other_articles">
    <? foreach ($arResult['ITEMS'] as $item) { ?>
        <li>
            <a href="<?=$item['DETAIL_PAGE_URL']?>"><?=$item['NAME']?></a>
        </li>
    <? } ?>
</ul>