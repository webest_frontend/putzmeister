<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true); ?>
<h2>Другие <?=$arResult['ELEMENTS_NAME']?>:</h2>
<div class="other_news">
    <? foreach ($arResult['ITEMS'] as $item) { ?>
        <div class="post">
            <div class="post__date"><?= $item['DISPLAY_ACTIVE_FROM'] ?></div>
            <div class="post__anons"><?= $item['PREVIEW_TEXT'] ?></div>
            <a class="post__details" href="<?= $item['DETAIL_PAGE_URL'] ?>">
                Читать дальше »
            </a>
        </div>
    <? } ?>
</div>