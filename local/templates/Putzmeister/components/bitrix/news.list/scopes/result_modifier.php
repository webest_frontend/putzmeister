<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

function parentSections($s) {
    global $arSections;
    if($s['IBLOCK_SECTION_ID']) {
        $arSections[$s['IBLOCK_SECTION_ID']]['SUBSECTIONS'][$s['ID']] = $s;
        parentSections($arSections[$s['IBLOCK_SECTION_ID']]);
    }
}

function assignArticles (&$i, $key, $collection) {
    $scopes = array_map(
        function ($e) use ($collection) {
            return [
                'name' => $collection['scopes'][$e]['NAME'],
                'url' => $collection['scopes'][$e]['DETAIL_PAGE_URL']
            ];
        },
        $i['PROPERTIES']['SCOPES']['VALUE']
    );
    $news = array_map(
        function ($e) use ($collection) {
            return [
                'name' => $collection['news'][$e]['NAME'],
                'url' => $collection['news'][$e]['DETAIL_PAGE_URL']
            ];
        },
        $i['PROPERTIES']['RELATED_ARTICLES']['VALUE']
    );
    if(!$scopes) $scopes = [];
    if(!$news) $news = [];
    $i['ARTICLES'] = array_merge($scopes, $news);
}

$scopes_iblock_id = $arResult['ITEMS'][0]['PROPERTIES']['SCOPES']['LINK_IBLOCK_ID'];
$cursor = \Bitrix\Iblock\ElementTable::getList([
    'filter' => [
        'IBLOCK_ID' => $scopes_iblock_id
    ],
    'select' => ['IBLOCK_ID', 'ID', 'CODE', 'NAME', 'DETAIL_PAGE_URL' => 'IBLOCK.DETAIL_PAGE_URL']
]);

$arArticles = ['scopes' => [], 'news' => []];

while($buf = $cursor->Fetch()) {
    $buf['DETAIL_PAGE_URL'] = CIBlock::ReplaceDetailUrl($buf['DETAIL_PAGE_URL'], $buf, false, 'E');
    $arArticles['scopes'][$buf['ID']] = $buf;
}

$news_iblock_id = $arResult['ITEMS'][0]['PROPERTIES']['RELATED_ARTICLES']['LINK_IBLOCK_ID'];

$cursor = \Bitrix\Iblock\ElementTable::getList([
    'filter' => [
        'IBLOCK_ID' => $news_iblock_id
    ],
    'select' => ['IBLOCK_ID', 'ID', 'CODE', 'NAME', 'DETAIL_PAGE_URL' => 'IBLOCK.DETAIL_PAGE_URL']
]);

while($buf = $cursor->Fetch()) {
    $buf['DETAIL_PAGE_URL'] = CIBlock::ReplaceDetailUrl($buf['DETAIL_PAGE_URL'], $buf, false, 'E');
    $arArticles['news'][$buf['ID']] = $buf;
}

array_walk($arResult['ITEMS'], 'assignArticles', $arArticles);

$cursor = \Bitrix\Iblock\SectionTable::getList([
    'filter' => [
        'IBLOCK_ID' => $arParams['IBLOCK_ID']
    ],
    'select' => ['IBLOCK', 'ID', 'NAME', 'IBLOCK_SECTION_ID']
]);

global $arSections;
$arSections = [];

while($section = $cursor->Fetch()) {
    $arSections[$section['ID']] = $section;
}

foreach ($arResult['ITEMS'] as $item) {
    $arSections[$item['IBLOCK_SECTION_ID']]['ELEMENTS'][$item['ID']] = $item;
    parentSections($arSections[$item['IBLOCK_SECTION_ID']]);
}

$root_sections = array_filter($arSections, function ($e) {
    return !$e['IBLOCK_SECTION_ID'];
});

$arResult['SECTIONS'] = $root_sections;