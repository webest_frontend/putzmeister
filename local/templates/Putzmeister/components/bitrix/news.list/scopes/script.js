BX(function () {
    var opened_node;
    BX.bindDelegate(BX('scopes_tree'), 'click',{class: 'scopes__switch'}, BX.proxy(function (e) {
        e.target.classList.toggle('expanded');
    }));
   if(opened_node = BX.findChild(BX('scopes_tree'), {class: 'expanded'},true)) recurse(opened_node);

   function recurse(node) {
       var parent_node;
       if (parent_node = BX.findParent(node, {class: 'scopes__switch'})) {
           parent_node.classList.add('expanded');
           recurse(parent_node);
       }
   }
});