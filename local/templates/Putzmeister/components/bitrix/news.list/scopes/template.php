<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
global $model_id;
$model_id = 0;
if($request->getQuery('id')) $model_id = $request->getQuery('id');
function recurse ($section, $iter=1, $isElement = false) {
    global $model_id;
    if($iter > 5) die();?>
    <div class="scopes__switch <?if($isElement && $model_id == $section['ID']) echo 'expanded';?>" data-id="<?=$section['ID']?>">
        <?=$section['NAME'];?>
        <div class="scopes__content">
            <? if ($section['ARTICLES']) { ?>
                <ul class="scopes__links">
                    <? foreach ($section['ARTICLES'] as $article) { ?>
                        <li>
                            <a href="<?=$article['url']?>">
                                <?=$article['name']?>
                            </a>
                        </li>
                    <? } ?>
                </ul>
                <? } elseif ($section['SUBSECTIONS']) {
                    foreach ($section['SUBSECTIONS'] as $subsection) {
                        recurse($subsection, $iter+1);
                    }
                } elseif ($section['ELEMENTS']) {
                    foreach ($section['ELEMENTS'] as $element) {
                        recurse($element, $iter+1, true);
                    }
                } ?>
        </div>
    </div>
<? } ?>
<div id="scopes_tree" class="scopes__container">
    <? foreach ($arResult["SECTIONS"] as $arSection) {
        if ($arSection['SUBSECTIONS'] || $section['ELEMENTS']) {
            recurse($arSection, 1);
        }
    } ?>
</div>