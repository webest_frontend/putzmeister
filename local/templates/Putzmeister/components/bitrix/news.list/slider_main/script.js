BX.ready(function(){
    $(".useful__slider").owlCarousel({
        navigation: false, // Show next and prev buttons
        autoplay: true,
        autoplayTimeout: 3000,
        loop: true,
        singleItem: true,
        // "singleItem:true" is a shortcut for:
        items: 1,
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: false,
        itemsMobile: false
    });
});