<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
CJSCore::Init(array("owl_carousel"));?>
<div class="useful__slider owl-carousel owl-theme">
    <? foreach ($arResult["ITEMS"] as $idx => $arItem) {
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div
                id="<?=$this->GetEditAreaId($arItem['ID']);?>"
                class="useful__slide<?if($arItem['PROPERTIES']['DARK_THEME']['VALUE'] == 'Да') echo " dark"?>">
            <img
                    class="slide__picture<?if(isset($arItem['PROPERTIES']['POSITION']) && $arItem['PROPERTIES']['POSITION']['VALUE'] == 'right') echo " right-align"?>"
                    src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"
                    alt="Техника Putzmeister - фото <?=$idx?>">
            <h6 class="slide__title"><?= $arItem['NAME'];?></h6>
            <p class="slide__text">
                <?= $arItem['PREVIEW_TEXT']; ?>
            </p>
            <?if ($arItem['PROPERTIES']['LINK_TEXT']['~VALUE']) { ?>
                <a class="slide__link" href="<?=$arItem['PROPERTIES']['LINK_URL']['VALUE']?>">
                    <?=$arItem['PROPERTIES']['LINK_TEXT']['~VALUE']?>
                </a>
            <? } ?>
        </div>
    <? } ?>
</div>