<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->SetPageProperty("h1", $arResult['NAME']);
$APPLICATION->SetPageProperty("title", $arResult['NAME']." - Новости официального сайта Putzmeister");
$description_content = $arResult['PREVIEW_TEXT'] ? $arResult['PREVIEW_TEXT'] : $arResult['DETAIL_TEXT'];
$APPLICATION->SetPageProperty("description", mb_substr(trim(strip_tags($description_content)), 0, 159));
