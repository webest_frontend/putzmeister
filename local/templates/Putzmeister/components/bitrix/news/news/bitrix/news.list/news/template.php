<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);?>
<div class="news__tiles">
    <? $first = true;
    foreach ($arResult["ITEMS"] as $arItem) {
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>
        <div
                class="news__tile cover-link<? if($first) echo ' news__tile--top';?>"
                id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="news__tile_body">
                <div class="news__preview">
                    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                    <span class="news__date"><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
                </div>
                <? if($first) { ?>
                    <div class="tile__column">
                <? } ?>
                <div class="news__title"><?=$arItem['NAME']?></div>
                <div class="news__anons"><?=$arItem['PREVIEW_TEXT']?></div>
                <div class="news__details">Читать дальше »</div>
                <? if($first) { ?>
                    </div>
                <? } ?>
            </div>
            <a href="<?=$arItem['DETAIL_PAGE_URL']?>"></a>
        </div>
    <? $first = false;
    } ?>
</div>
<? if($arParams["DISPLAY_BOTTOM_PAGER"]) {
    echo $arResult["NAV_STRING"];
} ?>