<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
if($arResult['NUM_PRODUCTS'] > 0) { ?>
    <div class="bx-hdr-profile">
        <div class="bx-basket-block">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/icons/17.svg">
            <? if ($arResult['NUM_PRODUCTS']) { ?>
                <div class="bx-basket-quantity"><?= $arResult['NUM_PRODUCTS']; ?></div>
            <? } ?>
            <a href="<?= $arParams['PATH_TO_BASKET'] ?>"></a>
        </div>
    </div>
<? }