BX.namespace('BX.Sale');
BX.Sale.BasketComponent = function (config) {
	this.container = config.container;
	this.total_sum = BX.findChild(this.container, {class: "order__total_amount"}, true);
	this.currentItem = {};
	this.signedParams = config.signedParamsString;
	this.siteId = config.siteId;
	this.template = config.template;
	this.ajaxConfig = {
		url: config.ajaxUrl,
		method: 'post',
		dataType: 'json',
		data: {
			fullRecalculation: "Y",
			via_ajax: 'Y',
			signedParamsString: this.signedParams,
			action: "refreshAjax",
			site_id: config.siteId,
			sessid: BX.bitrix_sessid(),
			template: config.template
		},
		onsuccess: function() {}
	};
	this._init.call(this);
};
BX.Sale.BasketComponent.prototype = {
	_init: function () {
		BX.bind(this.container, "click", BX.proxy(this._onItemDelete, this));
		BX.ajax(Object.assign({}, this.ajaxConfig));
	},
	_onItemDelete: function (e) {
		var id;
		if (this.currentItem = BX.findParent(e.target, {attribute: "data-id"})) {
			id = this.currentItem.dataset.id;
			delete this.ajaxConfig.data.fullRecalculation;
			this.ajaxConfig.data.action = "recalculateAjax";
			this.ajaxConfig.data.lastAppliedDiscounts = "";
			this.ajaxConfig.data.basket = {
				["DELETE_" + id]: "Y"
			};
			this.ajaxConfig.onsuccess = BX.proxy(this._onSuccess, this);
			BX.ajax(Object.assign({}, this.ajaxConfig));
		}
	},
	_onSuccess: function (json) {
		if(!json["BASKET_DATA"]["BASKET_ITEMS_COUNT"]) {
			location.reload();
			return;
		}
		!!this.currentItem && BX.remove(this.currentItem);
		BX.onCustomEvent(
			'onDeleteBasketItem',
			[json["BASKET_DATA"]["allSum_FORMATED"]]
		);
		this._recalculateBasket.call(this);
	},
	_recalculateBasket: function () {
		delete this.ajaxConfig.data.basket;
		this.ajaxConfig.data.fullRecalculation = "N";
		this.ajaxConfig.data.action = "refreshAjax";
		this.ajaxConfig.onsuccess = BX.proxy(function (json) {
			BX.adjust(this.total_sum,{text: json["BASKET_DATA"]["allSum_FORMATED"]});
		}, this);
		BX.ajax(Object.assign({}, this.ajaxConfig));
	}
};