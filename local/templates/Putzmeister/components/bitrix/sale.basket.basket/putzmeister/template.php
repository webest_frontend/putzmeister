<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */
$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedTemplate = $signer->sign($templateName, 'sale.basket.basket');
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.basket.basket');
if (!$arResult['EMPTY_BASKET']) { ?>
    <div class="order__basket_container" id="basket_container">
        <div class="order__basket">
            <? foreach ($arResult["GRID"]["ROWS"] as $id => $row) { ?>
                <div class="order__basket_item" data-id="<?=$id?>">
                    <div class="btn--close"></div>
                    <img src="<?=$row["PREVIEW_PICTURE_SRC"]?>" alt="">
                    <div class="order__basket_item_info">
                        <div class="order__basket_item_title"><?=$row["NAME"]?></div>
                        <div class="order__basket_item_price">
                            <span>Цена: </span><span class="order__basket_item_price_amount"><?=$row["SUM_FULL_PRICE_FORMATED"]?></span>
                        </div>
                    </div>
                </div>
            <? } ?>
        </div>
        <div class="order__total">
            <span>СУММА: </span><span class="order__total_amount"><?=$arResult["allSum_FORMATED"]?></span>
        </div>
    </div>
    <script>
        new BX.Sale.BasketComponent({
            container: BX('basket_container'),
            result: <?=CUtil::PhpToJSObject($arResult, false, false, true)?>,
            params: <?=CUtil::PhpToJSObject($arParams)?>,
            ajaxUrl: "<?=CUtil::JSEscape($component->getPath().'/ajax.php')?>",
            template: '<?=CUtil::JSEscape($signedTemplate)?>',
            signedParamsString: '<?=CUtil::JSEscape($signedParams)?>',
            siteId: '<?=CUtil::JSEscape($component->getSiteId())?>',
            templateFolder: '<?=CUtil::JSEscape($templateFolder)?>'
        });
    </script>
<? } ?>