<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$context = \Bitrix\Main\Application::getInstance()->getContext();
$request = $context->getRequest();
if(strlen($request->get('ORDER_ID')) > 0) {
    $APPLICATION->SetPageProperty("h1", "Ваш заказ сформирован и ожидает подтверждения");
}