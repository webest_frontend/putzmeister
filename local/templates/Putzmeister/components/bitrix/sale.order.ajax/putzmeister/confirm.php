<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var $APPLICATION CMain
 */
if (!empty($arResult["ORDER"])) {
    $order = CSaleOrderPropsValue::GetOrderProps($arResult["ORDER"]["ID"]);
    $status = \Bitrix\Sale\Internals\StatusLangTable::getList([
        "filter" => [
            "STATUS_ID" => $arResult["ORDER"]["STATUS_ID"]
        ]
    ])->Fetch();
    $result = [];
    while($buf = $order->Fetch()) {
        $result[] = $buf;
    }
    $arProps = array_map(function($p) {
        return [
            "NAME" => $p["NAME"],
            "VALUE" => $p["VALUE"]
        ];
    }, $result)?>
    <div class="order__confirm">
        <p>
            В ближайшее время с Вами свяжется наш менеджер для уточнения информации.
        </p>
        <dl>
            <dt>номер заказа</dt>
            <dd>№ <?=$arResult["ORDER"]["ID"]?> от <?=$arResult["ORDER"]["DATE_INSERT"]->toUserTime()->format('d.m.Y')?></dd>
            <dt>статус</dt>
            <dd>
                <span><?=$status["NAME"]?></span>
                <? if ($arResult["ORDER"]["CANCELED"] == "Y") { ?>
                    <span>Отменен</span>
                <? } else { ?>
                    <a class="link link--plain modal-trigger--cancel_order" href="#cancel_order_modal" data-id="<?= $arResult["ORDER"]["ID"] ?>">Отменить</a>
                <? } ?>
            </dd>
            <dt>информация о покупателе</dt>
            <dd>
                <ul>
                    <? foreach ($arProps as $prop) { ?>
                        <li>
                            <span><?=$prop["NAME"]?>: </span><span><?=$prop["VALUE"]?></span>
                        </li>
                    <? } ?>
                </ul>
            </dd>
            <dt>сумма</dt>
            <dd>
                <?=$arResult["ORDER"]["PRICE"]?> р.
            </dd>
        </dl>
        <a class="btn btn--yellow" href="/">Перейти на главную</a>
    </div>
<? }
$APPLICATION->IncludeComponent(
    "bitrix:main.feedback",
    "cancel_order",
    Array(
        "USE_CAPTCHA" => "Y",
        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
        "EMAIL_TO" => "my@email.com",
        "REQUIRED_FIELDS" => Array("NAME","EMAIL","MESSAGE"),
        "EVENT_MESSAGE_ID" => Array("5")
    )
);