BX(function () {
    BX.namespace('BX.Sale');

    BX.Sale.OrderAjaxComponent = function (config) {
        BX.Webest.CustomForm.call(this, config.form);
        this.total_sum = BX.findChild(this.form, {class: "order__total_amount"}, true);
        this.saveAllowed = true;
        this.result = config.result;
        this.params = config.params;
        this.phoneInputs = BX.findChildren(this.form, {class: 'phone-input'}, true);
        this.basket = document.getElementById("basket_container");
        this.ajaxConfig = {
            url: config.ajaxUrl,
            method: 'post',
            dataType: 'json',
            data: {
                via_ajax: 'Y',
                siteId: config.siteId,
                signedParamsString: config.params,
                sessid: BX.bitrix_sessid()
            }
        };
    };

    BX.extend(BX.Sale.OrderAjaxComponent, BX.Webest.CustomForm);

    BX.Sale.OrderAjaxComponent.prototype.init = function () {
        this.superclass.init.call(this, this.form);
        BX.addCustomEvent('onAjaxFailure', BX.proxy(this._onError, this));
        BX.addCustomEvent('onDeleteBasketItem', BX.proxy(this._recalculate, this));
    };

    BX.Sale.OrderAjaxComponent.prototype._initInputFields = function () {
        this.phoneInputs = BX.findChildren(this.form, {class: 'phone-input'}, true);
        this.textInputs = BX.findChildren(this.form, {class: "wrap__text"}, true);
        this.phoneInputs.forEach(i => {
            var assignedValue = i.value;
            var input = new BX.MaskedInput({
                mask: '+7 999 999 99 99',
                input: i,
                placeholder: '_'
            });
            input.setValue(assignedValue.replace(/^\d(.*)/, "$1"));
        });
        this.textInputs = this.textInputs.map(t => new BX.Webest.TextInput(t));
        this.region_autocomplete_list = this.form.querySelector("#suggest_region_list");
    };

    BX.Sale.OrderAjaxComponent.prototype._onChange = function (e) {
        var prepared, XHR, orderData;
        this.superclass._onChange.call(this, e);
        if (e.target.name === "PERSON_TYPE") {
            this.btn.disabled = true;
            document.body.classList.add("busy");
            BX.cleanNode(BX('field_props_block'), false);
            orderData = Object.assign({
                ["soa-action"]: 'refreshOrderAjax',
                order: {
                    ["soa-action"]: 'saveOrderAjax'
                }
            }, this.ajaxConfig.data);
            prepared = BX.ajax.prepareForm(this.form);
            for (let i in prepared.data) {
                orderData.order[i] = prepared.data[i];
            }
            XHR = BX.ajax(Object.assign({}, this.ajaxConfig, {
                data: orderData,
                onsuccess: (e) => {
                    this._renderFields.call(this, e);
                    this._initInputFields.call(this);
                }
            }));
            XHR.onloadend = BX.proxy(this._onComplete, this);
        } else if (e.target.name === "consent") {
            this.saveAllowed = e.target.checked;
        }
    };

    BX.Sale.OrderAjaxComponent.prototype._onSubmit = function (e) {
        var XHR, prepared, orderData;
        e.preventDefault();
        if (!this.saveAllowed) return;
        this.btn.disabled = true;
        document.body.classList.add("busy");
        BX.removeClass(this.form, "invalid");
        prepared = BX.ajax.prepareForm(this.form);
        for (let i in prepared.data) {
            this.ajaxConfig.data[i] = prepared.data[i];
        }
        prepared = BX.ajax.prepareForm(this.form);
        orderData = Object.assign({
            ["soa-action"]: 'saveOrderAjax',
            action: "saveOrderAjax"
        }, this.ajaxConfig.data);
        for (let i in prepared.data) {
            orderData[i] = prepared.data[i];
        }
        XHR = BX.ajax(Object.assign({}, this.ajaxConfig, {
            data: orderData,
            onsuccess: BX.proxy(this._onSuccess, this)
        }));
        return false;
    };

    BX.Sale.OrderAjaxComponent.prototype._onError = function (e) {
        BX.addClass(this.form, "invalid");
        this._onComplete.call(this);
    };

    BX.Sale.OrderAjaxComponent.prototype._onSuccess = function (json) {
        if (!!json.order["ERROR"] && json.order["ERROR"].length) {
            var errorHTML = '';
            for (let errors in json.order["ERROR"]) {
                json.order["ERROR"][errors].forEach(err => {
                    errorHTML += "<div>" + err + "</div>";
                });
            }
            BX.html(this.form_errors, errorHTML);
            this._onError.call(this, {});
        } else if (json.order["REDIRECT_URL"]) {
            location.href = json.order["REDIRECT_URL"];
        }
    };

    BX.Sale.OrderAjaxComponent.prototype._renderFields = function (json) {
        var error_container, input_container, label_container, input_container_config, suggest_list, field_container, arProps, attrs, required;
        arProps = json.order["ORDER_PROP"]["properties"];
        for (let prop_id in arProps) {
            required = arProps[prop_id]["REQUIRED"] === "Y";
            input_class = "";
            if (arProps[prop_id]["IS_PHONE"] === "Y") {
                input_class = "phone-input";
            }
            error_container = BX.create("span", {props: {classList: "text__error"}});
            attrs = {maxlength: "50"};
            input_container_config = {
                className: input_class,
                type: "text",
                name: "ORDER_PROP_" + arProps[prop_id]["ID"],
                value: arProps[prop_id]["VALUE"][0],
                placeholder: (required ? arProps[prop_id]["NAME"] + "*" : arProps[prop_id]["NAME"])
            };
            if (required) input_container_config.required = true;
            if (arProps[prop_id]["TYPE"] === "LOCATION") {
                attrs.list = "suggest_region_list";
            }
            if (!!arProps[prop_id]["DESCRIPTION"]) {
                attrs.autocomplete = arProps[prop_id]["DESCRIPTION"];
            }
            if (arProps[prop_id]["PATTERN"]) input_container_config.pattern = arProps[prop_id]["PATTERN"];
            input_container = BX.create("input", {props: input_container_config, attrs: attrs, events: {blur: BX.proxy(this._onBlur, this)}});
            label_container = BX.create("label", {props: {classList: "wrap__text"}, children: [error_container, input_container]});
            if (arProps[prop_id]["TYPE"] === "LOCATION") {
                suggest_list = BX.create("datalist", {props: {id: "suggest_region_list"}});
                BX.append(suggest_list, label_container);
            }
            if (!!arProps[prop_id]["MAXLENGTH"]) {
                if (field_container) {
                    BX.append(label_container, field_container);
                } else {
                    field_container = BX.create("div", {props: {classList: "inputs_container"}, children: [label_container]});
                }
            } else {
                if (field_container) {
                    BX.append(field_container, BX('field_props_block'));
                    field_container = null;
                    BX.append(label_container, BX('field_props_block'));
                } else {
                    BX.append(label_container, BX('field_props_block'));
                }
            }
        }
    };

    BX.Sale.OrderAjaxComponent.prototype._recalculate = function (total) {
        BX.adjust(this.total_sum, {text: total});
    };
});