<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var CMain $APPLICATION
 * @var CUser $USER
 * @var SaleOrderAjax $component
 * @var string $templateFolder
 */

$context = Main\Application::getInstance()->getContext();
$request = $context->getRequest();

if (strlen($request->get('ORDER_ID')) > 0 && !empty($arResult["ORDER"])) {
	include(Main\Application::getDocumentRoot().$templateFolder.'/confirm.php');
} elseif ($arParams['DISABLE_BASKET_REDIRECT'] === 'Y' && $arResult['SHOW_EMPTY_BASKET']) {
    include(Main\Application::getDocumentRoot().$templateFolder.'/empty.php');
} else {
	$hideDelivery = empty($arResult['DELIVERY']);
    $signer = new Main\Security\Sign\Signer;
    $signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.order.ajax');?>
    <div class="order__page">
        <div class="order__main">
            <form class="order__form" action="<?= POST_FORM_ACTION_URI ?>" id="order-form">
                <?= bitrix_sessid_post(); ?>
                <input type="hidden" name="location_type" value="code">
                <input type="hidden" name="BUYER_STORE" value="<?= $arResult['BUYER_STORE'] ?>">
                <? foreach ($arResult['PAY_SYSTEM'] as $id => $item) { ?>
                    <input type="radio" name="PAY_SYSTEM_ID" value="<?= $item["PAY_SYSTEM_ID"]?>"<?if($item['CHECKED']) echo " checked";?> style="display: none">
                <? } ?>
                <? foreach ($arResult['DELIVERY'] as $id => $item) { ?>
                    <input type="radio" name="<?=$item["FIELD_NAME"]?>" value="<?=$item['ID']?>"<?if($item['CHECKED']) echo " checked";?> style="display: none">
                <? } ?>
                <div class="order__section">
                    <div class="order__section_header">
                        Выбор профиля
                    </div>
                    <div class="order__section_body">
                        <? foreach ($arResult['JS_DATA']["PERSON_TYPE"] as $data) { ?>
                            <label class="wrap__radio">
                                <input type="radio" name="PERSON_TYPE" value="<?=$data["ID"]?>" <?if($data['CHECKED']) echo " checked";?>>
                                <span class="checkmark"></span>
                                <?=$data["NAME"]?>
                            </label>
                        <? } ?>
                    </div>
                </div>
                <div class="order__section">
                    <div class="order__section_header">
                        Личные данные
                    </div>
                    <div class="order__section_body">
                        <div id="field_props_block">
                            <? $is_prev_input_was_half = false;
                            foreach ($arResult['JS_DATA']["ORDER_PROP"]["properties"] as $data) {
                                if($data["MAXLENGTH"]) {
                                    if (!$is_prev_input_was_half) {
                                        $is_prev_input_was_half = true; ?>
                                        <div class="inputs_container">
                                        <? }
                                } else {
                                    if($is_prev_input_was_half) {
                                        $is_prev_input_was_half = false; ?>
                                        </div>
                                    <? }
                                } ?>
                                    <label class="wrap__text">
                                        <span class="text__error"></span>
                                        <input
                                            <?if($data["IS_PHONE"] == "Y") echo "class='phone-input'";?>
                                            type="text"
                                            name="ORDER_PROP_<?=$data["ID"]?>"
                                            placeholder="<?=$data["REQUIRED"] == "Y" ? $data["NAME"].'*' : $data["NAME"];?>"
                                            <?if($data["REQUIRED"] == "Y") echo " required";?>
                                            <?if($data["PATTERN"]) echo "pattern='".$data["PATTERN"]."'";?>
                                            maxlength="50"
                                            <?if(!empty($data['VALUE'])) echo 'value="'.$data['VALUE'][0].'"'?>
                                            <?if($data["TYPE"] == "LOCATION") echo 'list="suggest_region_list"';?>
                                            <?if($data["DESCRIPTION"]) echo 'autocomplete="'.$data["DESCRIPTION"].'"';?>>
                                            <?if($data["TYPE"] == "LOCATION") { ?>
                                                <datalist id="suggest_region_list"></datalist>
                                            <? } ?>
                                    </label>
                            <? }
                            if($is_prev_input_was_half) { ?>
                                </div>
                            <? } ?>
                        </div>
                        <textarea
                                class="wrap__textarea"
                                name="ORDER_DESCRIPTION"
                                placeholder="Комментарий к заказу">
                        </textarea>
                    </div>
                </div>
                <div class="order__checkout" id="bx-soa-orderSave">
                    <div class="order__total">
                        <span>СУММА: </span><span class="order__total_amount"><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></span>
                    </div>
                    <div class="order__actions">
                        <?$APPLICATION->IncludeComponent(
                            'bitrix:main.userconsent.request',
                            'putzmeister',
                            array(
                                'ID' => $arParams['USER_CONSENT_ID'],
                                'IS_CHECKED' => $arParams['USER_CONSENT_IS_CHECKED'],
                                'IS_LOADED' => $arParams['USER_CONSENT_IS_LOADED'],
                                'AUTO_SAVE' => 'N',
                                'INPUT_NAME' => 'consent',
                                'SUBMIT_EVENT_NAME' => 'bx-soa-order-save',
                                'REPLACE' => array(
                                    'button_caption' => isset($arParams['~MESS_ORDER']) ? $arParams['~MESS_ORDER'] : $arParams['MESS_ORDER'],
                                    'fields' => $arResult['USER_CONSENT_PROPERTY_DATA']
                                )
                            )
                        )?>
                        <input class="btn" type="submit" value="Подтвердить заказ" disabled>
                    </div>
                </div>
                <div class="form__errors"></div>
            </form>
            <script>
               BX(function() {
                   new BX.Sale.OrderAjaxComponent({
                       form: BX('order-form'),
                       orderBlockId: 'bx-soa-order',
                       result: <?=CUtil::PhpToJSObject($arResult['JS_DATA'])?>,
                       siteId: '<?=CUtil::JSEscape($component->getSiteId())?>',
                       ajaxUrl: "<?=CUtil::JSEscape($component->getPath().'/ajax.php')?>",
                       params: "<?=CUtil::JSEscape($signedParams)?>"
                   }).init();
               });
            </script>
        </div>
        <div class="order__side">
            <?$APPLICATION->IncludeComponent(
                "bitrix:sale.basket.basket",
                "putzmeister",
                Array(
                    "ACTION_VARIABLE" => "action",
                    "AUTO_CALCULATION" => "Y",
                    "TEMPLATE_THEME" => "blue",
                    "COLUMNS_LIST" => array("NAME","DISCOUNT","WEIGHT","DELETE","DELAY","TYPE","PRICE","QUANTITY"),
                    "COMPONENT_TEMPLATE" => ".default",
                    "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                    "GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
                    "GIFTS_CONVERT_CURRENCY" => "Y",
                    "GIFTS_HIDE_BLOCK_TITLE" => "N",
                    "GIFTS_HIDE_NOT_AVAILABLE" => "N",
                    "GIFTS_MESS_BTN_BUY" => "Выбрать",
                    "GIFTS_MESS_BTN_DETAIL" => "Подробнее",
                    "GIFTS_PAGE_ELEMENT_COUNT" => "4",
                    "GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
                    "GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",
                    "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
                    "GIFTS_SHOW_IMAGE" => "Y",
                    "GIFTS_SHOW_NAME" => "Y",
                    "GIFTS_SHOW_OLD_PRICE" => "Y",
                    "GIFTS_TEXT_LABEL_GIFT" => "Подарок",
                    "GIFTS_PLACE" => "BOTTOM",
                    "HIDE_COUPON" => "N",
                    "OFFERS_PROPS" => array("SIZES_SHOES","SIZES_CLOTHES"),
                    "PATH_TO_ORDER" => "/personal/order.php",
                    "PRICE_VAT_SHOW_VALUE" => "N",
                    "QUANTITY_FLOAT" => "N",
                    "SET_TITLE" => "Y",
                    "USE_GIFTS" => "Y",
                    "USE_PREPAYMENT" => "N"
                )
            )?>
        </div>
    </div>
<? }