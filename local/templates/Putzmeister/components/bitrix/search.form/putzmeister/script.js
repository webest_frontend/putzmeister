BX(function() {
    var search_panel;
    BX.namespace('BX.Webest');
    BX.Webest.SearchPanel = function (container) {
        this.container = container;
        this.panel = this.container.querySelector('.search__form');
        this._init.call(this);
    };
    BX.Webest.SearchPanel.prototype = {
        _init: function () {
            BX.bind(document, 'click', BX.proxy(this._onClick, this));
            return this;
        },
        _onClick: function (e) {
            var component;
            if(e.target.closest('.search__icon')) {
                e.stopPropagation();
                this.container.classList.toggle('activated');
            } else if(!e.target.closest('.search__form')){
                this.container.classList.remove("activated");
            }
        }
    };
    if (search_panel = document.querySelector('.header__search')) {
        new BX.Webest.SearchPanel(search_panel);
    }
});