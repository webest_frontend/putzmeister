<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="header__search">
    <svg class="search__icon trigger" data-target="search_form">
        <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#magnific_glass"></use>
    </svg>
    <form class="search__form" id="search_form" action="<?=$arResult["FORM_ACTION"]?>">
        <input type="text" name="q" value="" placeholder="Введите запрос">
        <input name="s" type="submit" value="" style="display: none">
    </form>
</div>