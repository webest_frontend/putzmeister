<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<div class="page-nav news">
    <a class="prev <?if($arResult['NavPageNomer'] == 1) echo 'disable'?>" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a>
    <a class="next <?if($arResult['NavPageNomer'] == $arResult['NavPageCount']) echo 'disable'?>" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"></a>
    <div class="count-page">
        <?=$arResult['NavPageNomer']?><span>/<?=$arResult['NavRecordCount']?></span>
    </div>
</div>
<? if ($arResult['NavShowAll']) { ?>
    <a href="/info/news/?all=1" class="fnews"><span>Все <?=$arResult['NavTitle']?></span> »</a>
<? }