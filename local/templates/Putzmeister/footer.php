<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<footer class="footer">
    <div class="wrap-container">
        <div class="footer__top">
            <div class="footer__contacts">
                <a class="footer__logo" href="/">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/icons/logo-white.svg">
                </a>
                <div class="footer__address">
                    <span>ООО «Путцмайстер-Рус» 129343, Россия, Москва, ул. Уржумская, 4, стр. 31</span>
                    <br>
                    <a href="/contacts">Схема проезда</a>
                </div>
                <dl>
                    <dt class="footer__phones">Телефон:</dt>
                    <dd>
                        <a href="tel:+78007071958">
                            8 (800) 707-19-58
                        </a>
                    </dd>
                    <dd>
                        <a href="tel:+74957752237">
                            +7 (495) 775-22-37
                        </a>
                    </dd>
                </dl>
                <div class="footer__email">
                    <a href="mailto:info@putzmeister.ru">info@putzmeister.ru</a>
                </div>
                <div class="footer__duty">
                    График работы: Офис, по будням с 8:00 до 18:00
                    <br>
                    Склад и Сервис с 8:00 до 17:00
                </div>
            </div>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "putz_bottom",
                array(
                    "ROOT_MENU_TYPE" => "bottom",
                    "MAX_LEVEL" => "2",
                    "CHILD_MENU_TYPE" => "footer",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(),
                    "COMPONENT_TEMPLATE" => "putz_bottom"
                ),
                false
            ); ?>
        </div>
        <div class="footer__bottom">
            <div class="bottom--left">
                Данный cайт и размещенная на нём информация являются собственностью компании. При использовании
                материалов сайта ссылка обязательна. Ваши вопросы, комментарии и замечания Вы можете направить по адресу
                <a href="mailto:info@putzmeister.ru">info@putzmeister.ru</a><br><br>
                2003-<?= date('Y'); ?> Putzmeister Concrete Pumps GmbH - бетононасосы, автобетоносмесители, штукатурные
                станции, бетонные заводы и другая бетонная техника Путцмайстер в Москве
                <br>
                <br>
                <a href="https://wbest.ru/?utm_source=site-client&utm_medium=banner&utm_campaign=wbest&utm_content=putzmeister">Оптимизация
                    и продвижение сайта – Webest</a>
                <iframe src="https://yandex.ru/sprav/widget/rating-badge/1016257607" width="150" height="50"
                        frameborder="0" style="display: block;margin-top: 20px;"></iframe>
            </div>
            <div class="bottom--right">
                <a href="/politika_obrabotki_personalnyh_dannyh/">Политика обработки персональных данных</a>
                <a href="/privacy-policy/">Соглашение об использовании сайта</a>
                <a href="/sitemap/">Карта сайта</a>
                <div class="footer__socials">
                    <div class="socials--fb cover-link">
                        <a href="https://www.facebook.com/PutzmeisterGroup/"></a>
                    </div>
                    <div class="socials--vk cover-link">
                        <a href="https://vk.com/putzmeisterrus"></a>
                    </div>
                    <div class="socials--yt cover-link">
                        <a href="https://www.youtube.com/channel/UCp61k8xK9avlchWLl2dY2qg"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<button
        class="btn btn--up"
        onclick="document.getElementById('top').scrollIntoView({behavior: 'smooth'})"></button>

<? if ($APPLICATION->GetCurPage() !="/service/order/") { ?>
    <div class="mfp-modal mfp-hide" id="tech_request_modal">
        <div class="modal__title">Запрос на технику</div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.feedback",
            "tech_request",
            Array(
                "USE_CAPTCHA" => "Y",
                "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                "EMAIL_TO" => "my@email.com",
                "REQUIRED_FIELDS" => Array("NAME", "EMAIL", "MESSAGE"),
                "EVENT_MESSAGE_ID" => Array("5"),
                "MODELS_IBLOCK_ID" => 2,
                "LEAD_FIELDS" => [
                    "MODEL" => "UF_CRM_1537191797205",
                    "REAL_LOCATION" => "UF_CRM_1541750114",
                    "COMPANY" => "UF_CRM_1539164225716"
                ],
                "ACTION_URL" => "https://putzmeister.bitrix24.ru/rest/11/b0o23a16sp3s5rb2/crm.lead.add.json/"
            )
        ); ?>
    </div>
<? } ?>

<? $APPLICATION->IncludeComponent(
    "bitrix:main.feedback",
    "callback_request",
    Array(
        "USE_CAPTCHA" => "Y",
        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
        "EMAIL_TO" => "my@email.com",
        "REQUIRED_FIELDS" => Array("NAME", "EMAIL", "MESSAGE"),
        "EVENT_MESSAGE_ID" => Array("5"),
        "LEAD_FIELDS" => [
            "MODEL" => "UF_CRM_1537191797205",
            "REAL_LOCATION" => "UF_CRM_1541750114",
            "COMPANY" => "UF_CRM_1539164225716"
        ],
        "ACTION_URL" => "https://putzmeister.bitrix24.ru/rest/11/b0o23a16sp3s5rb2/crm.lead.add.json/"
    )
); ?>
<? $APPLICATION->IncludeComponent(
    "bitrix:main.feedback",
    "login",
    Array(
        "USE_CAPTCHA" => "Y",
        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
        "EMAIL_TO" => "my@email.com",
        "REQUIRED_FIELDS" => Array("NAME", "EMAIL", "MESSAGE"),
        "EVENT_MESSAGE_ID" => Array("5")
    )
); ?>

<? $APPLICATION->IncludeComponent(
    "bitrix:main.feedback",
    "price_request",
    Array(
        "USE_CAPTCHA" => "Y",
        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
        "EMAIL_TO" => "my@email.com",
        "REQUIRED_FIELDS" => Array("NAME", "EMAIL", "MESSAGE"),
        "EVENT_MESSAGE_ID" => Array("5"),
        "LEAD_FIELDS" => [
            "MODEL" => "UF_CRM_1537191797205",
            "REAL_LOCATION" => "UF_CRM_1541750114",
            "COMPANY" => "UF_CRM_1539164225716"
        ],
        "ACTION_URL" => "https://putzmeister.bitrix24.ru/rest/11/b0o23a16sp3s5rb2/crm.lead.add.json/"
    )
); ?>

<div id="sumbit__info" class="mfp-modal mfp-hide">
    <div class="modal__title">Ваша заявка отправлена</div>
</div>
<div id="preloader">
    <div class="lds-ring">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<div id="tooltip"></div>
<script id="bx24_form_button" data-skip-moving="true">
    (function (w, d, u, b) {
        w['Bitrix24FormObject'] = b;
        w[b] = w[b] || function () {
            arguments[0].ref = u;
            (w[b].forms = w[b].forms || []).push(arguments[0])
        };
        if (w[b]['forms']) return;
        var s = d.createElement('script');
        s.async = 1;
        s.src = u + '?' + (1 * new Date());
        var h = d.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s, h);
    })(window, document, 'https://putzmeister.bitrix24.ru/bitrix/js/crm/form_loader.js', 'b24form');
    b24form({"id": "1", "lang": "ru", "sec": "8y19qp", "type": "button", "click": ""});
    b24form({"id": "10", "lang": "ru", "sec": "yw5ma4", "type": "button", "click": ""});
    b24form({"id": "12", "lang": "ru", "sec": "srjrfm", "type": "button", "click": ""});
    b24form({"id": "6", "lang": "ru", "sec": "z3afrb", "type": "button", "click": ""});
    b24form({"id": "8", "lang": "ru", "sec": "jxuff9", "type": "button", "click": ""});
    b24form({"id": "14", "lang": "ru", "sec": "ayf3oh", "type": "button", "click": ""});
    b24form({"id": "16", "lang": "ru", "sec": "4lj63t", "type": "button", "click": ""});
    b24form({"id": "22", "lang": "ru", "sec": "23f4vp", "type": "button", "click": ""});
    b24form({"id": "20", "lang": "ru", "sec": "67ub5g", "type": "button", "click": ""});
</script>
<!--Открытая линия:-->
<script>
    (function (w, d, u) {
        var s = d.createElement('script');
        s.async = true;
        s.src = u + '?' + (Date.now() / 60000 | 0);
        var h = d.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s, h);
    })(window, document, 'https://cdn.bitrix24.ru/b7792551/crm/site_button/loader_1_u3jsuh.js');
</script>
<!-- Yandex.Metrika counter -->
<? /*<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(23629588, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/23629588" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117941781-14"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117941781-14');
</script>*/ ?>
</body
</html>