<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Page\Asset;?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
    <head>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->ShowHead(true);
        $APPLICATION->ShowHeadStrings();
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/bx24.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/crm.css');
        Asset::getInstance()->addCss("https://fonts.googleapis.com/css?family=Open+Sans:400,700");
        Asset::getInstance()->addCss("/bitrix/css/main/font-awesome.min.css");
        Asset::getInstance()->addString("<link rel=\"shortcut icon\" href=\"".SITE_TEMPLATE_PATH."/favicon.ico\" type=\"image/x-icon\">");
        Asset::getInstance()->addString('<script type="application/ld+json"> 
            {
                "@context": "http://www.schema.org",
                "@type": "LocalBusiness",
                "name": "ООО «Путцмайстер-Рус»",
                "url": "https://www.putzmeister.ru",
                "logo": "https://www.putzmeister.ru/templates/putzmeister/images/logo.svg",
                "image": "https://www.putzmeister.ru/templates/putzmeister/images/logo.svg",
                "telephone" : "8 (800) 707-19-58",
                "description": "Компания Putzmeister осуществляет передовые разработки, производство, реализацию и техническое обслуживание широкого спектра современной бетонной техники с 1958 года. Широкая география охватывает Германию, Россию, Великобританию, Бразилию, США, Японию, Италию, Испанию, Францию и десятки других стран.",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "ул. Уржумская, 4, стр. 31",
                    "addressLocality": "г. Москва",
                    "addressRegion": "Московская область",
                    "postalCode": "129343",
                    "addressCountry": "Россия"
                },
                "aggregateRating" : {
                    "@type" : "AggregateRating",
                    "ratingValue" : "4.9",
                    "ratingCount" : "9612"
                },
                "geo": {
                    "@type": "GeoCoordinates",
                    "latitude": "55.849767",
                    "longitude": "37.666953"
                },
                "openingHours": "Mo, Tu, We, Th, Fr, 08:00-18:00",
                "contactPoint": {
                    "@type": "ContactPoint",
                    "telephone": "+7 (495) 775-22-37",
                    "email" : "info@putzmeister.ru",
                    "contactType": "customer service"
                }
            }
            </script>');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/common.js');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/form.js');
        Asset::getInstance()->addJs('https://api-maps.yandex.ru/2.1/?apikey=21fcbf9e-f597-45bb-ba61-6bcb21081e5e&lang=ru_RU');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/geolocation.js');
        $APPLICATION->SetPageProperty("og-image", SITE_TEMPLATE_PATH . "/images/logo.png");?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta property="og:site_name" content="Компания Пуцмайстер: Продажа бетонной техники и бетононасосов от производителя">
        <meta property="og:title" content="<? $APPLICATION->ShowProperty("title") ?>">
        <meta property="og:description" content="<? $APPLICATION->ShowProperty("description") ?>">
        <meta property="og:url" content="https://www.putzmeister.ru<?= $APPLICATION->GetCurPage(); ?>">
        <? $APPLICATION->ShowMeta("og-image", "og:image"); ?>
        <meta property="og:image:type" content="image/png">
        <meta property="og:locale" content="ru_RU">
        <meta property="og:type" content="website">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="Компания Пуцмайстер: Продажа бетонной техники и бетононасосов от производителя">
        <meta name="twitter:title" content="<? $APPLICATION->ShowTitle(false) ?>">
        <meta name="twitter:description" content="<? $APPLICATION->ShowProperty("description") ?>">
        <meta name="twitter:creator" content="«ПУЦМАЙСТЕР»">
        <meta name="twitter:image:src" content="<?= SITE_TEMPLATE_PATH ?>/images/logo.png">
        <meta name="twitter:domain" content="//www.putzmeister.ru/">
    </head>
    <body id="appBody">
        <? $APPLICATION->ShowPanel(); ?>
        <header id="top">
            <div class="mobile-hidden">
                <div class="header__top">
                    <div class="wrap-container">
                        <div class="header__top_row">
                            <div class="header__outer_links">
                                <div class="wrap__select">
                                    <img class="globe" src="/images/ru_map.png" alt="">
                                    <div class="select__selected">World links</div>
                                    <div class="select__list">
                                        <a href="https://www.putzmeister.com/">Putzmeister Holding</a>
                                    </div>
                                </div>
                                <div>link to: </div>
                                <a href="http://www.sanygroup.com/group/en-us/">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/sany.png" alt="SANY link">
                                </a>
                            </div>
                            <div class="header__contacts">
                                <div class="tablet-hidden">
                                    <div class="header__contacts_item header__address">г. Москва, ул. Уржумская, 4, стр. 31</div>
                                    <div class="header__contacts_item header__duty">ПН-ПТ: с 8:00 до 18:00</div>
                                    <button class="btn btn--yellow header__contacts_item header__consult modal_trigger--consult">Консультация</button>
                                </div>
                                <div class="header__phones">
                                    <a href="tel:+78007071958">
                                        8 (800) 707-19-58
                                    </a>
                                    <a href="tel:+74957752237">
                                        +7 (495) 775-22-37
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header__bottom">
                <div class="wrap-container">
                    <div class="header__bottom_row">
                        <a class="header__logo" href="/">
                            <svg class="logo--picture">
                                <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#logo"></use>
                            </svg>
                            <div class="mobile-hidden">
                                <svg class="logo--text">
                                    <use href="<?= SITE_TEMPLATE_PATH ?>/images/icons/sprite.svg#logo_brand"></use>
                                </svg>
                            </div>
                        </a>
                        <div class="tablet-hidden">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "putz_top",
                                array(
                                    "ROOT_MENU_TYPE" => "top",
                                    "MAX_LEVEL" => "2",
                                    "CHILD_MENU_TYPE" => "left",
                                    "USE_EXT" => "Y",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "Y",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "COMPONENT_TEMPLATE" => "putz_top"
                                ),
                                false
                            ); ?>
                        </div>
                        <div class="tablet-visible">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "mobile",
                                array(
                                    "ROOT_MENU_TYPE" => "top",
                                    "MAX_LEVEL" => "2",
                                    "CHILD_MENU_TYPE" => "left",
                                    "USE_EXT" => "Y",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "Y",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "COMPONENT_TEMPLATE" => "mobile"
                                ),
                                false
                            ); ?>
                        </div>
                        <div class="mobile-hidden">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:search.form",
                                "putzmeister",
                                Array(
                                    "USE_SUGGEST" => "Y",
                                    "PAGE" => "#SITE_DIR#search/index.php"
                                )
                            ); ?>
                        </div>
                        <div id="cart_inline">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:sale.basket.basket.line",
                                "putzmeister",
                                array(
                                    "AJAX" => "Y",
                                    "HIDE_ON_BASKET_PAGES" => "Y",
                                    "PATH_TO_BASKET" => SITE_DIR . "cart/",
                                    "PATH_TO_ORDER" => "",
                                    "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                                    "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                                    "PATH_TO_REGISTER" => SITE_DIR . "login/",
                                    "POSITION_FIXED" => "N",
                                    "POSITION_HORIZONTAL" => "right",
                                    "POSITION_VERTICAL" => "top",
                                    "SHOW_AUTHOR" => "N",
                                    "SHOW_DELAY" => "N",
                                    "SHOW_EMPTY_VALUES" => "Y",
                                    "SHOW_IMAGE" => "Y",
                                    "SHOW_NOTAVAIL" => "N",
                                    "SHOW_NUM_PRODUCTS" => "Y",
                                    "SHOW_PERSONAL_LINK" => "N",
                                    "SHOW_PRICE" => "Y",
                                    "SHOW_PRODUCTS" => "N",
                                    "SHOW_SUMMARY" => "Y",
                                    "SHOW_TOTAL_PRICE" => "N",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "PATH_TO_AUTHORIZE" => "",
                                    "SHOW_REGISTRATION" => "N",
                                    "MAX_IMAGE_SIZE" => "70"
                                ),
                                false
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <?if ($APPLICATION->GetCurPage() != '/') { ?>
            <section class="page__heading">
                <div class="wrap-container">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "breadcrumbs",
                        Array(
                            "START_FROM" => "0",
                            "PATH" => "",
                            "SITE_ID" => "s1"
                        )
                    ); ?>
                </div>
            </section>
        <? } ?>
