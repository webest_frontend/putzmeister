BX(function () {
    BX.namespace('Webest');
    BX.Webest.SectionComponent = function (container) {
        this.container = container;
        this.basketSmall = BX("cart_inline");
        this.ajaxConfig = {
            url: '/api/ajax.php?action=add2basket',
            method: 'post',
            headers: {
                ['Content-Type']: 'application/x-www-form-urlencoded'
            },
            processData: true,
            data: {
                sessid: BX.bitrix_sessid()
            },
            onsuccess: BX.proxy(this._onSuccess, this)
        };
        this.init.call(this);
    };
    BX.Webest.SectionComponent.prototype = {
        init: function () {
            BX.bindDelegate(
                this.container,
                'click',
                {
                    attribute: "data-product_id"
                },
                BX.proxy(this._sendRequest, this)
            );
        },
        _sendRequest: function (e) {
            var onsuccess, XHR;
            e.target.disabled = true;
            this.ajaxConfig.data.pid = e.target.dataset.product_id;
            onsuccess = function (btn, html) {
                btn.disabled = false;
                btn.innerText = 'Добавлено';
                setTimeout(BX.delegate(function() {
                    this.innerText = 'В корзину';
                }, btn), 1000);
                BX.html(this.basketSmall, html);
            };
            XHR = BX.ajax(Object.assign({}, this.ajaxConfig, {onsuccess: onsuccess.bind(this, e.target)}));
            XHR.onerror = BX.delegate(function (XHR) {
                this.innerText = XHR.xhr.responseText || 'ошибка';
            }, e.target);
        }
    };
    new BX.Webest.SectionComponent(BX('appBody'));
});