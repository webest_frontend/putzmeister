BX(function() {
    BX.namespace("BX.Webest");

    BX.Webest.App = function (container) {
        this.body = container;
        this.navbarOffcetHeight = 69;
        this.tooltip = document.querySelector('#tooltip');
        this.tooltipConfig = {
            top: -60,
            left: -40
        };
        this.scrolledDown = false;
        this._init.call(this);
    };

    BX.Webest.App.prototype = {
        _init: function () {
            BX.bind(this.body, 'click', BX.delegate(this._onClick, this));
            document.addEventListener('scroll', this._onScroll.bind(this));
            window.addEventListener('scroll', this._onScroll.bind(this));
            document.addEventListener('mouseover', this._onMouseOver.bind(this));
            document.addEventListener('mouseout', this._onMouseOut.bind(this));
            return this;
        },
        _onClick(e) {
            var exception, excluded, target, component, selected;
            exception = e.target.closest('.select__selected');
            if (exception && exception.parentElement.classList.contains('disabled')) return;
            document.body.querySelectorAll('.wrap__select').forEach(s => {
                excluded = exception && exception.parentElement;
                if(Object.is(s, excluded)) {
                    s.classList.toggle("expanded");
                } else {
                    s.classList.remove("expanded");
                }
            });
            if (target = e.target.closest('.list__item')) {
                component = e.target.closest('.wrap__select');
                selected = component.querySelector('.select__selected');
                selected.innerText = target.innerText.trim();
            }
        },
        _onScroll(e) {
            if (this.scrolledDown) {
                if (window.scrollY <= this.navbarOffcetHeight) {
                    document.body.classList.remove('sticked_header');
                    this.scrolledDown = false;
                }
            } else if (window.scrollY > this.navbarOffcetHeight) {
                document.body.classList.add('sticked_header');
                this.scrolledDown = true;
            }
        },
        _onMouseOver (e) {
            var target;
            if (target = e.target.closest("[title]")) {
                BX.adjust(
                    this.tooltip,
                    {
                        style: {
                            top: String(e.clientY + this.tooltipConfig.top) + "px",
                            left: String(e.clientX + this.tooltipConfig.left) + "px"
                        },
                        text: target.title,
                        props: {
                            className: "visible"
                        }
                    }
                );
            }
        },
        _onMouseOut (e) {
            var target;
            if (target = e.target.closest("[title]")) {
                BX.adjust(
                    this.tooltip,
                    {
                        props: {
                            className: ""
                        }
                    }
                );
            }
        }
    };

    new BX.Webest.App(BX("appBody"));
});

if(!!window.Worker) {
    window.suggestRegionWorker = new Worker("/local/templates/Putzmeister/js/worker.js");
}