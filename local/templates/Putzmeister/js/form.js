BX(function () {
    BX.namespace('BX.Webest.CustomForm');

    BX.Webest.CustomForm = function(form, fields=null) {
        this.form = form;
        this.btn = BX.findChild(this.form, {attribute: {type: 'submit'}}, true);
        this.captchaText = BX.findChild(this.form, {attribute: {class: 'mf-text'}}, true);
        this.form_errors = this.form.querySelector(".form__errors");
        this.textInputs = BX.findChildren(this.form, {class: "wrap__text"}, true);
        this.selectInputs = BX.findChildren(this.form, {class: "wrap__select"}, true);
        this.error_container = BX.findChild(this.form, {class: "form__error"}, true);
        this.phoneInputs = BX.findChildren(this.form, {attribute: {type: 'tel'}}, true);
        this.captcha = {
            input_sid: BX.findChild(this.form, {attribute: {name: 'captcha_sid'}}, true),
            input_word: BX.findChild(this.form, {attribute: {name: 'captcha_word'}}, true),
            picture: BX.findChild(this.form, {attribute: {alt: 'CAPTCHA'}}, true)
        };
        this.throttle = "";
        this.region_autocomplete_list = this.form.querySelector("#suggest_region_list");
        this.ajax_url = "/api/ajax.php";
        this.fieldsCodes = {
            title: "TITLE",
            realRegion: fields ? fields.realLocation : "",
            model: fields ? fields.model : "",
            trace: "TRACE"
        };
        this.modal_config = {
            closeMarkup: '<button class="mfp-close btn btn--close" type="button"></button>',
            mainClass: "mfp-fade",
            callbacks: {
                beforeOpen: function () {
                    this.st.postActions.beforeOpen && this.st.postActions.beforeOpen.call(this);
                    document.body.classList.add('force-scroll-block');
                },
                close: function () {
                    this.st.postActions.close && this.st.postActions.close.call(this);
                    document.body.classList.remove('force-scroll-block');
                    this.content[0].querySelectorAll('form').forEach(f => f.reset());
                }
            },
            postActions: {
                beforeOpen: () => {},
                close: () => {}
            }
        };
        this.submit_message_modal_config = Object.assign({}, this.modal_config, {
            items: {src: document.getElementById('sumbit__info')},
            modalTitle: "Спасибо. Ваша заявка отправлена",
            callbacks: {
                beforeOpen: function () {
                    var modal_title;
                    if (modal_title = this.items[0].src.querySelector('.modal__title')) {
                        modal_title.innerText = this.st.modalTitle;
                    }
                }
            }
        });
    };

    BX.Webest.CustomForm.prototype = {
        init: function () {
            BX.bind(this.form, 'submit', BX.proxy(this._onSubmit, this));
            BX.bind(this.form, 'reset', BX.proxy(this._onReset, this));
            BX.bind(this.form, 'change', BX.proxy(this._onChange, this));
            BX.bind(this.form, 'keyup', BX.proxy(this._onKeyUp, this));
            this.textInputs = this.textInputs.map(t => new BX.Webest.TextInput (t));
            this.selectInputs = this.selectInputs.map(i => new BX.Webest.SelectInput(i));
            this.phoneInputs.forEach(i => {
                var input = new BX.MaskedInput({
                    mask: '+7 999 999 99 99',
                    input: i,
                    placeholder: '_'
                });
                if (i.hasAttribute("value")) {
                    input.setValue(i.getAttribute("value").replace(/^\d(.*)/, "$1"));
                }
            });
        },
        _onReset: function(e) {
            this.textInputs.forEach(i => BX.removeClass(i, "invalid"));
            this.selectInputs.forEach(i => i.reset());
        },
        _onKeyUp (e) {
            clearTimeout(this.throttle);
            this.throttle = setTimeout(closure.bind(this, e), 200);

            function closure(e) {
                this.form.dispatchEvent(new Event('change', {bubbles: true}));
                if (this.region_autocomplete_list && window.suggestRegionWorker && e.target.hasAttribute("list")) {
                    window.suggestRegionWorker.onmessage = this._onMessage.bind(this);
                    window.suggestRegionWorker.postMessage(e.target.value);
                }
            }
        },
        _onMessage (message) {
            var option;
            this.region_autocomplete_list.innerHTML = "";
            message.data.forEach(e => {
                option = document.createElement("option");
                option.setAttribute("value", e.CITY_NAME);
                this.region_autocomplete_list.appendChild(option);
            });
        },
        _onChange: function (e) {
            if (e.target.name === "consent") {
                this.btn.disabled = !e.target.checked;
            }
            this.btn.disabled = !this.form.checkValidity();
        },
        _onSubmit: function (e) {
            var XHR, region, self;
            e.preventDefault();
            this.btn.disabled = true;
            document.body.classList.add('busy');
            this.form.classList.remove("error");
            try {
                this.captchaText.innerText = "Введите код с картинки";
            } catch (e) {}
            if(region = document.cookie
                .split(";")
                .map(e => {
                    var t = e.split("=");
                    return {[t[0].trim()]: t[1]};
                })
                .find(e => 'bx_region_name' in e)) {
                try {
                    this.form.elements["fields[" + this.fieldsCodes["realRegion"] + "]"].value = region["bx_region_name"];
                } catch (e) {}
            }
            try {
                this.form.elements["fields[" + this.fieldsCodes["trace"] + "]"].value = b24Tracker.guest.getTrace();
            } catch (e) {}
            this._validateRequest.call(this);
            return false;
        },
        _sendRequest: function() {
            var form_data = new FormData(this.form);
            form_data.delete("sessid");
            var XHR = new XMLHttpRequest();
            XHR.onload = this._onSuccess.bind(this);
            XHR.onerror = this._onError.bind(this);
            XHR.onloadend = this._onComplete.bind(this);
            XHR.responseType = 'json';
            XHR.open(this.form.method, this.form.action, true);
            XHR.send(form_data);
        },
        _validateRequest: function() {
            var self = this;
            var XHR = new XMLHttpRequest();
            XHR.onerror = e => {
                self._onError.call(self);
                self._onComplete.bind(self);
            }
            XHR.onload = e => {
                if(!e.target.response.error) {
                    self._sendRequest.call(self);
                } else {
                    e.target.response = e.target.response.error;
                    e.target.dispatchEvent(new Event("error"));
                }
            };
            XHR.onloadend = e => {
                try {
                    self.captcha.input_sid.value = e.target.response.captcha;
                    self.captcha.input_word.value = "";
                    self.captcha.picture.setAttribute('src', self.captcha.picture.getAttribute('src').replace(/(.*captcha_sid=)(.*)/, '$1' + e.target.response.captcha));
                } catch (err) {}
            };
            XHR.responseType = 'json';
            XHR.open("post", this.ajax_url, true);
            XHR.send(new FormData(this.form));
        },
        _onSuccess: function (e) {
            if(e.target.status < 400) {
                this.form.reset();
                $.magnificPopup.open(this.submit_message_modal_config);
                $.magnificPopup.instance.content[0].querySelector(".modal__title").innerText = this.submit_message_modal_config.modalTitle;
                $.magnificPopup.instance.st.callbacks.close = this.submit_message_modal_config.callbacks.close;
            } else {
                e.target.response = e.target.response.error;
                e.target.dispatchEvent(new Event("error"));
            }
        },
        _onError: function (e) {
            this.form_errors.innerText = e.target.response.error || e.target.responseText || 'ошибка';
            this.form.classList.add("error");
        },
        _onComplete: function (e) {
            document.body.classList.remove('busy');
            this.btn.disabled = false;
        }
    };
});