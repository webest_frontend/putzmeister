SilentGeoLocation = function() {
    this._init.call(this);
};
SilentGeoLocation.prototype = {
    set current(region) {
        var expires = new Date();
        expires.setYear(expires.getFullYear() + 1);
        document.cookie = `bx_region_name=${region};path=/;Expires=${expires}`;
    },
    _init: function () {
        window.addEventListener('DOMContentLoaded', this._isRegionDefined.bind(this));
    },
    _defineRegion: function (result) {
        var region_title;
        if(region_title = result
            .geoObjects
            .get(0)
            .properties
            .get('metaDataProperty')
            .GeocoderMetaData
            .AddressDetails
            .Country
            .AdministrativeArea
            .SubAdministrativeArea
            .Locality
            .LocalityName
        ) this.current = region_title;
    },
    _isRegionDefined: function () {
        var self = this;
        if (!document.cookie
            .split(";")
            .map(e => {
                t = e.split("=");
                return {[t[0].trim()]: t[1]};
            })
            .find(e => 'bx_region_name' in e)) {
            if(!!window.ymaps) {
                window.ymaps.ready(() => {
                    ymaps
                        .geolocation
                        .get()
                        .then(self._defineRegion.bind(self));
                });
            }
        }
    }
};
new SilentGeoLocation();