var geoData = [];
var XHR = new XMLHttpRequest();
XHR.onload = e => {
    geoData = e.target.response;
};
XHR.open("GET", "/api/geo.php");
XHR.responseType = "json";
XHR.send();
onmessage = function(query) {
    var needle = query.data.toLowerCase();
    var result = geoData.filter(g => {
        return g['CITY_NAME'] && g['CITY_NAME'].toLowerCase().startsWith(needle);
    });
    postMessage(result);
};