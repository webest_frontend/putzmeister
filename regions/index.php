<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Где купить | Putzmeister Россия - Официальный сайт");
$APPLICATION->SetPageProperty("description", "Весь список региональных дилеров Putzmeister в России.");
CJSCore::Init('ymaps');
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("highloadblock");
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
$highblock_id = 5;
$hl_block = HLBT::getById($highblock_id)->fetch();
$entity = HLBT::compileEntity($hl_block);
$entity_data_class = $entity->getDataClass();
$rs_data = $entity_data_class::getList(array(
    'select' => array('*')
));
$arRegions = [];
while ($el = $rs_data->fetch()){
    $arRegions[] = $el;
}
$cursor = CIBlockElement::GetList(
    [],
    ["IBLOCK_ID" => 17, 'ACTIVE' => "Y"],
    false,
    false,
    ['IBLOCK_ID', 'ID', 'NAME', 'PROPERTY_COORDINATES', 'PROPERTY_ADDRESS', 'PROPERTY_EMAIL', 'PROPERTY_LINK', 'PROPERTY_PHONE', 'PROPERTY_MAIN', 'PROPERTY_DEALER', 'PROPERTY__city']
);
$data = [];
while($buf = $cursor->Fetch()) {
    $coords = explode(',', $buf['PROPERTY_COORDINATES_VALUE']);
    $data[] = [
        'NAME' => $buf["NAME"],
        'ADDRESS' => $buf["PROPERTY_ADDRESS_VALUE"],
        'PHONE' => $buf["PROPERTY_PHONE_VALUE"],
        'EMAIL' => $buf["PROPERTY_EMAIL_VALUE"],
        'LINK' => $buf["PROPERTY_LINK_VALUE"],
        'COORDINATES' => $coords,
        'STATUS' => $buf["PROPERTY_DEALER_VALUE"] ? 'dealer' : ($buf["PROPERTY_MAIN_VALUE"] ? 'main' : 'partner'),
    ];
}?>
<section class="regions">
    <div class="wrap-container">
        <h1 class="regions__title">Где купить</h1>
        <form class="regions__selector" onchange="BX.Webest.regionsMap.setCenter(this.elements['region'].value.split(','));">
            <div class="wrap__select">
                <div class="select__selected">Выберите город...</div>
                <div class="select__list">
                    <?foreach ($arRegions as $region) { ?>
                        <label class="list__item">
                            <?=$region['UF_NAME']?>
                            <input type="radio" name="region" value="<?=$region['UF_COORDINATES']?>">
                        </label>
                    <? } ?>
                </div>
            </div>
        </form>
        <div id="map__regions"></div>
        <div class="regions__legend">
            <div>
                <img src="/images/lp-d.png"><span> - Дилеры</span>
            </div>
            <div>
                <img src="/images/lp-p.png"><span> - Партнеры</span>
            </div>
            <div>
                <img src="/images/lp-main.png"><span> - Главный офис</span>
            </div>
        </div>
        <script>
            BX.Webest.regionsMapConfig = {
                placemarks: <?=CUtil::PhpToJSObject($data);?>,
                zoom: 10,
                center: [55.749949, 37.619847],
                template: '<div class="info-lp">' +
                    '<ins>{{ properties.name }}</ins>' +
                    '<p style="margin-top:10px" class="address">{{ properties.address }}</p>' +
                    '<div class="phone"><p>{{ properties.phone }}</p></div>' +
                    '<a href="mailto:{{ properties.phone }}" class="mail">{{ properties.email }}</a>' +
                    '<p><a href="{{ properties.link }}" class="site">{{ properties.link }}</a></p>' +
                '</div>'
            };
            BX.Webest.regionsMap = new BX.Webest.YandexMaps(BX('map__regions'), BX.Webest.regionsMapConfig).init();
        </script>
    </div>
</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");