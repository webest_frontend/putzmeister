<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");?>
<section class="search_results">
    <div class="wrap-container">
        <h1>Результаты поиска</h1>
        <? $APPLICATION->IncludeComponent("bitrix:search.page", "putzmeister", Array(
	"RESTART" => "N",	// Искать без учета морфологии (при отсутствии результата поиска)
		"CHECK_DATES" => "Y",	// Искать только в активных по дате документах
		"arrWHERE" => array(
			0 => "iblock_catalog",
		),
		"arrFILTER" => array(	// Ограничение области поиска
			0 => "iblock_catalog",
		),
		"SHOW_WHERE" => "N",	// Показывать выпадающий список "Где искать"
		"PAGE_RESULT_COUNT" => "10",	// Количество результатов на странице
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"TAGS_SORT" => "NAME",	// Сортировка тегов
		"TAGS_PAGE_ELEMENTS" => "20",	// Количество тегов
		"TAGS_PERIOD" => "",	// Период выборки тегов (дней)
		"TAGS_URL_SEARCH" => "",	// Путь к странице поиска (от корня сайта)
		"TAGS_INHERIT" => "Y",	// Сужать область поиска
		"SHOW_RATING" => "N",	// Включить рейтинг
		"FONT_MAX" => "50",	// Максимальный размер шрифта (px)
		"FONT_MIN" => "10",	// Минимальный размер шрифта (px)
		"COLOR_NEW" => "000000",	// Цвет позднего тега (пример: "C0C0C0")
		"COLOR_OLD" => "C8C8C8",	// Цвет раннего тега (пример: "FEFEFE")
		"PERIOD_NEW_TAGS" => "",	// Период, в течение которого считать тег новым (дней)
		"SHOW_CHAIN" => "N",	// Показывать цепочку навигации
		"COLOR_TYPE" => "Y",	// Плавное изменение цвета
		"WIDTH" => "100%",	// Ширина облака тегов (пример: "100%" или "100px", "100pt", "100in")
		"PATH_TO_USER_PROFILE" => "#SITE_DIR#people/user/#USER_ID#/",	// Шаблон пути к профилю пользователя
		"COMPONENT_TEMPLATE" => "tags",
		"NO_WORD_LOGIC" => "N",	// Отключить обработку слов как логических операторов
		"USE_TITLE_RANK" => "N",	// При ранжировании результата учитывать заголовки
		"DEFAULT_SORT" => "rank",	// Сортировка по умолчанию
		"FILTER_NAME" => "",	// Дополнительный фильтр
		"arrFILTER_iblock_catalog" => array(	// Искать в информационных блоках типа "iblock_catalog"
			0 => "all",
		),
		"SHOW_WHEN" => "N",	// Показывать фильтр по датам
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"USE_LANGUAGE_GUESS" => "Y",	// Включить автоопределение раскладки клавиатуры
		"USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
		"RATING_TYPE" => "",	// Вид кнопок рейтинга
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над результатами
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под результатами
		"PAGER_TITLE" => "Результаты поиска",	// Название результатов поиска
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => "",	// Название шаблона
	),
	false
); ?>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");