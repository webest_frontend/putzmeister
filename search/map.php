<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Карта сайта");?>
<section class="sitemap">
    <div class="wrap-container">
        <?$APPLICATION->IncludeComponent(
        "bitrix:main.map",
        ".default",
            Array(
                "LEVEL"	=>	"3",
                "COL_NUM"	=>	"2",
                "SHOW_DESCRIPTION"	=>	"Y",
                "SET_TITLE"	=>	"Y",
                "CACHE_TIME"	=>	"3600"
            )
        );?>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>