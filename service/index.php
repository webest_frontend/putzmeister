<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Сервисное обслуживание Putzmeister | Putzmeister Россия - Официальный сайт");
$APPLICATION->SetPageProperty("description", "Мы всегда рады Вам помочь");?>
<section class="service">
    <div class="wrap-container">
        <h1>Сервисное обслуживание Putzmeister</h1>
        <div class="page-services">
            <p>Контакты по вопросам гарантийного и постгарантийного обслуживания техники:</p>
            <div class="service-contacts">
                <div class="service-contacts__item">
                    <div class="service-contacts__item-title"><img src="/images/service-phone.png" alt="" width="24"> Бетонная техника</div>
                    <div class="service-contacts__item-name">Настычук Андрей</div>
                    <div class="service-contacts__item-phone"><strong>+7 (495) 775 22 47</strong></div>
                    <div class="service-contacts__item-worktime">с 08:00 до 17:00 по московскому времени</div>
                    <div class="service-contacts__item-email"><a href="mailto:nastychuk@putzmeister.ru">nastychuk@putzmeister.ru</a></div>
                </div>
                <div class="service-contacts__item">
                    <div class="service-contacts__item-title"><img src="/images/service-phone.png" alt="" width="24"> Растворная техника</div>
                    <div class="service-contacts__item-name">Голосной Олег</div>
                    <div class="service-contacts__item-phone"><strong>+7 (495) 775 22 37</strong> Доб. номер: 403</div>
                    <div class="service-contacts__item-worktime">с 08:00 до 17:00 по московскому времени</div>
                    <div class="service-contacts__item-email"><a href="mailto:golosnoj@putzmeister.ru">golosnoj@putzmeister.ru</a></div>
                </div>
                <div class="service-contacts__item">
                    <div class="service-contacts__item-title"><img src="/images/service-phone.png" alt="" width="24"> Тоннельное и шахтное оборудование</div>
                    <div class="service-contacts__item-name">Красильников Владимир</div>
                    <div class="service-contacts__item-phone"><strong>+7 (495) 775 22 37</strong> Доб. номер: 314</div>
                    <div class="service-contacts__item-worktime">с 09:00 до 18:00 по московскому времени</div>
                    <div class="service-contacts__item-email"><a href="mailto:luguev@putzmeister.ru">luguev@putzmeister.ru</a></div>
                </div>
            </div>
            <p>Вы можете положиться как на нашу технику, так и сервис!</p>
            <h2>Услуги</h2>
            <div class="uslugi__tiles">
                <div>
                    <div class="uslugi__tile cover-link filled">
                        <img src="/images/services/kit-order.jpg" alt="">
                        <span>Заказ запчастей</span>
                        <a href="/service/spares/"></a>
                    </div>
                </div>
                <div>
                    <div class="uslugi__tile cover-link filled">
                        <img src="/images/services/tech-request.jpg" alt="">
                        <span>Ремонт</span>
                        <a href="/service/remont/"></a>
                    </div>
                </div>
                <div>
                    <div class="uslugi__tile cover-link filled">
                        <img src="/images/services/service.jpg" alt="">
                        <span>Запись на сервисное обслуживание и ремонт</span>
                        <a onclick="return false;" href="javascript:void(0);" class="modal_trigger--service" data-suffix="Cервисное обслуживание и ремонт" rel="nofollow"></a>
                    </div>
                </div>
                <div>
                    <div class="uslugi__tile cover-link filled">
                        <img src="/images/services/education.jpg" alt="">
                        <span>Обучение операторов и механиков</span>
                        <a onclick="return false;" href="javascript:void(0);" class="modal_trigger--service" data-suffix="Обучение операторов и механиков" rel="nofollow"></a>
                    </div>
                </div>
                <div>
                    <div class="uslugi__tile cover-link filled">
                        <img src="/images/services/weighter.jpg" alt="">
                        <span>Взвешивание автобетононасоса</span>
                        <a onclick="return false;" href="javascript:void(0);" class="modal_trigger--service" data-suffix="Взвешивание автобетононасоса" rel="nofollow"></a>
                    </div>
                </div>
                <div>
                    <div class="uslugi__tile cover-link filled">
                        <img src="/images/services/repair.jpg" alt="">
                        <span>Восстановление насосов бывших в эксплуатации</span>
                        <a onclick="return false;" href="javascript:void(0);" class="modal_trigger--service" data-suffix="Восстановление Б&nbsp;/&nbsp;У насосов" rel="nofollow"></a>
                    </div>
                </div>
                <div>
                    <div class="uslugi__tile cover-link filled" style="background-image: url('');">
                        <img src="/images/services/assessment.jpg" alt="">
                        <span>Оценка техники продажа / покупка</span>
                        <a onclick="return false;" href="javascript:void(0);" class="modal_trigger--service" data-suffix="Оценка техники продажа&nbsp;/&nbsp;покупка" rel="nofollow"></a>
                    </div>
                </div>
                <div>
                    <div class="uslugi__tile cover-link filled" style="background-image: url('');">
                        <img src="/images/services/authentication.jpg" alt="">
                        <span>Проверка подлинности и происхождения оборудования</span>
                        <a onclick="return false;" href="javascript:void(0);" class="modal_trigger--service" data-suffix="Проверка подлинности и происхождения оборудования" rel="nofollow"></a>
                    </div>
                </div>
            </div>
            <div class="container--half">
                <h2>Сервисное обслуживание и ремонт бетононасосов Putzmeister - After Sales and Parts (ASP) Putzmeister</h2>
                <p>
                    Насосы Putzmeister легко, выгодно и приятно эксплуатировать: они подходят для решения большинства строительных задач при бетонных работах, внутренней и внешней отделке сооружений. Могут эффективно работать в тяжелых условиях, их конструкция продумана и надежно защищена от преждевременного износа фирменными разработками немецких инженеров и патентами компании, в совокупности с высоким качеством материалов используемых в производстве.<br><br>
                    Однако, любое оборудование со временем изнашивается. Разработчики Putzmeister позаботились о том, чтобы максимальное число деталей оборудования можно было легко и недорого заменить прямо на строительной площадке, а также износ таких деталей происходил максимально равномерно и прогнозируемо.<br><br>
                    Российское представительство Putzmeister, в свою очередь, позаботилось о том, чтобы нужные запчасти для бетононасоса всегда были на складах в Москве и у партнеров. Кроме запчастей, мы всегда предложим большой ассортимент дополнительного оборудования, которое расширит возможности вашей техники и упростит повседневную работу.<br><br>
                    Вы можете купить запчасти необходимые Вам на наших складах и получить их в короткие сроки.
                    <br>
                    <br>
                    Ассортимент компании включает большое количество высококачественных оригинальных аксессуаров для любых областей применения бетонной, растворной техники, торкрет установок и другой техники Putzmeister.<br><br>
                </p>
                <h2>Персонал - опыт и квалификация</h2>
                <p>
                    Сервисные мастера с многолетним опытом работы по обслуживанию бетононасосной техники. Уникальные специалисты, совмещающие мультизадачность и проверенные временем навыки гидравлики, электрики и мехатроники. В штате фирмы электросварщик, сертифицированный на сварочные работы всех металлоконструкций бетононасосов.
                </p>
                <div class="image-container--row">
                    <img src="/images/Putzmeister-Support-Service-Slider2.jpg">
                    <img src="/images/Putzmeister-Support-Service-Slider3.jpg">
                </div>
                <h2>Какой инструмент, материалы, от каких производителей используем в работе над техникой</h2>
                <p>
                    Сервисный центр укомплектован немецким инструментом марок Wuerth и Gedore. В распоряжении полуавтоматическая сварка MIG-MAG, профессиональный электроинструмент.<br><br>
                    Сервисные автомобили позволяют совершать удаленный ремонт и техническое обслуживание практически в любых условиях.<br><br>
                    Какие операции мы можем проводить на нашей сервисной базе, сколько по времени они могут занимать: без преувеличения, все виды ремонт бетоноподающей техники.<br><br>
                    Какие операции мы можем проводить на выезде, сколько по времени они могут занимать: техническое обслуживание согласно нормативам.<br><br>
                    Доступность сервисной бригады в базовом режиме 8\5, по требованию ситуации возможно срочное реагирование сервисной бригады 24\7.
                </p>
                <div class="image-container--row">
                    <img src="/images/m47-5_15-08122_800_417_0_64_800_481.jpg">
                    <img src="/images/putzmeister_bsf38_11.jpg">
                </div>
                <h2>Цена работы</h2>
                <p>
                    Индивидуальный подход к ценообразованию. Цена рассчитывается исходя из реального состояния насоса. Отпадает необходимость доплачивать за дополнительные запчасти или навязанную работу.
                    <br>
                    <br>
                    Например, цена за работу без учета материала и запасных частей:
                </p>
            </div>
            <div class="service-prices">
                <div>
                    <div class="service-prices__item">
                        <div class="service-prices__item-title">Замена изнашиваемых частей бункера</div>
                        <div class="service-prices__item-time">8&nbsp;часов</div>
                        <div class="service-prices__item-price">3&nbsp;000&nbsp;руб. час</div>
                    </div>
                </div>
                <div>
                    <div class="service-prices__item">
                        <div class="service-prices__item-title">Замена подающих поршней</div>
                        <div class="service-prices__item-time">6&nbsp;часов /</div>
                        <div class="service-prices__item-price">3&nbsp;000&nbsp;руб. час</div>
                    </div>
                </div>
                <div>
                    <div class="service-prices__item">
                        <div class="service-prices__item-title">Ремонт подшипниковых узлов шибера</div>
                        <div class="service-prices__item-time">8&nbsp;часов /</div>
                        <div class="service-prices__item-price">3&nbsp;000&nbsp;руб. час</div>
                    </div>
                </div>
            </div>
            <blockquote>При заказе запчастей и работы в нашем сервисном центре действует постоянная скидка на запчасти 10%, от розничной цены запасных частей.</blockquote>
            <h2>Наши преимущества</h2>
            <div class="service-advs">
                <div class="service-advs__item">
                    <div class="service-advs__item-title">Доступность запасных частей и расходных материалов на складе, на месте</div>
                    <div class="service-advs__item-descr">Расходные материалы и запчасти всегда в наличии на складе, поставка редких запчастей от двух недель.</div>
                </div>
                <div class="service-advs__item">
                    <div class="service-advs__item-title">Какую гарантию мы даем на работы на запчасти</div>
                    <div class="service-advs__item-descr">На работы и запасные части распространяется официальная гарантия производителя – 6 месяцев.</div>
                </div>
                <div class="service-advs__item">
                    <div class="service-advs__item-title">Система накопительной скидки для постоянных заказчиков</div>
                    <div class="service-advs__item-descr">Например, для постоянных клиентов цена работы начинается от 2000 рублей за нормочас.</div>
                </div>
            </div>
            <div class="container--half">
                <p>
                    Мы заботимся о наших насосах и предоставляем уникальное предложение! Не важно, примете ли вы решение обслуживаться у нас или в другом сервисном центре, мы предоставляем бесплатную диагностику вашего насоса Putzmeister на месте. Результат: Подробная дефектовка с описанием неисправности (ей) для минимизации ваших рисков.
                </p>
                <h2>Обучение операторов и механиков, владельцев техники</h2>
                <p>
                    Кроме бесплатного обучения ваших специалистов входящую в контракт при первой поставке насоса, мы предлагаем обучение ваших специалистов при последующих обращениях. Возможность проведения обучения на нашей базе на практике, с привлечением ведущих специалистов компаний поставщиков.
                </p>
                <h2>Дополнительные услуги</h2>
                <p>
                    Наша производственная площадка оборудована сертифицированными весами. Вы можете воспользоваться услугой взвешивания вашего автобетононасоса, с получением официального акта взвешивания, для минимизации ваших рисков и потери времени на дороге.<br><br>
                    Услуга восстановления насосов бывших в эксплуатации! Полное или частичное восстановление, от узлов и агрегатов и смены фильтров, жидкостей и изношенных частей, до высококачественного восстановление лакокрасочного покрытия с фирменными лейблами Putzmeister, что существенно повышает цену вашего бетононасоса на вторичном рынке. Возможно получение дополнительной гарантии, рассматривается индивидуально!
                </p>
                <h2>Профессиональная оценка бетононасоса перед покупкой</h2>
                <p>
                    Вы хотите купить или продать бетононасос, или распределительную стрелу, и хотите быть уверены в реальной карте неисправностей, плюсов и минусов техники, для еще более точного ценообразования или аргументации в процессе торга? Закажите оценку насоса в нашем сервисном центре, или пригласите нашу бригаду на площадку, где работает насос. Мы проведем оценку состояния техники, и дадим наиболее точную цену ремонта или восстановления, что позволит вам сэкономить не только время на поиски решения, но и деньги при покупке или продаже насоса.
                </p>
            </div>
            <div class="service-request">
                <h2>Сделайте запрос прямо сейчас! Putzmeister - ближе вашему Делу!</h2>
                <button class="btn btn--yellow modal_trigger--tech">Запрос на технику</button>
            </div>
            <p>
                Ответить на все вопросы технического характера может наша служба технической поддержки клиентов. Не важно, идет ли речь о современном оборудовании или "раритете", – никто не знает нашу продукцию лучше нас самих. Сотрудники нашей службы поддержки всегда найдут время, чтобы подробно проконсультировать вас. По всем техническим вопросам мы поможем вам как словом, так и делом. Мы сможем компетентно ответить на все вопросы, касающиеся эксплуатации, технического обслуживания, гарантии, ремонта и пусконаладочных работ продукции Putzmeister. Высококвалифицированные специалисты сервисного центра обеспечивают ввод поставленной заказчику техники в эксплуатацию, обучение на местах операторов заказчиков, гарантийное и постгарантийное обслуживание.
            </p>
        </div>
    </div>
</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>