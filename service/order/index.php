<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Запрос на технику | Putzmeister Россия - Официальный сайт");
$APPLICATION->SetPageProperty("description", "Заказ техники Putzmeister на официальном сайте с гарантией от производителя.");?>
<section class="order">
    <div class="wrap-container">
        <h1>Запрос на технику</h1>
        <div class="tech__content">
            <div class="tech__left">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.feedback",
                    "tech_request",
                    Array(
                        "USE_CAPTCHA" => "Y",
                        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                        "EMAIL_TO" => "my@email.com",
                        "REQUIRED_FIELDS" => Array("NAME","EMAIL","MESSAGE"),
                        "EVENT_MESSAGE_ID" => Array("5"),
                        "MODELS_IBLOCK_ID" => 2,
                        "LEAD_FIELDS" => [
                            "MODEL" => "UF_CRM_1537191797205",
                            "REAL_LOCATION" => "UF_CRM_1541750114",
                            "COMPANY" => "UF_CRM_1539164225716"
                        ],
                        "ACTION_URL" => "https://putzmeister.bitrix24.ru/rest/11/b0o23a16sp3s5rb2/crm.lead.add.json/"
                    )
                );?>
            </div>
            <div class="tech__right">
                <p>
                    С помощью формы обратной связи Вы имеете возможность оставить запрос на технику в любое удобное для Вас время. Для удобства Вы можете указать предпочтительное время связи с Вами.
                    <br>
                    <br>
                    Консультации по телефону: <a href="tel:+78007071958">8&nbsp;(800)&nbsp;707&nbsp;1958</a> (Бесплатный номер для звонков с любых телефонов России) или <a href="tel:+74957752237">8&nbsp;(495)&nbsp;775&nbsp;2237</a> по будням с 8:00 до 18:00 по московскому времени
                </p>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");