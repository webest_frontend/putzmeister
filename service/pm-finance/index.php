<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("PM Finance | Putzmeister Россия - Официальный сайт");
$APPLICATION->SetPageProperty("description", "Надежное партнерство дает Вам уверенность - используйте услуги лизинга или аренды от надежных компаний!");?>
<section class="pm-finance">
    <div class="wrap-container">
        <div class="main-rt-side">
            <div class="main-side">
                <div class="text-block one-news">
                    <div class="main-title"><h1>PM Finance</h1></div>
                    <div><div class="text-block one-news">
                            <p>На данный момент ООО «Путцмайстер-Рус», в содружестве с лидерами Российского рынка финансовых услуг, предлагает своим заказчикам услуги Лизинга или Финансовой аренды <a href="http://www.putzmeister.ru/catalog/betononasosy/">бетононасосов</a>, пневмоконвееров и штукатурных станций, а также тоннельного оборудования и шламовых насосов произведенных компанией Putzmeister.</p>
                            <h4>Чем удобно предложение от программы PM Finance?</h4>
                            <p>Обращаясь с запросом к нашим специалистам, Ваши условия рассматривают сразу более трех лизинговых компаний-партнеров ООО "Путцмайстер-Рус".&nbsp;Вам нет необходимости тратить время на поиски подходящей для Вас лизинговой компании.&nbsp;Компании-Партнеры проходят жесткий репутационный контроль, что позволяет Вам доверять лизинговой компании как Putzmeister. Вы получаете предложения и просто выбираете наиболее выгодное для Вас. Далее происходит оформление, которое наши специалисты и специалисты компании-партнера берут на себя. Услуга PM Finance предоставляется бесплатно!</p>
                            <h4>Надежное партнерство дает Вам уверенность!</h4>
                            <p>Для получения дополнительной информации условиях продажи и по другим возникающим вопросам Вы можете обратиться к специалистам компании Putzmeister по телефону 8 (800) 707-19-58 или 8 (495) 775-22-37 по будням с 09.00 до 18.00, по московскому времени или оставив&nbsp; <a style="font-size: 15px; font-weight: normal;" href="http://www.putzmeister.ru/service/order/" target="_blank">заявку через сайт</a>, с отметкой "Лизинг".</p>
                            <h4>С PM Finance Вы экономите время и средства!</h4>
                        </div>
                    </div>
                </div>
                <div class="social-bottom">
                    <div class="social-block addthis_toolbox">
                        <a class="addthis_button_google_plusone_share gp">
                            <span></span>
                        </a>
                        <a class="addthis_button_facebook fb at300b" title="Facebook" href="#">
                            <span></span>
                        </a>
                        <a class="addthis_button_twitter tw at300b" title="Twitter" href="#">
                            <span></span>
                        </a>
                        <a class="addthis_button_vk vk at300b" target="_blank" title="Vkontakte" href="#">
                            <span></span>
                        </a>
                        <div class="atclear"></div>
                    </div>
                    <?/*<script type="text/javascript">
                        var addthis_config = {"data_track_addressbar":false};
                    </script>
                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5295e43f48b67026"></script>*/?>
                </div>
                <link rel="image_src" href="<?=SITE_TEMPLATE_PATH?>/images/nophoto.jpg">
            </div>
            <div class="rt-side"><div class="interesting-links">
                    <div class="main-title"><ins>Другие статьи:</ins></div>
                    <ul>
                        <li><a href="/service/pm-finance/">PM Finance</a></li>
                        <li><a href="/service/documentation/">Документация</a></li>
                        <li><a href="/service/spares/">Заказ запчастей</a></li>
                        <li><a href="/service/order/">Запрос на технику</a></li>
                        <li><a href="/service/remont/">Ремонт</a></li>
                        <li><a href="/service/bu_tehnika/">Б/у техника</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>