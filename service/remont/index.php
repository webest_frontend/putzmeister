<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ремонт Putzmeister: все виды ремонта от официального представителя");
$APPLICATION->SetPageProperty("description", "Мы ремонтируем любую бетонную технику от компании Putzmeister, используя только оригинальные детали.");?>
<section class="remont">
    <div class="wrap-container">
        <h1>Ремонт</h1>
        <div class="services">
            <div class="services__row">
                <div>
                    <div class="service__item">
                        <img src="/images/services/Repair-concrete-pumps.jpg">
                        <a href="/service/remont/remont_betononasosov/">Ремонт бетонанососов</a>
                    </div>
                </div>
                <div>
                    <div class="service__item">
                        <img src="/images/services/Remont_avtobetononasosov.jpg">
                        <a href="/service/remont/remont_avtobetononasosov/">Ремонт автобетононасосов</a>
                    </div>
                </div>
                <div>
                    <div class="service__item" style="background-image: url('/images/services/Remont_rastvoronasosov.jpg');">
                        <img src="/images/services/Remont_rastvoronasosov.jpg">
                        <a href="/service/remont/remont_rastvoronasosov/">Ремонт растворонасосов</a>
                    </div>
                </div>
            </div>
            <h2>Сервисное обслуживание и ремонт бетононасосов Putzmeister - After Sales and Parts (ASP) Putzmeister</h2>
            <p>
                Насосы Putzmeister легко, выгодно и приятно эксплуатировать: они подходят для решения большинства строительных задач при бетонных работах, внутренней и внешней отделке сооружений. Могут эффективно работать в тяжелых условиях, их конструкция продумана и надежно защищена от преждевременного износа фирменными разработками немецких инженеров и патентами компании, в совокупности с высоким качеством материалов используемых в производстве.
                Однако, любое оборудование со временем изнашивается. Разработчики Putzmeister позаботились о том, чтобы максимальное число деталей оборудования можно было легко и недорого заменить прямо на строительной площадке, а также износ таких деталей происходил максимально равномерно и прогнозируемо.
                Российское представительство Putzmeister, в свою очередь, позаботилось о том, чтобы нужные запчасти для бетононасоса всегда были на складах в Москве и у партнеров. Кроме запчастей, мы всегда предложим большой ассортимент дополнительного оборудования, которое расширит возможности вашей техники и упростит повседневную работу.
                Вы можете купить запчасти необходимые Вам на наших складах и получить их в короткие сроки.
                Ассортимент компании включает большое количество высококачественных оригинальных аксессуаров для любых областей применения бетонной, растворной техники, торкрет установок и другой техники Putzmeister.
            </p>
            <h2>Персонал - опыт и квалификация</h2>
            <p>Сервисные мастера с многолетним опытом работы по обслуживанию бетононасосной техники. Уникальные специалисты, совмещающие мультизадачность и проверенные временем навыки гидравлики, электрики и мехатроники. В штате фирмы электросварщик, сертифицированный на сварочные работы всех металлоконструкций бетононасосов.</p>
            <div class="image-container--row">
                <img src="/images/service-img-1.jpg">
                <img src="/images/service-img-2.jpg">
            </div>
            <h2>Какой инструмент, материалы, от каких производителей используем в работе над техникой:</h2>
            <p>
                Сервисный центр укомплектован немецким инструментом марок Wuerth и Gedore. В распоряжении полуавтоматическая сварка MIG-MAG, профессиональный электроинструмент.
                <br>
                Сервисные автомобили позволяют совершать удаленный ремонт и техническое обслуживание практически в любых условиях.
                <br>
                Какие операции мы можем проводить на нашей сервисной базе, сколько по времени они могут занимать: без преувеличения, все виды ремонт бетоноподающей техники.
                <br>
                Какие операции мы можем проводить на выезде, сколько по времени они могут занимать: Техническое обслуживание согласно нормативам.
                <br>
                Доступность сервисной бригады в базовом режиме 8\5, по требованию ситуации возможно срочное реагирование сервисной бригады 24\7.</p>
            <p>
            <div class="image-container--row">
                <img src="/images/service-img-3.jpg">
                <img src="/images/service-img-4.jpg">
            </div>
            <h2>Цена работы:</h2>
            <p>
                Индивидуальный подход к ценообразованию. Цена рассчитывается исходя из реального состояния насоса. Отпадает необходимость доплачивать за дополнительные запчасти или навязанную работу.
                <br>
                Например, цена за работу без учета материала и запасных частей:
            </p>
            <div class="service-prices">
                <div>
                    <div class="service-prices__item">
                        <div class="service-prices__item-title">Замена изнашиваемых частей бункера</div>
                        <div class="service-prices__item-time">8&nbsp;часов</div>
                        <div class="service-prices__item-price">3&nbsp;000&nbsp;руб. час</div>
                    </div>
                </div>
                <div>
                    <div class="service-prices__item">
                        <div class="service-prices__item-title">Замена подающих поршней</div>
                        <div class="service-prices__item-time">6&nbsp;часов /</div>
                        <div class="service-prices__item-price">3&nbsp;000&nbsp;руб. час</div>
                    </div>
                </div>
                <div>
                    <div class="service-prices__item">
                        <div class="service-prices__item-title">Ремонт подшипниковых узлов шибера</div>
                        <div class="service-prices__item-time">8&nbsp;часов /</div>
                        <div class="service-prices__item-price">3&nbsp;000&nbsp;руб. час</div>
                    </div>
                </div>
            </div>
            <blockquote>При заказе запчастей и работы в нашем сервисном центре действует постоянная скидка на запчасти 10%, от розничной цены запасных частей.</blockquote>
            <div class="service-request">
                <h2>Сделайте запрос прямо сейчас!<br>Putzmeister - ближе вашему Делу!</h2>
                <button class="btn btn--yellow modal_trigger--tech" data-suffix="Ремонт">Запрос на технику</button>
            </div>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");