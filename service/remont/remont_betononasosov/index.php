<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ремонт бетононасосов Putzmeister от официального представителя");
$APPLICATION->SetPageProperty("description", "Производим ремонт бетононасосов Путцмастер любой сложности в официальном сервисном центре с использованием оригинальных комплектующих.");?>
<section class="remont--betononasosy">
    <div class="wrap-container html_content">
        <h1>Ремонт бетононасосов</h1>
        <div class="service-request">
            <h2>Сделайте запрос прямо сейчас!<br>Putzmeister - ближе вашему Делу!</h2>
            <button class="btn btn--yellow modal_trigger--tech" data-suffix="Ремонт бетононасосов">Запрос на технику</button>
        </div>
        <p>
            Ответить на все вопросы технического характера может наша служба технической поддержки клиентов. Не важно, идет ли речь о современном оборудовании или «раритете», – никто не знает нашу продукцию лучше нас самих. Сотрудники нашей службы поддержки всегда найдут время, чтобы подробно проконсультировать вас. По всем техническим вопросам мы поможем вам как словом, так и делом. Мы сможем компетентно ответить на все вопросы, касающиеся эксплуатации, технического обслуживания, гарантии, ремонта и пусконаладочных работ продукции Putzmeister. Высококвалифицированные специалисты сервисного центра обеспечивают ввод поставленной заказчику техники в эксплуатацию, обучение на местах операторов заказчиков, гарантийное и постгарантийное обслуживание.
        </p>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");