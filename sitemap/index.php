<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Карта сайта");
$APPLICATION->SetPageProperty('header_title', 'Карта сайта');?>
<section class="search_results">
    <div class="wrap-container">
        <h1>Карта сайта</h1>
        <?$APPLICATION->IncludeComponent(
	"bitrix:main.map", 
	".default", 
	array(
		"LEVEL" => "4",
		"COL_NUM" => "2",
		"SHOW_DESCRIPTION" => "Y",
		"SET_TITLE" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");