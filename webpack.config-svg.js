const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
module.exports = {
    mode: 'development',
    entry: './sprite.js',
    output: {
        filename: 'bundle.js',
        path: __dirname + '/temp'
    },
    module: {
        rules: [
            {
                test: /\.svg$/,
                loader: 'svg-sprite-loader',
                options: {
                    extract: true,
                    spriteFilename: '../design/images/icons/sprite.svg'
                }
            }
        ]
    },
    plugins: [
        new SpriteLoaderPlugin()
    ]
};